angular.module('browseApp', ['ngCookies'])

.run(function($rootScope, $http, $cookies, $location) {
  $rootScope.rootUrl = window.location.origin;
  if ($cookies.authToken == null || window.location.href.indexOf('?sharedAuthToken') > -1) {
    $http.defaults.headers.common.Authorization = $cookies.sharedAuthToken;
    $rootScope.account = $cookies.sharedAccount;
    $rootScope.sharable = false;
  }
  else {
    $http.defaults.headers.common.Authorization = $cookies.authToken;
    $rootScope.account = $cookies.account;
    $rootScope.sharable = true;
  }
})

.controller('ListContainersController', function($scope, $cookies, $http, $rootScope) {
  $scope.showModal = false;
  var qrcode = null;
  $scope.toggleModal = function(){
    if (qrcode == null) {
      qrcode = new QRCode("qrcode");
      var qrImage = angular.element( document.querySelector( '#qrcode img' ) );
      qrImage.addClass("center-block");
      qrImage.addClass("img-responsive");
    }
    var qrcodeText = { account: $rootScope.account, authToken: $cookies.authToken, url: window.location.origin }
    qrcode.makeCode(JSON.stringify(qrcodeText));
    $scope.showModal = !$scope.showModal;
  };
  $http.get('/api/container/' + $rootScope.account + '/containers').
    success(function(data, status, headers, config) {
    var containerList = $scope.containers = [];
    data.containers.forEach(function(container) {
      $http.get('/api/file/' + $rootScope.account + '/' + container + '/metadata.json').
        success(function(data, status, headers, config) {
        var feed = JSON.parse(data.Feed);
        var key = atob(feed[2]);
        if (key.length == 0 || key == "contacts" ||
            key == "favoritethings" || key.substring(0, 7) == "control_") {
          return;
        }

        var containerObject = {
          title : data.Name,
          containerId : container,
          thumbnailURI : 'img/omlet.png'
        };

        if (data.Thumbnail != null && data.Thumbnail.Hash != null) {
          containerObject.thumbnailURI = 'api/file/' + base64.encode(data.Thumbnail.Hash);
        }

        if (data.Name == null) {
          var title = '';
          data.Members.forEach(function(member) {
            title = title + JSON.parse(member.Identity)[0].split(':')[1] + ' ';
          });
          containerObject.title = title;
        }

        containerList.push(containerObject);
      });
    });
  });
})

.controller('ListMediaController', function($scope, $cookies, $http, $sce, $rootScope, $location) {
  var containerObject;
  $scope.shown = false;
  $scope.trustSrc = function(url) {
    return $sce.trustAsResourceUrl(url);
  }

  $scope.getSharableURL = function() {
    $http.get("/api/container/" + $rootScope.account + '/' + containerObject.containerId + '/share').
          success(function(data, status, headers, config) {
            $scope.sharableURL = $location.absUrl() + '?sharedAuthToken=' + data.sharedAuthToken;
        });
  };

  $scope.showShareModal = false;
  $scope.toggleShareModal = function(){
    $scope.showShareModal = !$scope.showShareModal;
    if ($scope.sharableURL == null) {
        console.log("HI");
        $scope.getSharableURL();
    }
  };

  $scope.toggleShare = function() {
    $scope.shown = true;
  };

  $scope.init = function(containerId) {
    var mediaObjectArray = $scope.media = [];
    $http.get('/api/file/' + $rootScope.account + '/' + containerId + "/files?tag=msgs").
      success(function(data, status, headers, config) {
      data.files.forEach(function(feedsJson) {
        $http.get('/api/file/' + base64.encode(feedsJson.md5)).
          success(function(data, status, headers, config) {
          data.forEach(function(feedEntryJson) {
            if (feedEntryJson.Type == "picture") {
              var messageJson = JSON.parse(atob(feedEntryJson.Message));
              var mediaObject = {
                thumbnailHash : base64.encode(messageJson.thumbnailHash),
                fullSizeHash : base64.encode(messageJson.fullSizeHash),
                fullSizeHeight : messageJson.fullSizeHeight,
                fullSizeWidth : messageJson.fullSizeWidth,
                type: feedEntryJson.Type
              }
              mediaObjectArray.push(mediaObject);
            }
            else if (feedEntryJson.Type == "animated_gif") {
              var messageJson = JSON.parse(atob(feedEntryJson.Message));
              var mediaObject = {
                thumbnailHash : base64.encode(messageJson.gifHash),
                fullSizeHash : base64.encode(messageJson.gifHash),
                fullSizeHeight : messageJson.height,
                fullSizeWidth : messageJson.width,
                type: feedEntryJson.Type
              }
              mediaObjectArray.push(mediaObject);
            }
            else if (feedEntryJson.Type == "video") {
              var messageJson = JSON.parse(atob(feedEntryJson.Message));
              var mediaObject = {
                thumbnailHash : base64.encode(messageJson.thumbnailHash),
                fullSizeHash : base64.encode(messageJson.fileHash),
                fullSizeHeight : messageJson.thumbnailHeight,
                fullSizeWidth : messageJson.thumbnailWidth,
                type: feedEntryJson.Type
              }
              mediaObjectArray.push(mediaObject);
            }
          });
        });
      });
    });
    $http.get('/api/file/' + $rootScope.account + '/' + containerId + '/metadata.json').
      success(function(data, status, headers, config) {
      containerObject = {
        title : data.Name,
        containerId : containerId,
        thumbnailURI : 'img/omlet.png'
      };

      if (data.Thumbnail != null && data.Thumbnail.Hash != null) {
        containerObject.thumbnailURI = 'api/file/' + base64.encode(data.Thumbnail.Hash);
      }

      if (data.Name == null) {
        var title = '';
        data.Members.forEach(function(member) {
          title = title + JSON.parse(member.Identity)[0].split(':')[1] + ' ';
        });
        containerObject.title = title;
      }
      $scope.container = containerObject;
    });
  };
  $scope.showModal = false;
  $scope.toggleModal = function(mediaEntity){
    $scope.mediaEntity = mediaEntity;
    $scope.showModal = !$scope.showModal;
  };
})

.directive('modal', function ($timeout) {
  return {
    template: '<div class="modal fade" role="dialog">' +
      '<div class="modal-dialog">' +
      '<div class="modal-content">' +
      '<div class="modal-body" ng-transclude></div>' +
      '</div>' +
      '</div>' +
      '</div>',
    restrict: 'E',
    transclude: true,
    replace:true,
    scope:true,
    link: function postLink(scope, element, attrs) {
      scope.title = attrs.title;
      scope.$watch(attrs.visible, function(value){
        if(value == true)
          $(element).modal('show');
        else
          $(element).modal('hide');
      });
      $(element).on('shown.bs.modal', function() {
        $timeout(function(){
          scope.$apply(function(){
            scope.$parent[attrs.visible] = true;
          });
        });
      });
      $(element).on('hidden.bs.modal', function() {
        $timeout(function(){
          scope.$apply(function(){
            scope.$parent[attrs.visible] = false;
            scope.shown = false;
          });
        });
      });
    }
  };
});
