#!/bin/bash

echo "#################### Creating Jar with dependencies ####################"
mvn clean compile assembly:single
rm -rf build
mkdir build
cd build
echo "#################### Copying static files ####################"
cp ../target/omlet-store-0.0.1-SNAPSHOT-jar-with-dependencies.jar .
cp -R ../conf .
cp -R ../webroot .
cp -R ../templates .
cd ..
echo "#################### Creating tarball ####################"
tar -czf ombox-build.tar.gz build
echo "#################### Cleanup ####################"
rm -rf build
echo "Done!"
