package core;

import java.util.Arrays;

/**
 * Light weight class that just provides hashCode() and equals().
 */
public class ByteArrayWrapper {

    private byte[] bytes;

    public ByteArrayWrapper(byte[] bytes) {
        this.bytes = Arrays.copyOf(bytes, bytes.length);
    }

    @Override
    public int hashCode() {
        return Arrays.hashCode(bytes);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (!(obj instanceof ByteArrayWrapper))
            return false;
        ByteArrayWrapper other = (ByteArrayWrapper) obj;
        if (!Arrays.equals(bytes, other.bytes))
            return false;
        return true;
    }

}
