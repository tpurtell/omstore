package core;

import javax.ws.rs.client.Client;

import org.glassfish.jersey.client.HttpUrlConnectorProvider;
import org.glassfish.jersey.client.JerseyClientBuilder;

import com.fasterxml.jackson.jaxrs.json.JacksonJsonProvider;

public class WebClient {
    final static Client webClient = new JerseyClientBuilder().register(JacksonJsonProvider.class)
            .property(HttpUrlConnectorProvider.USE_FIXED_LENGTH_STREAMING, true).build();

    public static Client getWebClient() {
        return webClient;
    }

}
