package core;

import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.Logger;

public class LogConfigurator {
    static {
        // Get the root logger
        Logger rootLogger = Logger.getLogger("");
        for (Handler handler : rootLogger.getHandlers()) {
            // Change log level of default handler(s) of root logger
            // The paranoid would check that this is the ConsoleHandler ;)
            handler.setLevel(Level.FINE);
        }
        // Set root logger level
        rootLogger.setLevel(Level.FINE);
    }

    public static void configure() {
    }

}
