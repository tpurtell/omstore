package core;

import java.io.IOException;

import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.guava.GuavaModule;

public class JsonSerializer {
    public static final ObjectMapper mapper = getSharedObjectMapper();
    public static final ObjectMapper tolerantMapper = getTolerantSharedObjectMapper();

    public static <T> byte[] serialize(T t) {
        try {
            return mapper.writeValueAsBytes(t);
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }

    public static <T> T deserialize(byte[] b, Class<T> c) {
        try {
            return mapper.readValue(b, c);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public static <T> String serializeString(T t) {
        try {
            return mapper.writeValueAsString(t);
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }

    public static <T> T deserializeString(String s, Class<T> c) {
        try {
            return mapper.readValue(s, c);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public static <T> T deserializeTolerant(byte[] b, Class<T> c) {
        try {
            return tolerantMapper.readValue(b, c);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public static <T> T deserializeStringTolerant(String s, Class<T> c) {
        try {
            return tolerantMapper.readValue(s, c);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public static ObjectMapper getSharedObjectMapper() {
        ObjectMapper om = new ObjectMapper();
        om.registerModule(new GuavaModule());
        om.setSerializationInclusion(Include.NON_NULL);
        return om;
    }

    public static ObjectMapper getTolerantSharedObjectMapper() {
        ObjectMapper om = new ObjectMapper();
        om.registerModule(new GuavaModule());
        om.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        om.setSerializationInclusion(Include.NON_NULL);
        return om;
    }

}
