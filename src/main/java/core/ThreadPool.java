package core;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ThreadPool {
    public static final ExecutorService GLOBAL = Executors.newCachedThreadPool();
}
