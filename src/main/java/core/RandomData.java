package core;

import java.math.BigInteger;
import java.security.SecureRandom;

import com.google.common.io.BaseEncoding;

public class RandomData {
    public static final SecureRandom random = new SecureRandom();

    public static byte[] randomBytes(int length) {
        byte[] data = new byte[length];
        random.nextBytes(data);
        return data;
    }

    public static BigInteger randomToken(int bits) {
        return new BigInteger(bits, random);
    }

    public static byte[] randomBytes(int minLength, int maxLength) {
        int l = random.nextInt(maxLength - minLength) + minLength;
        return randomBytes(l);
    }

    public static String randomString(int minLength, int maxLength) {
        int l = random.nextInt(maxLength - minLength) + minLength;
        return randomString(l);
    }

    public static String randomString(int length) {
        byte[] data = randomBytes((length * 3 + 3) / 4);
        return BaseEncoding.base64().encode(data).substring(0, length);
    }

    public static String randomWebSafeString(int length) {
        byte[] data = randomBytes(length);
        return BaseEncoding.base32().encode(data).substring(0, length);
    }

    public static long randomPositiveLong() {
        return Math.abs(random.nextLong());
    }

}
