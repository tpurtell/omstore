package core;

import java.io.*;

import com.amazonaws.util.IOUtils;

public class ConfigurationFile {
    public static <T> T load(Class<T> configurationClass) {
        // load a deployment specific config
        try {
            FileInputStream in = new FileInputStream("override/" + configurationClass.getSimpleName() + ".json");
            return JsonSerializer.deserialize(IOUtils.toByteArray(in), configurationClass);
        } catch (IOException e) {
        }
        // load a checked in config, don't put secret keys here
        try {
            FileInputStream in = new FileInputStream("conf/" + configurationClass.getSimpleName() + ".json");
            return JsonSerializer.deserialize(IOUtils.toByteArray(in), configurationClass);
        } catch (IOException e) {
        }
        try {
            return configurationClass.newInstance();
        } catch (InstantiationException | IllegalAccessException e) {
            throw new RuntimeException(e);
        }
    }

    public static <T> void writeConfigForProd(T config) {
        try {
            new File("override/").mkdirs();
            FileOutputStream out = new FileOutputStream("override/" + config.getClass().getSimpleName() + ".json");
            out.write(JsonSerializer.serialize(config));
            out.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
