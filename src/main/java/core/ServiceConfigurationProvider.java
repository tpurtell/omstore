package core;

import org.glassfish.hk2.api.ServiceLocator;

public interface ServiceConfigurationProvider {
    public void bind(ServiceLocator locator);
}
