package core;

import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.log4j.Logger;

import com.amazonaws.ClientConfiguration;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClient;
import com.amazonaws.services.dynamodbv2.model.AttributeValue;
import com.amazonaws.services.dynamodbv2.model.DescribeTableRequest;
import com.amazonaws.services.dynamodbv2.model.ResourceNotFoundException;
import com.amazonaws.services.dynamodbv2.model.TableDescription;
import com.amazonaws.services.dynamodbv2.model.TableStatus;
import com.fasterxml.jackson.annotation.JsonProperty;

public class DynamoUtils {
    public static final String TEST_TABLE_BASE = "TEST_";
    static Logger logger = Logger.getLogger(DynamoUtils.class);

    public static class DynamoConfiguration {
        @JsonProperty("Charles")
        public boolean charles;
        @JsonProperty("Key")
        public String key;
        @JsonProperty("Secret")
        public String secret;
        @JsonProperty("Region")
        public String region;
        @JsonProperty("Endpoint")
        public String endpoint;
        @JsonProperty("ServiceName")
        public String serviceName;
    }

    public static AmazonDynamoDBClient getDynamoClient() {
        DynamoConfiguration dynamo_config = ConfigurationFile.load(DynamoConfiguration.class);

        ClientConfiguration config = new ClientConfiguration();
        if (dynamo_config.charles)
            config.withProxyHost("127.0.0.1").withProxyPort(8888);

        AmazonDynamoDBClient client = new AmazonDynamoDBClient(new BasicAWSCredentials(
                dynamo_config.key, dynamo_config.secret), config);

        if (dynamo_config.region != null)
            client.setRegion(Region.getRegion(Regions.fromName(dynamo_config.region)));
        if (dynamo_config.endpoint != null)
            client.setEndpoint(dynamo_config.endpoint);
        if (dynamo_config.serviceName != null)
            client.setServiceNameIntern(dynamo_config.serviceName);

        return client;
    }

    public static void waitForTableToBeDeleted(AmazonDynamoDBClient client, String tableName) {
        long startTime = System.currentTimeMillis();
        long endTime = startTime + (10 * 60 * 1000);
        while (System.currentTimeMillis() < endTime) {
            try {
                DescribeTableRequest request = new DescribeTableRequest().withTableName(tableName);
                TableDescription tableDescription = client.describeTable(request).getTable();
                String tableStatus = tableDescription.getTableStatus();
                logger.info("  - current state: " + tableStatus);
                if (tableStatus.equals(TableStatus.ACTIVE.toString()))
                    return;
            } catch (ResourceNotFoundException e) {
                logger.info("Table " + tableName + " is not found. It was deleted.");
                return;
            }
            try {
                Thread.sleep(1000 * 5);
            } catch (Exception e) {
            }
        }
        throw new RuntimeException("Table " + tableName + " was never deleted");
    }

    static class AvListEntry {
        public String key;
        public List<String> ns;
        public List<String> ss;
        public List<byte[]> bs;
        public String n;
        public String s;
        public byte[] b;
    }

    static class AvList {
        public List<AvListEntry> l;
    }

    private static List<byte[]> convertByteBuffersToArrays(List<ByteBuffer> l) {
        if (l == null)
            return null;
        ArrayList<byte[]> m = new ArrayList<>(l.size());
        for (ByteBuffer b : l)
            m.add(b.array());
        return m;
    }

    private static List<ByteBuffer> convertArraysToByteBuffers(List<byte[]> l) {
        if (l == null)
            return null;
        ArrayList<ByteBuffer> m = new ArrayList<>(l.size());
        for (byte[] b : l)
            m.add(ByteBuffer.wrap(b));
        return m;
    }

    public static byte[] temporarySerializeKey(Map<String, AttributeValue> k) {
        if (k == null)
            return null;
        ArrayList<AvListEntry> avl = new ArrayList<>(k.size());
        for (Entry<String, AttributeValue> e : k.entrySet()) {
            AvListEntry av = new AvListEntry();
            av.key = e.getKey();
            av.ns = e.getValue().getNS();
            av.ss = e.getValue().getSS();
            av.bs = convertByteBuffersToArrays(e.getValue().getBS());
            av.n = e.getValue().getN();
            av.s = e.getValue().getS();
            if (e.getValue().getB() != null)
                av.b = e.getValue().getB().array();
            avl.add(av);
        }
        AvList l = new AvList();
        l.l = avl;
        return JsonSerializer.serialize(l);
    }

    public static Map<String, AttributeValue> temporaryDeserializeKey(byte[] data) {
        if (data == null)
            return null;
        AvList l = JsonSerializer.deserialize(data, AvList.class);
        List<AvListEntry> avl = l.l;
        HashMap<String, AttributeValue> k = new HashMap<>(avl.size());
        for (AvListEntry av : avl) {
            AttributeValue v = new AttributeValue();
            if (av.n != null)
                v.setN(av.n);
            if (av.s != null)
                v.setS(av.s);
            if (av.b != null)
                v.setB(ByteBuffer.wrap(av.b));
            if (av.ns != null)
                v.setNS(av.ns);
            if (av.ss != null)
                v.setSS(av.ss);
            if (av.bs != null)
                v.setBS(convertArraysToByteBuffers(av.bs));
            k.put(av.key, v);
        }
        return k;
    }
}
