package core;

import java.nio.ByteBuffer;

public class KeyEncoding {
    public static ByteBuffer encodeLongBigEndian(long timestamp) {
        return (ByteBuffer) ByteBuffer.allocate(8).putLong(timestamp).rewind();
    }

    public static long decodeLongBigEndian(ByteBuffer data) {
        return data.getLong();
    }

    public static ByteBuffer encodeBool(boolean b) {
        ByteBuffer buf = ByteBuffer.allocate(1);
        buf.put(b ? (byte) 1 : (byte) 0);
        buf.rewind();
        return buf;
    }

    public static boolean decodeBool(ByteBuffer buf) {
        return buf.get() == 0 ? false : true;
    }

    public static class AccountAndContainer {
        public String account;
        public String container;
    }



    public static ByteBuffer encodeAccountAndContainer(String account, String container) {
        byte[] account_bytes = account.getBytes();
        byte[] container_bytes = container.toString().getBytes();

        if (account_bytes.length > 65535)
            throw new RuntimeException("Account size too large");
        if (container_bytes.length > 65535)
            throw new RuntimeException("Container size too large");

        ByteBuffer buf = ByteBuffer.allocate(2 + account_bytes.length + 2 + container_bytes.length);
        buf.putShort((short) account_bytes.length);
        buf.put(account_bytes);
        buf.putShort((short) container_bytes.length);
        buf.put(container_bytes);
        buf.rewind();
        return buf;
    }

    public static AccountAndContainer decodeAccountAndContainer(ByteBuffer buf) {
        byte[] account_raw = new byte[buf.getShort()];
        buf.get(account_raw);
        byte[] container_raw = new byte[buf.getShort()];
        buf.get(container_raw);
        AccountAndContainer ac = new AccountAndContainer();
        ac.account = new String(account_raw);
        ac.container = new String(container_raw);
        return ac;
    }



}
