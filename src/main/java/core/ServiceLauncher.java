package core;

import java.io.IOException;
import java.net.URI;
import java.util.regex.Pattern;

import org.glassfish.grizzly.http.server.HttpServer;
import org.glassfish.grizzly.http.server.NetworkListener;
import org.glassfish.grizzly.http.server.Request;
import org.glassfish.grizzly.http.server.Response;
import org.glassfish.grizzly.http.server.ServerConfiguration;
import org.glassfish.grizzly.http.server.StaticHttpHandler;
import org.glassfish.hk2.api.ServiceLocator;
import org.glassfish.hk2.utilities.ServiceLocatorUtilities;
import org.glassfish.jersey.grizzly2.httpserver.GrizzlyHttpContainer;
import org.glassfish.jersey.grizzly2.httpserver.GrizzlyHttpContainerProvider;
import org.glassfish.jersey.server.ApplicationHandler;
import org.glassfish.jersey.server.ResourceConfig;

import core.jxp.JxpTemplateProvider;

public class ServiceLauncher {
    public final URI baseUri;
    public final ServiceLocator locator;
    public final ResourceConfig restletConfig;
    public final GrizzlyHttpContainer restletContainer;
    public final HttpServer server;
    private boolean started;

    public ServiceLauncher(URI baseUri, ResourceConfig restletConfig) {
        this.baseUri = baseUri;
        this.restletConfig = restletConfig;
        restletContainer = new GrizzlyHttpContainerProvider().createContainer(
                GrizzlyHttpContainer.class, restletConfig);
        ApplicationHandler handler = restletContainer.getApplicationHandler();
        locator = handler.getServiceLocator();
        int port = baseUri.getPort() < 0 ? (baseUri.getScheme().equals("https") ? 443 : 80)
                : baseUri.getPort();
        server = new HttpServer();
        final ServerConfiguration config = server.getServerConfiguration();
        String path = baseUri.getPath().replaceAll("/{2,}", "/");
        path = path.endsWith("/") ? path.substring(0, path.length() - 1) : path;
        NetworkListener listener = new NetworkListener("listener", "0.0.0.0", port);
        server.addListener(listener);

        config.addHttpHandler(new OmstoreStaticHttpHandler());

        // in case we need access to these somewhere else, this could be helpful
        ServiceLocatorUtilities.addOneConstant(locator, this);
        ServiceLocatorUtilities.addClasses(locator, JxpTemplateProvider.class);
    }

    public void start() throws IOException {
        started = true;
        server.start();
    }

    public void stop() {
        if (!started)
            return;
        server.shutdownNow();
    }

    private class OmstoreStaticHttpHandler extends StaticHttpHandler {
        public OmstoreStaticHttpHandler() {
            super("webroot");
            setFileCacheEnabled(false);
        }

        Pattern urlPattern = Pattern.compile("^(?!/api).*\\.(gif|html|css|js|png|jpg|map|eot|svg|ttf|woff|woff2)");
        Pattern blankUrlPattern = Pattern.compile("^/$");

        @Override
        public void service(Request request, Response response) throws Exception {
            final String uri = getRelativeURI(request);
            if (uri != null && (urlPattern.matcher(uri).matches() | blankUrlPattern.matcher(uri).matches())) {
                super.service(request, response);
                return;
            }
            restletContainer.service(request, response);
        }
    }
}
