package core;

public class LocatorIds {
    public static final String TABLE_BASE = "TableBase";
    public static final String PUBLIC_URI = "PublicUri";
    public static final String CHANNEL = "Channel";
    public static final String LOCAL_STORAGE_PATH = "LocalStoragePath";

}
