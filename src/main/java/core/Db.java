package core;

public interface Db {
    void createTables();

    void deleteTables();
}
