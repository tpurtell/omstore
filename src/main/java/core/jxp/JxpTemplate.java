package core.jxp;

import java.io.IOException;
import java.io.Writer;

public interface JxpTemplate {
    void process(Object model, Writer out) throws IOException;
}
