package core.jxp;

import java.io.File;
import java.io.FileInputStream;
import java.io.PrintWriter;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.inject.Inject;
import javax.tools.JavaCompiler;
import javax.tools.JavaFileObject;
import javax.tools.StandardJavaFileManager;
import javax.tools.ToolProvider;
import javax.ws.rs.ext.Provider;

import org.apache.commons.io.IOUtils;
import org.apache.commons.io.output.StringBuilderWriter;
import org.apache.log4j.Logger;
import org.glassfish.hk2.api.ServiceLocator;
import org.glassfish.hk2.utilities.ServiceLocatorUtilities;

import com.google.common.base.Joiner;

@Provider
public class JxpTemplateProvider {
    Logger logger = Logger.getLogger(JxpTemplateProvider.class);
    ServiceLocator locator;

    @Inject
    public JxpTemplateProvider(ServiceLocator locator) {
        this.locator = locator;
        try {
            jxpLoader = new URLClassLoader(new URL[] { new File("tmp").toURL() }, getClass()
                    .getClassLoader());
        } catch (MalformedURLException e) {
            throw new RuntimeException(e);
        }
        new File("tmp").mkdirs();
    }

    ClassLoader jxpLoader;
    Pattern pattern = Pattern.compile("<%(.+?)%>", Pattern.MULTILINE | Pattern.DOTALL);

    static class Slice {
        String plain;
        String code;
        String expr;

        static Slice makePlain(String plain) {
            Slice s = new Slice();
            s.plain = plain;
            return s;
        }

        static Slice makeCode(String code) {
            Slice s = new Slice();
            s.code = code;
            return s;
        }

        static Slice makeExpr(String expr) {
            Slice s = new Slice();
            s.expr = expr;
            return s;
        }
    }

    public static class Entry {
        File file;
        long time;
        Class<? extends JxpTemplate> cls;

        boolean expired() {
            return file.lastModified() > time;
        }

    }

    ConcurrentHashMap<String, Entry> compiled = new ConcurrentHashMap<String, JxpTemplateProvider.Entry>();

    private Class<? extends JxpTemplate> resolve(String templatePath) throws Exception {
        Entry entry = compiled.get(templatePath);
        if (entry == null || entry.expired())
            entry = compile(templatePath);
        return entry.cls;
    }

    public synchronized void evict() {
        compiled.clear();
    }

    private synchronized Entry compile(String templatePath) throws Exception {
        long now = System.currentTimeMillis();
        String clsName = "JXP_"
                + templatePath.replace(".jxp", "").replace('/', '_').replace('-', '_') + "_" + now;
        File file = new File(".", templatePath);
        FileInputStream f = new FileInputStream(file);
        PrintWriter o = null;
        try {
            String data = IOUtils.toString(f);

            Matcher m = pattern.matcher(data);

            List<String> imports = new LinkedList<>();
            List<String> injects = new LinkedList<>();
            String model = "Object";
            List<Slice> slices = new LinkedList<>();
            int last = 0;
            while (m.find()) {
                String body = m.group(1);
                if (m.start() != last)
                    slices.add(Slice.makePlain(data.substring(last, m.start())));
                last = m.end();
                if (body.charAt(0) == '@') {
                    // non standard syntax
                    if (body.split("\"").length == 1)
                        imports.add(body);
                    imports.addAll(Arrays.asList(body.split("\"")[1].split(",")));
                } else if (body.charAt(0) == '!') {
                    body = body.substring(1);
                    body = body.split(";")[0];
                    body = body.trim();
                    if (body.split("[ \t]")[1].equals("model")) {
                        model = body.split("[ \t]")[0];
                    } else {
                        injects.add(body);
                    }
                } else if (body.charAt(0) == '=') {
                    slices.add(Slice.makeExpr(body.substring(1)));
                } else {
                    slices.add(Slice.makeCode(body));
                }
            }
            if (last < data.length()) {
                slices.add(Slice.makePlain(data.substring(last)));
            }

            File src_path = new File("tmp", clsName + ".java");
            o = new PrintWriter(src_path);

            o.println("import org.glassfish.jersey.process.internal.RequestScoped;");
            o.println("import core.jxp.JxpTemplate;");
            o.println("import java.io.IOException;");
            o.println("import java.io.Writer;");
            o.println("import javax.inject.Inject;");
            for (String i : imports)
                o.println("import " + i + ";");

            int intr = 0;
            o.println("@RequestScoped");
            o.println("public class " + clsName + " implements JxpTemplate {");
            for (String inject : injects) {
                String type = inject.split("[ \t]")[0];
                String name = inject.split("[ \t]")[1];
                o.println("\t" + type + " " + name + ";");
            }
            o.println("\t@Inject");
            o.println("\tpublic " + clsName + "(" + Joiner.on(", ").join(injects) + ") {");
            for (String inject : injects) {
                String name = inject.split("[ \t]")[1];
                o.println("\t\tthis." + name + " = " + name + ";");
            }
            o.println("\t}");
            o.println("\t@Override");
            o.println("\tpublic void process(Object __model, Writer __out) throws IOException {");
            o.println("\t\t" + model + " model = (" + model + ")__model;");
            for (Slice slice : slices) {
                if (slice.plain != null) {
                    String raw = slice.plain.replace("\\", "\\\\");
                    raw = raw.replace("\"", "\\\"");
                    raw = raw.replace("\r", "\\r");
                    raw = raw.replace("\n", "\\n");
                    o.print("\t__out.write(\"");
                    o.print(raw);
                    o.print("\");");
                } else if (slice.code != null) {
                    o.print(slice.code);
                } else if (slice.expr != null) {
                    o.println("\tString __intermediate_" + (++intr) + " = " + slice.expr + ";");
                    o.println("\t__out.write(__intermediate_" + intr + ".toString());");
                }
                o.println();
            }
            o.println("\t}");
            o.println("}");
            o.close();

            JavaCompiler compiler = ToolProvider.getSystemJavaCompiler();
            if (compiler == null)
                throw new RuntimeException("Must run using a JDK");
            StandardJavaFileManager fileManager = compiler.getStandardFileManager(null, null, null);

            Iterable<? extends JavaFileObject> compilationUnits1 = fileManager
                    .getJavaFileObjectsFromFiles(Arrays.asList(src_path));
            StringBuilderWriter output = new StringBuilderWriter();
            if (!compiler.getTask(output, fileManager, null, null, null, compilationUnits1).call())
                throw new RuntimeException("compile error for template " + templatePath + "\n" + output.toString());

            Class cls = (Class) jxpLoader.loadClass(clsName);
            ServiceLocatorUtilities.addClasses(locator, cls);
            Entry entry = new Entry();
            entry.cls = cls;
            entry.file = file;
            entry.time = now;
            compiled.put(templatePath, entry);
            return entry;
        } finally {
            f.close();
            if (o != null)
                o.close();
        }
    }

    public JxpProcessor instantiate(String template) {
        return instantiate(template, null);
    }

    public JxpProcessor instantiate(String template, final Object model) {
        try {
            JxpTemplate compiled = locator.getService(resolve(template));
            return new JxpProcessor(compiled, model);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
