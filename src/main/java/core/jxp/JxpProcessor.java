package core.jxp;

import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.StreamingOutput;

import org.apache.commons.io.output.StringBuilderWriter;

public class JxpProcessor implements StreamingOutput {

    public JxpTemplate compiled;
    public Object model;

    public JxpProcessor(JxpTemplate compiled, Object model) {
        this.compiled = compiled;
        this.model = model;
    }

    @Override
    public void write(OutputStream output) throws IOException, WebApplicationException {
        Writer writer = new OutputStreamWriter(output);
        try {
            compiled.process(model, writer);
        } finally {
            writer.close();
        }
    }

    public String evaluate() {
        try {
            StringBuilderWriter writer = new StringBuilderWriter();
            compiled.process(model, writer);
            return writer.toString();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

}
