package core;

public class JsonLoggable {
    @Override
    public String toString() {
        return JsonSerializer.serializeString(this);
    }
}
