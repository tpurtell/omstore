package omstore;

import omstore.http.RegistrationTest;
import org.junit.internal.TextListener;
import org.junit.runner.JUnitCore;

public class RegistrationTestRunner {
    public static String url;
    public static String adminKey;

    public static void main(String[] args) {
        url = args[0];
        adminKey = args[1];
        JUnitCore jUnitCore = new JUnitCore();
        jUnitCore.addListener(new TextListener(System.out));
        jUnitCore.run(RegistrationTest.class);
    }
}
