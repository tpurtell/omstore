package omstore.resolvers;

import omstore.OptionalAuthentication;
import org.apache.log4j.Logger;
import org.glassfish.hk2.api.Factory;
import org.glassfish.jersey.process.internal.RequestScoped;

import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;

@RequestScoped
public class OptionalAuthenticationFactory extends BaseAuthenticationFactory implements
        Factory<OptionalAuthentication> {
    private Logger logger = Logger.getLogger(OptionalAuthenticationFactory.class);

    @Context
    HttpHeaders headers;

    public OptionalAuthenticationFactory() {
    }

    @Override
    public void dispose(OptionalAuthentication a) {
    }

    @Override
    public OptionalAuthentication provide() {
        return getAuthentication(headers);
    }
}
