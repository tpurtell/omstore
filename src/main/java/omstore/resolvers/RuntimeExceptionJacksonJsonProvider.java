package omstore.resolvers;

import java.io.IOException;
import java.io.InputStream;
import java.lang.annotation.Annotation;
import java.lang.reflect.Type;

import javax.ws.rs.Consumes;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.ext.MessageBodyReader;
import javax.ws.rs.ext.MessageBodyWriter;
import javax.ws.rs.ext.Provider;

import omstore.exceptions.PojoParsingException;

import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.core.Versioned;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.jaxrs.json.JacksonJsonProvider;

@Provider
@Produces(MediaType.WILDCARD)
@Consumes(MediaType.WILDCARD)
public class RuntimeExceptionJacksonJsonProvider extends JacksonJsonProvider implements
        MessageBodyReader<Object>, MessageBodyWriter<Object>, Versioned {
    public RuntimeExceptionJacksonJsonProvider() {
        ObjectMapper object_mapper = new ObjectMapper();
        object_mapper.setSerializationInclusion(Include.NON_NULL);
        setMapper(object_mapper);
    }

    @Override
    public Object readFrom(Class<Object> type, Type genericType, Annotation[] annotations,
            MediaType mediaType, MultivaluedMap<String, String> httpHeaders,
            InputStream entityStream) throws IOException {
        try {
            return super.readFrom(type, genericType, annotations, mediaType, httpHeaders,
                    entityStream);
        } catch (IOException e) {
            throw new PojoParsingException(e);
        }
    }
}
