package omstore.resolvers;

import omstore.OptionalAuthentication;

import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.UriInfo;

public class BaseAuthenticationFactory {
    protected OptionalAuthentication getAuthentication(HttpHeaders headers) {
        String authorization = headers.getHeaderString("Authorization");
        if (authorization == null)
            return new OptionalAuthentication();
        return new OptionalAuthentication(authorization);
    }
}
