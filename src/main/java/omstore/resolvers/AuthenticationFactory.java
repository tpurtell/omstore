package omstore.resolvers;

import omstore.Authentication;
import omstore.exceptions.ApiException;
import omstore.exceptions.ApiException.ApiCode;
import org.apache.log4j.Logger;
import org.glassfish.hk2.api.Factory;
import org.glassfish.jersey.process.internal.RequestScoped;

import javax.inject.Inject;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.UriInfo;

@RequestScoped
public class AuthenticationFactory extends BaseAuthenticationFactory implements
        Factory<Authentication> {
    private Logger logger = Logger.getLogger(AuthenticationFactory.class);

    @Context
    HttpHeaders headers;

    @Inject
    public AuthenticationFactory() {
    }

    @Override
    public void dispose(Authentication a) {
    }

    @Override
    public Authentication provide() {
        Authentication auth = getAuthentication(headers);
        if (auth.getAccessToken() == null)
            throw new ApiException(Status.FORBIDDEN, ApiCode.AccessDenied);
        return auth;
    }
}
