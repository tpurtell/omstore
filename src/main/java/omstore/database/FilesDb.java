package omstore.database;

import com.fasterxml.jackson.annotation.JsonProperty;
import core.Db;
import org.jvnet.hk2.annotations.Contract;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;

@Contract
public interface FilesDb extends Db {
    public static class FileInfo implements Serializable {
        @JsonProperty
        public String account;
        @JsonProperty
        public String container;
        @JsonProperty
        public String specifiedId;
        @JsonProperty
        public String relativeStorageUrl;
        @JsonProperty
        public String md5;

        @JsonProperty
        public String tag;
        @JsonProperty
        public String mimeType;
        @JsonProperty
        public long size;
        @JsonProperty
        public long uploadDate;
    }

    public static class FilesResult {
        @JsonProperty
        public Collection<FileInfo> files;
        @JsonProperty
        public byte[] continuationKey;
    }

    public FilesResult getFilesForContainer(String account, String container, String mimeType,
                                            String tag, byte[] continuationKey);

    public void addFile(String account, String container, String specifiedId,
                        String relativeStorageUrl, String tag, String mimeType, long size,
                        String md5, long uploadDate);

    public String getStoragePath(String md5);

    public void removeFile(String account, String container, String specifiedId);

    public FileInfo getFileFromSpecifiedId(String account, String container, String specifiedId);
}
