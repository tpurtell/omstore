package omstore.database;

import core.Db;
import org.jvnet.hk2.annotations.Contract;

@Contract
public interface UserDb extends Db {
    public void registerUser(String account, String password);

    public Boolean checkUser(String account);

    public Boolean checkPassword(String account, String password);
}
