package omstore.database;

import core.Db;
import org.jvnet.hk2.annotations.Contract;

@Contract
public interface TokenDb extends Db {
    public String getTokenForAccount(String account);

    public void setTokenForAccount(String account, String serverToken);

    public String getAccountForToken(String clientToken);

    public void deleteToken(String account);

    public void addAccessCode(String account, String accessCode, long expires);

    public String getAccountForAccessCode(String accessCode);

    public String checkAccountForAccessCode(String account, String accessCode);

    public String getAccessCodeForAccount(String account);
}
