package omstore.database;

import com.fasterxml.jackson.annotation.JsonProperty;
import core.Db;
import omstore.dto.SharedContainer;
import org.jvnet.hk2.annotations.Contract;

import java.util.Collection;

@Contract
public interface ContainerDb extends Db {
    public static class ContainerInfo {
        private String container;
        private String account;

        public ContainerInfo(String container, String account) {
            this.container = container;
            this.account = account;
        }

        public String getAccount() {
            return account;
        }

        public String getContainer() {
            return container;
        }
    }

    public static class ContainerResult {
        @JsonProperty
        private Collection<String> containers;
        @JsonProperty
        private byte[] continuationKey;

        public ContainerResult() {}

        public ContainerResult(Collection<String> containers, byte[] continuationKey) {
            this.containers = containers;
            this.continuationKey = continuationKey;
        }

        public Collection<String> getContainers() {
            return containers;
        }

        public byte[] getContinuationKey() {
            return continuationKey;
        }
    }

    public ContainerResult listContainersForAccount(String account, byte[] continuationKey);

    public ContainerInfo getContainerForAccount(String account, String container);

    public void addContainer(String account, String container, long creationTime);

    public void deleteContainer(String account, String container);

    public void markContainerUpdated(String account, String container, long updateTime);

    public void addSharedContainer(String ownerAccount, String container, String authToken);

    public SharedContainer getSharedContainerForAuthToken(String authToken);

    public String getAuthTokenForSharedContainer(String account, String container);
}
