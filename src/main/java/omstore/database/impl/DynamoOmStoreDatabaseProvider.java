package omstore.database.impl;

import core.DynamoUtils;
import core.ServiceConfigurationProvider;
import omstore.database.TokenDb;
import org.glassfish.hk2.api.ServiceLocator;
import org.glassfish.hk2.utilities.ServiceLocatorUtilities;

public class DynamoOmStoreDatabaseProvider implements ServiceConfigurationProvider {
    public void bind(ServiceLocator locator) {
        // register database implementations
        ServiceLocatorUtilities.addOneConstant(locator, DynamoUtils.getDynamoClient());
        ServiceLocatorUtilities.addClasses(locator, DynamoFilesDb.class);
        ServiceLocatorUtilities.addClasses(locator, DynamoContainersDb.class);
        ServiceLocatorUtilities.addClasses(locator, DynamoTokensDb.class);
        ServiceLocatorUtilities.addClasses(locator,DynamoUserDb.class);
    }
}
