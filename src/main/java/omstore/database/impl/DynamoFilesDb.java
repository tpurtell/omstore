package omstore.database.impl;

import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClient;
import com.amazonaws.services.dynamodbv2.model.*;
import com.amazonaws.services.dynamodbv2.util.Tables;
import com.google.common.collect.ImmutableMap;
import core.DynamoUtils;
import core.KeyEncoding;
import core.KeyEncoding.AccountAndContainer;
import core.LocatorIds;
import omstore.database.FilesDb;
import org.apache.log4j.Logger;
import org.jvnet.hk2.annotations.Service;

import javax.inject.Inject;
import javax.inject.Named;
import java.util.*;

@Service
public class DynamoFilesDb implements FilesDb {
    private final String FILES_TABLE;
    // hash key
    private static final String COL_ACCOUNT_CONTAINER = "AC";
    // range key
    private static final String COL_SPECIFIED_ID = "I";
    // index range key
    private static final String COL_UPLOAD_DATE = "U";
    private static final String COL_STORAGE_URL = "S";

    private static final String COL_MIME_TYPE = "M";
    private static final String COL_MD5_HASH = "H";
    private static final String COL_TAG = "T";
    private static final String COL_SIZE = "Z";

    private static final String INDEX_UPLOAD_DATE = "I_U";
    private static final String INDEX_HASH_STORAGE_PATH = "I_H_S";

    public String account;
    public String container;
    public long size;

    private final AmazonDynamoDBClient db;
    private Logger logger = Logger.getLogger(DynamoFilesDb.class);

    @Inject
    public DynamoFilesDb(AmazonDynamoDBClient db, @Named(LocatorIds.TABLE_BASE) String tableBase) {
        this.db = db;
        this.FILES_TABLE = tableBase + "F";
    }

    @Override
    public void createTables() {
        if (Tables.doesTableExist(db, FILES_TABLE)) {
            logger.info("Table already exists " + FILES_TABLE);
            return;
        }

        List<AttributeDefinition> attributeDefinitions = new ArrayList<>();
        attributeDefinitions.add(new AttributeDefinition().withAttributeName(COL_ACCOUNT_CONTAINER)
                .withAttributeType(ScalarAttributeType.B));
        attributeDefinitions.add(new AttributeDefinition().withAttributeName(COL_SPECIFIED_ID)
                .withAttributeType(ScalarAttributeType.S));
        attributeDefinitions.add(new AttributeDefinition().withAttributeName(COL_UPLOAD_DATE)
                .withAttributeType(ScalarAttributeType.B));
        attributeDefinitions.add(new AttributeDefinition().withAttributeName(COL_MD5_HASH)
                .withAttributeType(ScalarAttributeType.S));
        attributeDefinitions.add(new AttributeDefinition().withAttributeName(COL_STORAGE_URL)
                .withAttributeType(ScalarAttributeType.S));

        List<KeySchemaElement> tableKeySchema = new ArrayList<>();
        tableKeySchema.add(new KeySchemaElement().withAttributeName(COL_ACCOUNT_CONTAINER)
                .withKeyType(KeyType.HASH));
        tableKeySchema.add(new KeySchemaElement().withAttributeName(COL_SPECIFIED_ID).withKeyType(
                KeyType.RANGE));

        List<KeySchemaElement> indexKeySchema = new ArrayList<>();
        indexKeySchema.add(new KeySchemaElement().withAttributeName(COL_ACCOUNT_CONTAINER)
                .withKeyType(KeyType.HASH));
        indexKeySchema.add(new KeySchemaElement().withAttributeName(COL_UPLOAD_DATE).withKeyType(
                KeyType.RANGE));

        ProvisionedThroughput provisionedthroughput = new ProvisionedThroughput()
                .withReadCapacityUnits(50L).withWriteCapacityUnits(10L);

        Projection projection = new Projection().withProjectionType(ProjectionType.ALL);

        LocalSecondaryIndex localSecondaryIndex = new LocalSecondaryIndex()
                .withIndexName(INDEX_UPLOAD_DATE).withKeySchema(indexKeySchema)
                .withProjection(projection);

        List<KeySchemaElement> globalIndexKeySchema = new ArrayList<>();
        globalIndexKeySchema.add(new KeySchemaElement().withAttributeName(COL_MD5_HASH)
                .withKeyType(KeyType.HASH));
        globalIndexKeySchema.add(new KeySchemaElement().withAttributeName(COL_STORAGE_URL).withKeyType(
                KeyType.RANGE));

        GlobalSecondaryIndex globalSecondaryIndex = new GlobalSecondaryIndex()
                .withIndexName(INDEX_HASH_STORAGE_PATH).withKeySchema(globalIndexKeySchema)
                .withProjection(projection).withProvisionedThroughput(provisionedthroughput);

        CreateTableRequest request = new CreateTableRequest().withTableName(FILES_TABLE)
                .withKeySchema(tableKeySchema).withProvisionedThroughput(provisionedthroughput)
                .withAttributeDefinitions(attributeDefinitions)
                .withLocalSecondaryIndexes(localSecondaryIndex)
                .withGlobalSecondaryIndexes(globalSecondaryIndex);

        db.createTable(request);

        logger.info("Created table " + FILES_TABLE);

        Tables.waitForTableToBecomeActive(db, FILES_TABLE);
    }

    @Override
    public void deleteTables() {
        if (!Tables.doesTableExist(db, FILES_TABLE))
            return;

        logger.info("Deleting table " + FILES_TABLE);
        db.deleteTable(FILES_TABLE);
        DynamoUtils.waitForTableToBeDeleted(db, FILES_TABLE);
    }

    @Override
    public void addFile(String account, String container, String specifiedId,
                        String relativeStorageUrl, String tag, String mimeType, long size,
                        String md5, long uploadDate) {
        Map<String, AttributeValue> cols = new HashMap<>();
        cols.put(COL_ACCOUNT_CONTAINER, new AttributeValue().withB(KeyEncoding
                .encodeAccountAndContainer(account, container)));
        if (mimeType != null)
            cols.put(COL_MIME_TYPE, new AttributeValue().withS(mimeType));
        cols.put(COL_SIZE, new AttributeValue().withB(KeyEncoding.encodeLongBigEndian(size)));
        cols.put(COL_SPECIFIED_ID, new AttributeValue().withS(specifiedId));
        cols.put(COL_STORAGE_URL, new AttributeValue().withS(relativeStorageUrl));
        cols.put(COL_MD5_HASH, new AttributeValue().withS(md5));
        if (tag != null)
            cols.put(COL_TAG, new AttributeValue().withS(tag));
        cols.put(COL_UPLOAD_DATE,
                new AttributeValue().withB(KeyEncoding.encodeLongBigEndian(uploadDate)));
        db.putItem(FILES_TABLE, cols);
    }

    @Override
    public String getStoragePath(String md5) {
        Map<String, Condition> keyConditions = new HashMap<>();

        Condition hashKeyCondition = new Condition().withComparisonOperator(ComparisonOperator.EQ)
                .withAttributeValueList(new AttributeValue().withS(md5));
        keyConditions.put(COL_MD5_HASH, hashKeyCondition);

        QueryRequest query = new QueryRequest().withTableName(FILES_TABLE)
                .withIndexName(INDEX_HASH_STORAGE_PATH).withKeyConditions(keyConditions);

        QueryResult result = db.query(query);
        List<Map<String, AttributeValue>> items = result.getItems();
        if (items == null || items.isEmpty()) {
            return null;
        }

        return items.get(0).get(COL_STORAGE_URL).getS();
    }

    @Override
    public void removeFile(String account, String container, String specifiedId) {
        Map<String, AttributeValue> cols = new HashMap<>();
        cols.put(COL_ACCOUNT_CONTAINER, new AttributeValue().withB(KeyEncoding
                .encodeAccountAndContainer(account, container)));
        cols.put(COL_SPECIFIED_ID, new AttributeValue().withS(specifiedId));
        db.deleteItem(FILES_TABLE, cols);
    }

    @Override
    public FileInfo getFileFromSpecifiedId(String account, String container, String specifiedId) {
        GetItemResult result = db.getItem(FILES_TABLE, ImmutableMap.of(
                COL_ACCOUNT_CONTAINER, new AttributeValue().withB(KeyEncoding
                        .encodeAccountAndContainer(account, container)), COL_SPECIFIED_ID,
                new AttributeValue().withS(specifiedId)));
        if (result.getItem() == null)
            return null;
        return makeFileListFromItems(Arrays.asList(result.getItem())).iterator().next();
    }

    @Override
    public FilesResult getFilesForContainer(String account, String container, String mimeType,
                                            String tag, byte[] continuationKey) {
        Map<String, Condition> keyConditions = new HashMap<>();

        Condition hashKeyCondition = new Condition().withComparisonOperator(ComparisonOperator.EQ)
                .withAttributeValueList(
                        new AttributeValue().withB(KeyEncoding.encodeAccountAndContainer(account,
                                container)));
        keyConditions.put(COL_ACCOUNT_CONTAINER, hashKeyCondition);

        Map<String, AttributeValue> continuationKeyDecoded = DynamoUtils
                .temporaryDeserializeKey(continuationKey);
        QueryRequest query = new QueryRequest().withTableName(FILES_TABLE)
                .withIndexName(INDEX_UPLOAD_DATE).withKeyConditions(keyConditions)
                .withExclusiveStartKey(continuationKeyDecoded).withScanIndexForward(false);

        if ((tag != null && !tag.equals("")) || (mimeType != null && !mimeType.equals(""))) {
            Map<String, Condition> filterConditions = new HashMap<>();
            if (tag != null && !tag.equals("")) {
                Condition tagCondition = new Condition().withComparisonOperator(
                        ComparisonOperator.EQ).withAttributeValueList(
                        new AttributeValue().withS(tag));
                filterConditions.put(COL_TAG, tagCondition);
            }
            if (mimeType != null && !mimeType.equals("")) {
                Condition mimeTypeCondition = new Condition().withComparisonOperator(
                        ComparisonOperator.EQ).withAttributeValueList(
                        new AttributeValue().withS(mimeType));
                filterConditions.put(COL_MIME_TYPE, mimeTypeCondition);
            }
            query.withQueryFilter(filterConditions);
        }

        QueryResult result = db.query(query);
        return makeResultFromQueryResult(result);
    }

    private FilesResult makeResultFromQueryResult(QueryResult result) {
        FilesResult filesResult = new FilesResult();
        filesResult.continuationKey = DynamoUtils.temporarySerializeKey(result.getLastEvaluatedKey());
        filesResult.files = makeFileListFromItems(result.getItems());
        return filesResult;
    }

    private Collection<FileInfo> makeFileListFromItems(List<Map<String, AttributeValue>> items) {
        List<FileInfo> files = new ArrayList<>(items.size());

        for (Map<String, AttributeValue> item : items) {
            FileInfo fileInfo = new FileInfo();
            AccountAndContainer accountAndContainer = KeyEncoding.decodeAccountAndContainer(item.get(
                    COL_ACCOUNT_CONTAINER).getB());
            fileInfo.account = accountAndContainer.account;
            fileInfo.container = accountAndContainer.container;
            if (item.get(COL_MIME_TYPE) != null) {
                fileInfo.mimeType = item.get(COL_MIME_TYPE).getS();
            }
            fileInfo.relativeStorageUrl = item.get(COL_STORAGE_URL).getS();
            fileInfo.size = KeyEncoding.decodeLongBigEndian(item.get(COL_SIZE).getB());
            fileInfo.specifiedId = item.get(COL_SPECIFIED_ID).getS();
            fileInfo.md5 = item.get(COL_MD5_HASH).getS();
            if (item.get(COL_TAG) != null) {
                fileInfo.tag = item.get(COL_TAG).getS();
            }

            fileInfo.uploadDate = KeyEncoding.decodeLongBigEndian(item.get(COL_UPLOAD_DATE).getB());

            files.add(fileInfo);
        }
        return files;
    }
}
