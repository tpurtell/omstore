package omstore.database.impl;

import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClient;
import com.amazonaws.services.dynamodbv2.model.*;
import com.amazonaws.services.dynamodbv2.util.Tables;
import core.DynamoUtils;
import core.LocatorIds;
import omstore.database.ContainerDb;
import omstore.database.UserDb;
import org.apache.log4j.Logger;
import org.jvnet.hk2.annotations.Service;

import javax.inject.Inject;
import javax.inject.Named;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class DynamoUserDb implements UserDb {
    private static final String COL_ACCOUNT = "A";
    private static final String COL_PASSWORD = "P";

    private final String USER_TABLE;
    private final AmazonDynamoDBClient db;
    private Logger logger = Logger.getLogger(DynamoContainersDb.class);

    @Inject
    public DynamoUserDb(AmazonDynamoDBClient db, @Named(LocatorIds.TABLE_BASE) String tableBase) {
        this.db = db;
        this.USER_TABLE = tableBase + "U";
    }

    @Override
    public void createTables() {
        if (Tables.doesTableExist(db, USER_TABLE)) {
            logger.info("Table already exists " + USER_TABLE);
            return;
        }

        List<AttributeDefinition> attributeDefinitions = new ArrayList<>();
        attributeDefinitions.add(new AttributeDefinition().withAttributeName(COL_ACCOUNT)
                .withAttributeType(ScalarAttributeType.S));

        List<KeySchemaElement> tableKeySchema = new ArrayList<>();
        tableKeySchema.add(new KeySchemaElement().withAttributeName(COL_ACCOUNT).withKeyType(
                KeyType.HASH));

        ProvisionedThroughput provisionedthroughput = new ProvisionedThroughput()
                .withReadCapacityUnits(50L).withWriteCapacityUnits(10L);


        CreateTableRequest request = new CreateTableRequest().withTableName(USER_TABLE)
                .withKeySchema(tableKeySchema).withProvisionedThroughput(provisionedthroughput)
                .withAttributeDefinitions(attributeDefinitions);

        db.createTable(request);

        logger.info("Created table " + USER_TABLE);

        Tables.waitForTableToBecomeActive(db, USER_TABLE);
    }

    @Override
    public void deleteTables() {
        if (!Tables.doesTableExist(db, USER_TABLE)) {
            return;
        }

        logger.info("Deleting table " + USER_TABLE);
        db.deleteTable(USER_TABLE);
        DynamoUtils.waitForTableToBeDeleted(db, USER_TABLE);
    }

    @Override
    public void registerUser(String account, String password) {
        Map<String, AttributeValue> cols = new HashMap<>();
        cols.put(COL_ACCOUNT, new AttributeValue().withS(account));
        cols.put(COL_PASSWORD, new AttributeValue().withS(password));

        db.putItem(USER_TABLE, cols);
    }

    @Override
    public Boolean checkUser(String account) {
        Map<String, Condition> keyConditions = new HashMap<>();

        Condition accessCodeCondition = new Condition().withComparisonOperator(ComparisonOperator.EQ)
                .withAttributeValueList(new AttributeValue().withS(account));
        keyConditions.put(COL_ACCOUNT, accessCodeCondition);

        QueryRequest query = new QueryRequest().withTableName(USER_TABLE)
                .withKeyConditions(keyConditions);

        QueryResult result = db.query(query);
        List<Map<String, AttributeValue>> items = result.getItems();
        if (items == null || items.isEmpty()) {
            return false;
        }

        return true;
    }

    @Override
    public Boolean checkPassword(String account, String password) {
        Map<String, Condition> keyConditions = new HashMap<>();

        Condition accessCodeCondition = new Condition().withComparisonOperator(ComparisonOperator.EQ)
                .withAttributeValueList(new AttributeValue().withS(account));
        keyConditions.put(COL_ACCOUNT, accessCodeCondition);

        QueryRequest query = new QueryRequest().withTableName(USER_TABLE)
                .withKeyConditions(keyConditions);

        QueryResult result = db.query(query);
        List<Map<String, AttributeValue>> items = result.getItems();
        if (items == null || items.isEmpty()) {
            return false;
        }

        for (Map<String, AttributeValue> item : items) {
            if ((item.get(COL_PASSWORD).getS().equals(password))) {
                return true;
            }
        }
        return false;
    }
}
