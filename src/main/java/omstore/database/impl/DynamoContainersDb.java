package omstore.database.impl;

import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClient;
import com.amazonaws.services.dynamodbv2.model.*;
import com.amazonaws.services.dynamodbv2.util.Tables;
import core.DynamoUtils;
import core.LocatorIds;
import omstore.contract.ShareContainerResult;
import omstore.database.ContainerDb;
import omstore.dto.SharedContainer;
import org.apache.log4j.Logger;
import org.jvnet.hk2.annotations.Service;

import javax.inject.Inject;
import javax.inject.Named;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class DynamoContainersDb implements ContainerDb {
    private static final String COL_CONTAINER = "C";
    private static final String COL_ACCOUNT = "A";
    private static final String COL_CREATION_TIME = "CT";
    private static final String INDEX_ACCOUNT_AND_TIME = "I_A";

    private static final String COL_OWNER_ACCOUNT = "OA";
    private static final String COL_AUTH_TOKEN = "AT";
    private static final String INDEX_CONTAINER_AND_AUTH_TOKEN = "I_AT_C";

    private final String CONTAINERS_TABLE;
    private final String CONTAINERS_SHARE_TABLE;
    private final AmazonDynamoDBClient db;
    private Logger logger = Logger.getLogger(DynamoContainersDb.class);

    @Inject
    public DynamoContainersDb(AmazonDynamoDBClient db, @Named(LocatorIds.TABLE_BASE) String tableBase) {
        this.db = db;
        this.CONTAINERS_TABLE = tableBase + "C";
        this.CONTAINERS_SHARE_TABLE = tableBase + "CS";
    }

    @Override
    public void createTables() {
        if (Tables.doesTableExist(db, CONTAINERS_TABLE)) {
            logger.info("Table already exists " + CONTAINERS_TABLE);
            return;
        }

        createContainerTable();
        createContainerSharedTable();
    }

    private void createContainerTable() {
        List<AttributeDefinition> attributeDefinitions = new ArrayList<>();
        attributeDefinitions.add(new AttributeDefinition().withAttributeName(COL_CONTAINER)
                .withAttributeType(ScalarAttributeType.S));
        attributeDefinitions.add(new AttributeDefinition().withAttributeName(COL_ACCOUNT)
                .withAttributeType(ScalarAttributeType.S));
        attributeDefinitions.add(new AttributeDefinition().withAttributeName(COL_CREATION_TIME)
                .withAttributeType(ScalarAttributeType.N));

        List<KeySchemaElement> tableKeySchema = new ArrayList<>();
        tableKeySchema.add(new KeySchemaElement().withAttributeName(COL_ACCOUNT).withKeyType(
                KeyType.HASH));
        tableKeySchema.add(new KeySchemaElement().withAttributeName(COL_CONTAINER).withKeyType(
                KeyType.RANGE));

        List<KeySchemaElement> indexKeySchema = new ArrayList<>();
        indexKeySchema.add(new KeySchemaElement().withAttributeName(COL_ACCOUNT).withKeyType(
                KeyType.HASH));
        indexKeySchema.add(new KeySchemaElement().withAttributeName(COL_CREATION_TIME).withKeyType(
                KeyType.RANGE));

        ProvisionedThroughput provisionedthroughput = new ProvisionedThroughput()
                .withReadCapacityUnits(50L).withWriteCapacityUnits(10L);

        Projection projection = new Projection().withProjectionType(ProjectionType.ALL);

        LocalSecondaryIndex localSecondaryIndex = new LocalSecondaryIndex()
                .withIndexName(INDEX_ACCOUNT_AND_TIME).withKeySchema(indexKeySchema)
                .withProjection(projection);

        CreateTableRequest request = new CreateTableRequest().withTableName(CONTAINERS_TABLE)
                .withKeySchema(tableKeySchema).withProvisionedThroughput(provisionedthroughput)
                .withAttributeDefinitions(attributeDefinitions)
                .withLocalSecondaryIndexes(localSecondaryIndex);

        db.createTable(request);

        logger.info("Created table " + CONTAINERS_TABLE);

        Tables.waitForTableToBecomeActive(db, CONTAINERS_TABLE);
    }

    private void createContainerSharedTable() {
        List<AttributeDefinition> attributeDefinitions = new ArrayList<>();
        attributeDefinitions.add(new AttributeDefinition().withAttributeName(COL_CONTAINER)
                .withAttributeType(ScalarAttributeType.S));
        attributeDefinitions.add(new AttributeDefinition().withAttributeName(COL_OWNER_ACCOUNT)
                .withAttributeType(ScalarAttributeType.S));
        attributeDefinitions.add(new AttributeDefinition().withAttributeName(COL_AUTH_TOKEN)
                .withAttributeType(ScalarAttributeType.S));

        List<KeySchemaElement> tableKeySchema = new ArrayList<>();
        tableKeySchema.add(new KeySchemaElement().withAttributeName(COL_OWNER_ACCOUNT).withKeyType(
                KeyType.HASH));
        tableKeySchema.add(new KeySchemaElement().withAttributeName(COL_CONTAINER).withKeyType(
                KeyType.RANGE));

        List<KeySchemaElement> indexKeySchema = new ArrayList<>();
        indexKeySchema.add(new KeySchemaElement().withAttributeName(COL_AUTH_TOKEN).withKeyType(
                KeyType.HASH));
        indexKeySchema.add(new KeySchemaElement().withAttributeName(COL_CONTAINER).withKeyType(
                KeyType.RANGE));

        ProvisionedThroughput provisionedthroughput = new ProvisionedThroughput()
                .withReadCapacityUnits(50L).withWriteCapacityUnits(10L);

        Projection projection = new Projection().withProjectionType(ProjectionType.ALL);

        GlobalSecondaryIndex globalSecondaryIndex = new GlobalSecondaryIndex()
                .withIndexName(INDEX_CONTAINER_AND_AUTH_TOKEN).withKeySchema(indexKeySchema)
                .withProjection(projection).withProvisionedThroughput(provisionedthroughput);

        CreateTableRequest request = new CreateTableRequest().withTableName(CONTAINERS_SHARE_TABLE)
                .withKeySchema(tableKeySchema).withProvisionedThroughput(provisionedthroughput)
                .withAttributeDefinitions(attributeDefinitions)
                .withGlobalSecondaryIndexes(globalSecondaryIndex);

        db.createTable(request);

        logger.info("Created table " + CONTAINERS_SHARE_TABLE);

        Tables.waitForTableToBecomeActive(db, CONTAINERS_SHARE_TABLE);
    }

    @Override
    public void deleteTables() {
        deleteContainerTable();
        deleteSharedContainerTable();
    }

    private void deleteContainerTable() {
        if (!Tables.doesTableExist(db, CONTAINERS_TABLE)) {
            return;
        }

        logger.info("Deleting table " + CONTAINERS_TABLE);
        db.deleteTable(CONTAINERS_TABLE);
        DynamoUtils.waitForTableToBeDeleted(db, CONTAINERS_TABLE);
    }

    private void deleteSharedContainerTable() {
        if (!Tables.doesTableExist(db, CONTAINERS_SHARE_TABLE)) {
            return;
        }

        logger.info("Deleting table " + CONTAINERS_SHARE_TABLE);
        db.deleteTable(CONTAINERS_SHARE_TABLE);
        DynamoUtils.waitForTableToBeDeleted(db, CONTAINERS_SHARE_TABLE);
    }

    @Override
    public ContainerResult listContainersForAccount(String account, byte[] continuationKey) {
        List<String> containers = new ArrayList<>();

        Map<String, Condition> keyConditions = new HashMap<>();

        Condition hashKeyCondition = new Condition().withComparisonOperator(ComparisonOperator.EQ)
                .withAttributeValueList(
                        new AttributeValue().withS(account));
        keyConditions.put(COL_ACCOUNT, hashKeyCondition);

        Map<String, AttributeValue> continuationKeyDecoded = DynamoUtils
                .temporaryDeserializeKey(continuationKey);
        QueryRequest query = new QueryRequest().withTableName(CONTAINERS_TABLE)
                .withIndexName(INDEX_ACCOUNT_AND_TIME).withKeyConditions(keyConditions)
                .withExclusiveStartKey(continuationKeyDecoded).withScanIndexForward(false);

        QueryResult result = db.query(query);
        List<Map<String, AttributeValue>> items = result.getItems();
        for (Map<String, AttributeValue> item : items) {
            containers.add(item.get(COL_CONTAINER).getS());
        }

        return new ContainerResult(containers, DynamoUtils.temporarySerializeKey(result.getLastEvaluatedKey()));
    }

    @Override
    public ContainerInfo getContainerForAccount(String account, String container) {
        Map<String, AttributeValue> cols = new HashMap<>();
        cols.put(COL_ACCOUNT, new AttributeValue().withS(account));
        cols.put(COL_CONTAINER, new AttributeValue().withS(container));

        Map<String, AttributeValue> item = db.getItem(CONTAINERS_TABLE, cols).getItem();
        if (item == null) {
            return null;
        }
        return new ContainerInfo(item.get(COL_CONTAINER).getS(), item.get(COL_ACCOUNT).getS());
    }

    @Override
    public void addContainer(String account, String container, long creationTime) {
        Map<String, AttributeValue> cols = new HashMap<>();
        cols.put(COL_ACCOUNT, new AttributeValue().withS(account));
        cols.put(COL_CONTAINER, new AttributeValue().withS(container));
        cols.put(COL_CREATION_TIME, new AttributeValue().withN(String.valueOf(creationTime)));

        db.putItem(CONTAINERS_TABLE, cols);
    }

    @Override
    public void deleteContainer(String account, String container) {
        Map<String, AttributeValue> cols = new HashMap<>();
        cols.put(COL_ACCOUNT, new AttributeValue().withS(account));
        cols.put(COL_CONTAINER, new AttributeValue().withS(container));

        db.deleteItem(CONTAINERS_TABLE, cols);
    }

    @Override
    public void markContainerUpdated(String account, String container, long updateTime) {
        throw new RuntimeException("Not implemented!");
    }

    @Override
    public void addSharedContainer(String ownerAccount, String container, String authToken) {
        Map<String, AttributeValue> cols = new HashMap<>();
        cols.put(COL_OWNER_ACCOUNT, new AttributeValue().withS(ownerAccount));
        cols.put(COL_CONTAINER, new AttributeValue().withS(container));
        cols.put(COL_AUTH_TOKEN, new AttributeValue().withS(authToken));

        db.putItem(CONTAINERS_SHARE_TABLE, cols);
    }

    @Override
    public SharedContainer getSharedContainerForAuthToken(String authToken) {
        Map<String, Condition> keyConditions = new HashMap<>();

        Condition authTokenCondition = new Condition().withComparisonOperator(ComparisonOperator.EQ)
                .withAttributeValueList(new AttributeValue().withS(authToken));
        keyConditions.put(COL_AUTH_TOKEN, authTokenCondition);

        QueryRequest query = new QueryRequest().withTableName(CONTAINERS_SHARE_TABLE)
                .withIndexName(INDEX_CONTAINER_AND_AUTH_TOKEN).withKeyConditions(keyConditions).withLimit(1);

        QueryResult result = db.query(query);
        List<Map<String, AttributeValue>> items = result.getItems();
        if (items == null || items.isEmpty()) {
            return null;
        }

        return new SharedContainer(items.get(0).get(COL_OWNER_ACCOUNT).getS(),
                items.get(0).get(COL_CONTAINER).getS());
    }

    @Override
    public String getAuthTokenForSharedContainer(String account, String container) {
        Map<String, AttributeValue> cols = new HashMap<>();
        cols.put(COL_OWNER_ACCOUNT, new AttributeValue().withS(account));
        cols.put(COL_CONTAINER, new AttributeValue().withS(container));

        Map<String, AttributeValue> item = db.getItem(CONTAINERS_SHARE_TABLE, cols).getItem();
        if (item == null) {
            return null;
        }

        return item.get(COL_AUTH_TOKEN).getS();
    }
}
