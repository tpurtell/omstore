package omstore.database.impl;

import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClient;
import com.amazonaws.services.dynamodbv2.model.*;
import com.amazonaws.services.dynamodbv2.util.Tables;
import core.DynamoUtils;
import core.LocatorIds;
import omstore.database.TokenDb;
import org.apache.log4j.Logger;

import javax.inject.Inject;
import javax.inject.Named;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DynamoTokensDb implements TokenDb {
    private static final String COL_TOKEN = "T";
    private static final String COL_CODE = "C";
    private static final String COL_ACCOUNT = "A";
    private static final String COL_VALID_UNTIL = "VU";
    private static final String INDEX_ACCOUNT_CODE = "I_A_C";
    private static final String INDEX_ACCOUNT_TOKEN = "I_A_T";
    private final String TOKENS_TABLE;
    private final String CODES_TABLE;
    private final AmazonDynamoDBClient db;
    private Logger logger = Logger.getLogger(DynamoTokensDb.class);

    @Inject
    public DynamoTokensDb(AmazonDynamoDBClient db, @Named(LocatorIds.TABLE_BASE) String tableBase) {
        this.db = db;
        this.TOKENS_TABLE = tableBase + "T";
        this.CODES_TABLE = tableBase + "CO";
    }

    private void createTokensTable() {
        if (Tables.doesTableExist(db, TOKENS_TABLE)) {
            logger.info("Table already exists " + TOKENS_TABLE);
            return;
        }

        List<AttributeDefinition> attributeDefinitions = new ArrayList<>();
        attributeDefinitions.add(new AttributeDefinition().withAttributeName(COL_TOKEN)
                .withAttributeType(ScalarAttributeType.S));
        attributeDefinitions.add(new AttributeDefinition().withAttributeName(COL_ACCOUNT)
                .withAttributeType(ScalarAttributeType.S));
        List<KeySchemaElement> tableKeySchema = new ArrayList<>();
        tableKeySchema.add(new KeySchemaElement().withAttributeName(COL_TOKEN)
                .withKeyType(KeyType.HASH));

        List<KeySchemaElement> indexKeySchema = new ArrayList<>();
        indexKeySchema.add(new KeySchemaElement().withAttributeName(COL_ACCOUNT)
                .withKeyType(KeyType.HASH));
        indexKeySchema.add(new KeySchemaElement().withAttributeName(COL_TOKEN).withKeyType(
                KeyType.RANGE));


        ProvisionedThroughput provisionedthroughput = new ProvisionedThroughput()
                .withReadCapacityUnits(50L).withWriteCapacityUnits(10L);

        Projection projection = new Projection().withProjectionType(ProjectionType.ALL);
        GlobalSecondaryIndex globalSecondaryIndex = new GlobalSecondaryIndex()
                .withIndexName(INDEX_ACCOUNT_TOKEN).withKeySchema(indexKeySchema)
                .withProjection(projection).withProvisionedThroughput(provisionedthroughput);

        CreateTableRequest request = new CreateTableRequest().withTableName(TOKENS_TABLE)
                .withKeySchema(tableKeySchema).withProvisionedThroughput(provisionedthroughput)
                .withAttributeDefinitions(attributeDefinitions)
                .withGlobalSecondaryIndexes(globalSecondaryIndex);

        db.createTable(request);

        logger.info("Created table " + TOKENS_TABLE);
        Tables.waitForTableToBecomeActive(db, TOKENS_TABLE);
    }


    private void createCodesTable() {
        if (Tables.doesTableExist(db, CODES_TABLE)) {
            logger.info("Table already exists " + CODES_TABLE);
            return;
        }

        List<AttributeDefinition> attributeDefinitions = new ArrayList<>();
        attributeDefinitions.add(new AttributeDefinition().withAttributeName(COL_CODE)
                .withAttributeType(ScalarAttributeType.S));
        attributeDefinitions.add(new AttributeDefinition().withAttributeName(COL_ACCOUNT)
                .withAttributeType(ScalarAttributeType.S));

        List<KeySchemaElement> tableKeySchema = new ArrayList<>();
        tableKeySchema.add(new KeySchemaElement().withAttributeName(COL_CODE)
                .withKeyType(KeyType.HASH));

        List<KeySchemaElement> indexKeySchema = new ArrayList<>();
        indexKeySchema.add(new KeySchemaElement().withAttributeName(COL_ACCOUNT)
                .withKeyType(KeyType.HASH));
        indexKeySchema.add(new KeySchemaElement().withAttributeName(COL_CODE).withKeyType(
                KeyType.RANGE));


        ProvisionedThroughput provisionedthroughput = new ProvisionedThroughput()
                .withReadCapacityUnits(50L).withWriteCapacityUnits(10L);

        Projection projection = new Projection().withProjectionType(ProjectionType.ALL);
        GlobalSecondaryIndex globalSecondaryIndex = new GlobalSecondaryIndex()
                .withIndexName(INDEX_ACCOUNT_CODE).withKeySchema(indexKeySchema)
                .withProjection(projection).withProvisionedThroughput(provisionedthroughput);

        CreateTableRequest request = new CreateTableRequest().withTableName(CODES_TABLE)
                .withKeySchema(tableKeySchema).withProvisionedThroughput(provisionedthroughput)
                .withAttributeDefinitions(attributeDefinitions)
                .withGlobalSecondaryIndexes(globalSecondaryIndex);

        db.createTable(request);

        logger.info("Created table " + CODES_TABLE);

        Tables.waitForTableToBecomeActive(db, CODES_TABLE);
    }

    @Override
    public void createTables() {
        createTokensTable();
        createCodesTable();
    }

    @Override
    public void deleteTables() {
        deleteTokensTable();
        deleteCodesTable();
    }

    private void deleteTokensTable() {
        if (!Tables.doesTableExist(db, TOKENS_TABLE)) {
            return;
        }

        logger.info("Deleting table " + TOKENS_TABLE);
        db.deleteTable(TOKENS_TABLE);
        DynamoUtils.waitForTableToBeDeleted(db, TOKENS_TABLE);
    }

    private void deleteCodesTable() {
        if (!Tables.doesTableExist(db, CODES_TABLE)) {
            return;
        }

        logger.info("Deleting table " + CODES_TABLE);
        db.deleteTable(CODES_TABLE);
        DynamoUtils.waitForTableToBeDeleted(db, CODES_TABLE);
    }

    @Override
    public String getTokenForAccount(String account) {
        Map<String, Condition> keyConditions = new HashMap<>();

        Condition accountKeyCondition = new Condition().withComparisonOperator(ComparisonOperator.EQ)
                .withAttributeValueList(new AttributeValue().withS(account));
        keyConditions.put(COL_ACCOUNT, accountKeyCondition);

        QueryRequest query = new QueryRequest().withTableName(TOKENS_TABLE)
                .withIndexName(INDEX_ACCOUNT_TOKEN).withKeyConditions(keyConditions).withLimit(1);

        QueryResult result = db.query(query);
        List<Map<String, AttributeValue>> items = result.getItems();
        if (items == null || items.isEmpty()) {
            return null;
        }

        return items.get(0).get(COL_TOKEN).getS();
    }

    @Override
    public String getAccessCodeForAccount(String account) {
        Map<String, Condition> keyConditions = new HashMap<>();

        Condition accountKeyCondition = new Condition().withComparisonOperator(ComparisonOperator.EQ)
                .withAttributeValueList(new AttributeValue().withS(account));
        keyConditions.put(COL_ACCOUNT, accountKeyCondition);

        QueryRequest query = new QueryRequest().withTableName(CODES_TABLE)
                .withIndexName(INDEX_ACCOUNT_CODE).withKeyConditions(keyConditions).withLimit(1);

        QueryResult result = db.query(query);
        List<Map<String, AttributeValue>> items = result.getItems();
        if (items == null || items.isEmpty()) {
            return null;
        }

        return items.get(0).get(COL_CODE).getS();
    }

    @Override
    public void setTokenForAccount(String account, String serverToken) {
        Map<String, AttributeValue> cols = new HashMap<>();
        cols.put(COL_ACCOUNT, new AttributeValue().withS(account));
        cols.put(COL_TOKEN, new AttributeValue().withS(serverToken));

        db.putItem(TOKENS_TABLE, cols);
    }

    @Override
    public String getAccountForToken(String token) {
        Map<String, Condition> keyConditions = new HashMap<>();

        Condition hashKeyCondition = new Condition().withComparisonOperator(ComparisonOperator.EQ)
                .withAttributeValueList(new AttributeValue().withS(token));
        keyConditions.put(COL_TOKEN, hashKeyCondition);

        QueryRequest query = new QueryRequest().withTableName(TOKENS_TABLE)
                .withKeyConditions(keyConditions);

        QueryResult result = db.query(query);
        List<Map<String, AttributeValue>> items = result.getItems();
        if (items == null || items.isEmpty()) {
            return null;
        }

        return items.get(0).get(COL_ACCOUNT).getS();
    }

    @Override
    public void addAccessCode(String account, String accessCode, long expires) {
        Map<String, AttributeValue> cols = new HashMap<>();
        cols.put(COL_ACCOUNT, new AttributeValue().withS(account));
        cols.put(COL_CODE, new AttributeValue().withS(accessCode));
        cols.put(COL_VALID_UNTIL, new AttributeValue().withN(String.valueOf(expires)));

        db.putItem(CODES_TABLE, cols);
    }

    @Override
    public String getAccountForAccessCode(String accessCode) {
        Map<String, Condition> keyConditions = new HashMap<>();

        Condition accessCodeCondition = new Condition().withComparisonOperator(ComparisonOperator.EQ)
                .withAttributeValueList(new AttributeValue().withS(accessCode));
        keyConditions.put(COL_CODE, accessCodeCondition);

        QueryRequest query = new QueryRequest().withTableName(CODES_TABLE)
                .withKeyConditions(keyConditions);

        QueryResult result = db.query(query);
        List<Map<String, AttributeValue>> items = result.getItems();
        if (items == null || items.isEmpty()) {
            return null;
        }

        return items.get(0).get(COL_ACCOUNT).getS();
    }

    @Override
    public String checkAccountForAccessCode(String account, String accessCode) {
        Map<String, Condition> keyConditions = new HashMap<>();

        Condition accessCodeCondition = new Condition().withComparisonOperator(ComparisonOperator.EQ)
                .withAttributeValueList(new AttributeValue().withS(accessCode));
        keyConditions.put(COL_CODE, accessCodeCondition);

        QueryRequest query = new QueryRequest().withTableName(CODES_TABLE)
                .withKeyConditions(keyConditions);

        QueryResult result = db.query(query);
        List<Map<String, AttributeValue>> items = result.getItems();
        if (items == null || items.isEmpty()) {
            return null;
        }

        for (Map<String, AttributeValue> item : items) {
            if ((item.get(COL_ACCOUNT).getS().equals(account))) {
                return item.get(COL_VALID_UNTIL).getN();
            }
        }

        return null;
    }

    @Override
    public void deleteToken(String account) {
        Map<String, Condition> keyConditions = new HashMap<>();

        Condition hashKeyCondition = new Condition().withComparisonOperator(ComparisonOperator.EQ)
                .withAttributeValueList(new AttributeValue().withS(account));
        keyConditions.put(COL_ACCOUNT, hashKeyCondition);

        QueryRequest query = new QueryRequest().withTableName(TOKENS_TABLE)
                .withIndexName(INDEX_ACCOUNT_TOKEN).withKeyConditions(keyConditions);

        QueryResult result = db.query(query);
        List<Map<String, AttributeValue>> items = result.getItems();
        if (items == null || items.isEmpty()) {
            return;
        }

        Map<String, AttributeValue> cols = new HashMap<>();
        cols.put(COL_TOKEN, new AttributeValue().withS(items.get(0).get(COL_TOKEN).getS()));
        db.deleteItem(TOKENS_TABLE, cols);
    }
}
