package omstore.service;

import omstore.Authentication;
import org.jvnet.hk2.annotations.Contract;

import static omstore.Authentication.AuthenticationResponse;

@Contract
public interface AuthenticationService {
    public String generateToken();

    public AuthenticationResponse validateRequest(Authentication auth, String account);
}
