package omstore.service.impl;

import org.glassfish.hk2.api.ServiceLocator;
import org.glassfish.hk2.utilities.ServiceLocatorUtilities;

import core.LocatorIds;

import java.util.concurrent.ConcurrentHashMap;

public class DiskOmStoreProvider extends OmStoreProvider {
    public void bind(ServiceLocator locator) {
        super.bind(locator);
        ServiceLocatorUtilities.addOneConstant(locator, "storage", LocatorIds.LOCAL_STORAGE_PATH,
                String.class);
        ServiceLocatorUtilities.addOneConstant(locator, new ConcurrentHashMap<String, String>(), "cookieMap",
                ConcurrentHashMap.class);
        ServiceLocatorUtilities.addClasses(locator, DiskFileContentService.class);
    }
}
