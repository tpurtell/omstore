package omstore.service.impl;

import core.ServiceConfigurationProvider;
import omstore.database.ContainerDb;
import omstore.database.FilesDb;
import omstore.database.TokenDb;
import omstore.database.UserDb;
import org.glassfish.hk2.api.ServiceLocator;
import org.glassfish.hk2.utilities.ServiceLocatorUtilities;

public class OmStoreProvider implements ServiceConfigurationProvider {

    public void bind(ServiceLocator locator) {
        createTables(locator);
        ServiceLocatorUtilities.addClasses(locator,OmStoreAuthentication.class);
        // Add other common service implementations here
    }

    public void createTables(ServiceLocator locator) {
        // create the needed tables here
        locator.getService(FilesDb.class).createTables();
        locator.getService(ContainerDb.class).createTables();
        locator.getService(TokenDb.class).createTables();
        locator.getService(UserDb.class).createTables();
    }
}
