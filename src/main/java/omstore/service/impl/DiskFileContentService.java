package omstore.service.impl;

import com.google.common.io.ByteStreams;
import core.LocatorIds;
import omstore.exceptions.FileContentException.FileDoesntExist;
import omstore.service.FileContentService;
import org.jvnet.hk2.annotations.Service;

import javax.inject.Inject;
import javax.inject.Named;
import java.io.*;

@Service
public class DiskFileContentService implements FileContentService {

    private String localStoragePath;

    @Inject
    public DiskFileContentService(@Named(LocatorIds.LOCAL_STORAGE_PATH) String localStoragePath) {
        this.localStoragePath = localStoragePath;
        new File(localStoragePath).mkdirs();
    }

    @Override
    public InputStream openContent(String relativeStorageUrl) {
        try {
            return new FileInputStream(makeFileForPath(relativeStorageUrl));
        } catch (FileNotFoundException e) {
            throw new FileDoesntExist(e);
        }
    }

    @Override
    public void writeContent(String relativeStorageUrl, InputStream content) throws IOException {
        File forPath = makeFileForPath(relativeStorageUrl);
        forPath.getParentFile().mkdirs();
        FileOutputStream destination = new FileOutputStream(forPath);
        try {
            ByteStreams.copy(content, destination);
        } finally {
            try {
                destination.close();
            } catch (IOException e) {
            }
        }
    }

    @Override
    public void deleteContent(String relativeStorageUrl) {
        makeFileForPath(relativeStorageUrl).delete();
    }

    @Override
    public void moveContent(String sourceStorageUrl, String destinationStorageUrl) throws IOException {
        InputStream inputStream = this.openContent(sourceStorageUrl);
        this.writeContent(destinationStorageUrl, inputStream);
        this.deleteContent(sourceStorageUrl);
    }

    private File makeFileForPath(String relativeStorageUrl) {
        return new File(localStoragePath, relativeStorageUrl);
    }
}
