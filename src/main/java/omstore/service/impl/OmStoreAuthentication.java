package omstore.service.impl;

import core.RandomData;
import omstore.Authentication;
import omstore.database.TokenDb;
import omstore.service.AuthenticationService;

import javax.inject.Inject;

import static omstore.Authentication.AuthenticationResponse;

public class OmStoreAuthentication implements AuthenticationService {
    @Inject
    public OmStoreAuthentication() {

    }

    @Inject
    TokenDb tokenDb;

    @Override
    public String generateToken() {
        return RandomData.randomString(10, 20);

    }

    @Override
    public AuthenticationResponse validateRequest(Authentication auth, String account) {
        String accessToken = auth.getAccessToken();
        String fetchedAccount = tokenDb.getAccountForToken(accessToken);
        if (fetchedAccount != null && account.equals(fetchedAccount)) {
            return AuthenticationResponse.None;
        }

        return AuthenticationResponse.Forbidden;
    }
}
