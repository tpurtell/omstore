package omstore.service;

import java.io.IOException;
import java.io.InputStream;

import org.jvnet.hk2.annotations.Contract;

@Contract
public interface FileContentService {
    public InputStream openContent(String relativeStorageUrl);

    public void writeContent(String relativeStorageUrl, InputStream content) throws IOException;

    public void deleteContent(String relativeStorageUrl);

    public void moveContent(String sourceStorageUrl, String destinationStorageUrl) throws IOException;
}
