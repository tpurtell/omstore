package omstore.dto;

public class SharedContainer {
    private String ownerAccount;
    private String container;

    public SharedContainer(String ownerAccount, String container) {
        this.ownerAccount = ownerAccount;
        this.container = container;
    }

    public String getOwnerAccount() {
        return ownerAccount;
    }

    public String getContainer() {
        return container;
    }
}
