package omstore.http;

import omstore.database.TokenDb;
import omstore.service.AuthenticationService;

import javax.inject.Inject;
import javax.inject.Singleton;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;


import org.apache.oltu.oauth2.common.exception.OAuthProblemException;
import org.apache.oltu.oauth2.common.exception.OAuthSystemException;

import java.util.ArrayList;
import java.util.List;

@Singleton
@Path("/api/authorize")
public class Authorization {
    @Inject
    TokenDb tokenDb;
    @Inject
    AuthenticationService authService;

    public Authorization() {
    }

    @GET
    @Produces("application/json")
    public Response generateToken(@QueryParam("account") String account,
                                  @QueryParam("password") String password) throws OAuthProblemException, OAuthSystemException {
        String token = authService.generateToken();

        tokenDb.setTokenForAccount(account, token);

        return Response.ok(token).build();
    }
}
