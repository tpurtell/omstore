package omstore.http;

import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.Logger;

import omstore.Authentication;
import omstore.OptionalAuthentication;
import omstore.resolvers.AuthenticationFactory;
import omstore.resolvers.OptionalAuthenticationFactory;

import org.glassfish.hk2.utilities.binding.AbstractBinder;
import org.glassfish.jersey.server.ResourceConfig;

import core.LogConfigurator;

public class OmStoreHttpApi extends ResourceConfig {
    static {
        // Get the root logger
        Logger rootLogger = Logger.getLogger("");
        for (Handler handler : rootLogger.getHandlers()) {
            // Change log level of default handler(s) of root logger
            // The paranoid would check that this is the ConsoleHandler ;)
            handler.setLevel(Level.FINE);
        }
        // Set root logger level
        rootLogger.setLevel(Level.FINE);
    }

    public OmStoreHttpApi() {
        // deal with java.util.logging
        LogConfigurator.configure();
        packages("omstore.exceptions");
        packages("omstore.http");
        packages("omstore.resolvers");
        packages("omstore.http.OAuth.common");
        packages("omstore.http.oauth2.endpoints");

        register(new AbstractBinder() {
            @Override
            protected void configure() {
                bindFactory(AuthenticationFactory.class).to(Authentication.class);
                bindFactory(OptionalAuthenticationFactory.class).to(OptionalAuthentication.class);
            }
        });
    }
}
