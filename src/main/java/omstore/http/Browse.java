package omstore.http;

import core.jxp.JxpProcessor;
import core.jxp.JxpTemplateProvider;
import omstore.database.ContainerDb;
import omstore.dto.SharedContainer;
import omstore.util.URIUtils;
import org.apache.http.client.utils.URIBuilder;

import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;
import javax.ws.rs.*;
import javax.ws.rs.core.*;
import javax.ws.rs.core.Response.*;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.concurrent.ConcurrentHashMap;

import static javax.ws.rs.core.Response.*;

@Singleton
@Path("/")
public class Browse {
    @Inject
    ContainerDb containerDb;

    private JxpTemplateProvider jxp;
    private ConcurrentHashMap<String, String> cookieMap;

    public class TemplateModel {
        public String id;

        public TemplateModel(String id) {
            this.id = id;
        }
    }

    @Inject
    public Browse(JxpTemplateProvider jxp, @Named("cookieMap") ConcurrentHashMap cookieMap) {
        this.jxp = jxp;
        this.cookieMap = cookieMap;
    }

    @GET
    @Path("browse")
    @Produces(MediaType.TEXT_HTML)
    public Response browseContent(@CookieParam(value = "account") String account,
                                  @CookieParam(value = "authToken") String authToken, @Context HttpHeaders headers)
            throws URISyntaxException {
        if (account == null || authToken == null || !cookieMap.containsKey(account) ||
                !cookieMap.get(account).equals(authToken)) {
            ResponseBuilder responseBuilder = seeOther(URIUtils.createURI("/user?redirect_uri=/browse", headers));
            return responseBuilder.status(Status.SEE_OTHER).build();
        }

        JxpProcessor home = jxp.instantiate("/templates/home.jxp");
        return ok(home.evaluate()).build();
    }

    @GET
    @Path("browse/chats/{containerId}")
    @Produces(MediaType.TEXT_HTML)
    public Response browseChat(@CookieParam(value = "account") String account,
                               @CookieParam(value = "authToken") String authToken,
                               @PathParam("containerId") String containerId,
                               @QueryParam("sharedAuthToken") String sharedAuthToken,
                               @Context HttpHeaders headers) throws URISyntaxException {
        if (containerId == null) {
            throw new NotFoundException();
        }

        if (sharedAuthToken != null) {
            SharedContainer sharedContainer = containerDb.getSharedContainerForAuthToken(sharedAuthToken);
            if (sharedContainer == null) {
                throw new NotFoundException();
            }

            JxpProcessor home = jxp.instantiate("/templates/chat_data.jxp", new TemplateModel(containerId));
            return ok(home.evaluate()).cookie(new NewCookie("sharedAccount", sharedContainer.getOwnerAccount()),
                    new NewCookie("sharedAuthToken", sharedAuthToken)).build();
        } else if (account == null || authToken == null || !cookieMap.containsKey(account) ||
                !cookieMap.get(account).equals(authToken)) {
            ResponseBuilder responseBuilder = seeOther(URIUtils
                    .createURI("/user?redirect_uri=/browse/chats/" + containerId, headers));
            return responseBuilder.status(Status.SEE_OTHER).build();
        } else if (containerDb.getContainerForAccount(account, containerId) == null) {
            throw new NotFoundException();
        }

        JxpProcessor home = jxp.instantiate("/templates/chat_data.jxp", new TemplateModel(containerId));
        return ok(home.evaluate()).build();
    }
}
