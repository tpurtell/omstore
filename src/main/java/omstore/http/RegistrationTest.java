package omstore.http;

import com.google.common.hash.Hashing;
import com.google.common.io.BaseEncoding;
import core.RandomData;
import core.WebClient;
import omstore.RegistrationTestRunner;
import omstore.contract.ShareContainerResult;
import omstore.database.ContainerDb.ContainerResult;
import omstore.database.FilesDb.FileInfo;
import omstore.database.FilesDb.FilesResult;
import omstore.http.oauth2.AccessTokenResponse;
import omstore.util.Order;
import omstore.util.OrderedRunner;
import org.bouncycastle.util.encoders.UrlBase64;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedHashMap;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import java.io.ByteArrayInputStream;
import java.util.ArrayList;

import static core.RandomData.randomBytes;
import static core.RandomData.randomWebSafeString;
import static omstore.http.oauth2.Common.CLIENT_ID;
import static omstore.http.oauth2.Common.CLIENT_SECRET;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

@RunWith(OrderedRunner.class)
public class RegistrationTest {
    private Client client;
    private String url;
    private String account;
    private String password;
    private String adminKey;
    private static String accessToken;
    private String redirectUri;

    @Before
    public void setup() {
        System.setProperty("sun.net.http.allowRestrictedHeaders", "true");
        url = RegistrationTestRunner.url;
        client = WebClient.getWebClient();
        account = "test";
        password = "test";
        adminKey = RegistrationTestRunner.adminKey;
    }

    @Test
    @Order(order = 1)
    public void shouldRegisterUser() {
        System.out.println("Creating user account ...");
        MultivaluedMap<String, String> formData = new MultivaluedHashMap<>();
        formData.add("account", account);
        formData.add("password", password);
        formData.add("adminKey", adminKey);

        Response response = client.target(url).path("user").path("register")
                .request().post(Entity.entity(formData, MediaType.APPLICATION_FORM_URLENCODED));

        assertEquals(Status.OK, Status.fromStatusCode(response.getStatus()));
        System.out.println("Success");
    }

    @Test
    @Order(order = 2)
    public void shouldFollowOAuthFlow() {
        String code = oAuthFetchCode();

        accessToken = fetchAccessToken(code);
    }

    @Test
    @Order(order = 3)
    public void shouldPutFile() {
        System.out.println("Creating a file ...");
        String container = RandomData.randomWebSafeString(20);
        String specifiedId = randomWebSafeString(16);
        byte[] data = randomBytes(4096, 8192);
        ByteArrayInputStream in = new ByteArrayInputStream(data);
        String md5 = BaseEncoding.base64().encode(Hashing.md5().hashBytes(data).asBytes());

        Response response = client.target(url).path("api").path("file").path(account)
                .path(container).path(specifiedId).request().header("Content-MD5", md5)
                .header("Authorization", accessToken)
                .header("Expect", "100-continue")
                .header("Content-Length", data.length)
                .put(Entity.entity(in, MediaType.APPLICATION_OCTET_STREAM));

        assertEquals(Status.OK, Status.fromStatusCode(response.getStatus()));
        System.out.println("Success");
    }

    @Test
    @Order(order = 4)
    public void shouldFetchFile() {
        System.out.println("Fetching a file ...");
        byte[] data = randomBytes(4096, 8192);
        ByteArrayInputStream in = new ByteArrayInputStream(data);
        String container = randomWebSafeString(16);
        String specifiedId = randomWebSafeString(16);
        String md5 = BaseEncoding.base64().encode(Hashing.md5().hashBytes(data).asBytes());
        client.target(url).path("api").path("file").path(account)
                .path(container).path(specifiedId).request().header("Content-MD5", md5)
                .header("Authorization", accessToken)
                .header("Content-Length", data.length)
                .put(Entity.entity(in, MediaType.APPLICATION_OCTET_STREAM));

        Response response = client.target(url).path("api").path("file").path(account)
                .path(container).path(specifiedId).request().header("Authorization", accessToken)
                .get();

        assertEquals(Status.OK, Status.fromStatusCode(response.getStatus()));
        System.out.println("Success");
    }

    @Test
    @Order(order = 5)
    public void shouldFetchFileUsingMD5() {
        System.out.println("Fetching file by MD5 ...");
        byte[] data = randomBytes(4096, 8192);
        ByteArrayInputStream in = new ByteArrayInputStream(data);
        String specifiedId = randomWebSafeString(16);
        String container = randomWebSafeString(16);
        String md5 = BaseEncoding.base64().encode(Hashing.md5().hashBytes(data).asBytes());
        client.target(url).path("api").path("file").path(account)
                .path(container).path(specifiedId).request().header("Content-MD5", md5)
                .header("Authorization", accessToken)
                .header("Content-Length", data.length)
                .put(Entity.entity(in, MediaType.APPLICATION_OCTET_STREAM));

        Response response = client.target(url).path("api").path("file")
                .path(new String(UrlBase64.encode(md5.getBytes())))
                .request().get();

        byte[] fetchedData = response.readEntity(byte[].class);
        assertEquals(Status.OK, Status.fromStatusCode(response.getStatus()));
        assertEquals(md5, BaseEncoding.base64().encode(Hashing.md5().hashBytes(fetchedData).asBytes()));
        System.out.println("Success");
    }

    @Test
    @Order(order = 6)
    public void shouldListFilesForGivenContainer() {
        System.out.println("Listing files in a container ...");
        int numberOfFiles = 3;
        String container = randomWebSafeString(16);
        for (int i = 1; i <= numberOfFiles; i++) {
            String specifiedId = randomWebSafeString(i * 5);
            byte[] data = randomBytes(4096, 8192);
            ByteArrayInputStream in = new ByteArrayInputStream(data);
            String md5 = BaseEncoding.base64().encode(Hashing.md5().hashBytes(data).asBytes());

            Response response = client.target(url).path("api").path("file").path(account)
                    .path(container).path(specifiedId).request().header("Content-MD5", md5)
                    .header("Authorization", accessToken)
                    .header("Content-Length", data.length)
                    .put(Entity.entity(in, MediaType.APPLICATION_OCTET_STREAM));
            assertEquals(Status.OK, Status.fromStatusCode(response.getStatus()));
        }

        Response response = client.target(url).path("api").path("file").path(account)
                .path(container).path("files").queryParam("tag", "").queryParam("mimeType", "")
                .request().header("Authorization", accessToken).get();

        FilesResult fileResult = response.readEntity(FilesResult.class);
        assertEquals(numberOfFiles, fileResult.files.size());
        System.out.println("Success");
    }

    @Test
    @Order(order = 7)
    public void shouldDeleteFile() {
        System.out.println("Deleting file ...");
        byte[] data = randomBytes(4096, 8192);
        ByteArrayInputStream in = new ByteArrayInputStream(data);
        String container = randomWebSafeString(16);
        String specifiedId = randomWebSafeString(16);
        String md5 = BaseEncoding.base64().encode(Hashing.md5().hashBytes(data).asBytes());
        client.target(url).path("api").path("file").path(account)
                .path(container).path(specifiedId).request().header("Content-MD5", md5)
                .header("Authorization", accessToken)
                .header("Content-Length", data.length)
                .put(Entity.entity(in, MediaType.APPLICATION_OCTET_STREAM));

        Response deleteRequestResponse = client.target(url).path("api").path("file").path(account)
                .path(container).path(specifiedId).request()
                .header("Authorization", accessToken).delete();

        assertEquals(Status.OK, Status.fromStatusCode(deleteRequestResponse.getStatus()));
        Response getRequestResponse = client.target(url).path("api").path("file").path(account)
                .path(container).path(specifiedId).request()
                .header("Authorization", accessToken).get();
        assertEquals(Status.NOT_FOUND, Status.fromStatusCode(getRequestResponse.getStatus()));
        System.out.println("Success");
    }

    @Test
    @Order(order = 8)
    public void shouldInsertAndGetFileByTagAndType() {
        System.out.println("Upload file with tag and type and then fetch by tag and type ...");
        String container = randomWebSafeString(16);
        String tagOne = randomWebSafeString(16);
        String tagTwo = randomWebSafeString(16);
        String typeOne = MediaType.APPLICATION_OCTET_STREAM;
        String typeTwo = MediaType.APPLICATION_FORM_URLENCODED;

        for (int i = 0; i < 2; i++) {
            byte[] data = randomBytes(4096, 8192);
            ByteArrayInputStream in = new ByteArrayInputStream(data);
            String specifiedId = randomWebSafeString(16);
            String md5 = BaseEncoding.base64().encode(Hashing.md5().hashBytes(data).asBytes());
            client.target(url).path("api").path("file").path(account)
                    .path(container).path(specifiedId).request().header("Content-MD5", md5)
                    .header("Authorization", accessToken)
                    .header("Content-Length", data.length)
                    .header("Content-Tag", tagOne)
                    .put(Entity.entity(in, typeOne));
        }

        for (int i = 0; i < 3; i++) {
            byte[] data = randomBytes(4096, 8192);
            ByteArrayInputStream in = new ByteArrayInputStream(data);
            String specifiedId = randomWebSafeString(16);
            String md5 = BaseEncoding.base64().encode(Hashing.md5().hashBytes(data).asBytes());
            client.target(url).path("api").path("file").path(account)
                    .path(container).path(specifiedId).request().header("Content-MD5", md5)
                    .header("Authorization", accessToken)
                    .header("Content-Length", data.length)
                    .header("Content-Tag", tagTwo)
                    .put(Entity.entity(in, typeTwo));
        }

        Response correctResponseOne = client.target(url).path("api").path("file").path(account)
                .path(container).path("files").queryParam("tag", tagOne).queryParam("mimeType", typeOne)
                .request().header("Authorization", accessToken).get();
        FilesResult fileResultOne = correctResponseOne.readEntity(FilesResult.class);
        Response correctResponseTwo = client.target(url).path("api").path("file").path(account)
                .path(container).path("files").queryParam("tag", tagTwo).queryParam("mimeType", typeTwo)
                .request().header("Authorization", accessToken).get();
        FilesResult fileResultTwo = correctResponseTwo.readEntity(FilesResult.class);
        Response wrongResponseOne = client.target(url).path("api").path("file").path(account)
                .path(container).path("files").queryParam("tag", tagOne).queryParam("mimeType", typeTwo)
                .request().header("Authorization", accessToken).get();
        Response wrongResponseTwo = client.target(url).path("api").path("file").path(account)
                .path(container).path("files").queryParam("tag", tagTwo).queryParam("mimeType", typeOne)
                .request().header("Authorization", accessToken).get();

        assertEquals(Status.OK, Status.fromStatusCode(correctResponseOne.getStatus()));
        assertEquals(Status.OK, Status.fromStatusCode(correctResponseTwo.getStatus()));
        assertEquals(Status.NOT_FOUND, Status.fromStatusCode(wrongResponseOne.getStatus()));
        assertEquals(Status.NOT_FOUND, Status.fromStatusCode(wrongResponseTwo.getStatus()));
        assertEquals(2, fileResultOne.files.size());
        assertEquals(3, fileResultTwo.files.size());
        for (FileInfo file : fileResultOne.files) {
            assertEquals(tagOne, file.tag);
            assertEquals(typeOne, file.mimeType);
        }
        for (FileInfo file : fileResultTwo.files) {
            assertEquals(tagTwo, file.tag);
            assertEquals(typeTwo, file.mimeType);
        }
        System.out.println("Success");
    }

    @Test
    @Order(order = 9)
    public void shouldNotUploadFileOnChecksumMismatch() {
        System.out.println("Upload fails on File checksum mismatch ...");
        byte[] data = randomBytes(4096, 8192);
        ByteArrayInputStream in = new ByteArrayInputStream(data);
        String container = randomWebSafeString(16);
        String specifiedId = randomWebSafeString(16);
        String md5 = BaseEncoding.base64().encode(randomBytes(16));
        Response uploadResponse = client.target(url).path("api").path("file").path(account)
                .path(container).path(specifiedId).request().header("Content-MD5", md5)
                .header("Authorization", accessToken)
                .header("Content-Length", data.length)
                .header("Expect", "100-continue")
                .put(Entity.entity(in, MediaType.APPLICATION_OCTET_STREAM));

        Response getUploadedFileResponse = client.target(url).path("api").path("file").path(account)
                .path(container).path(specifiedId).request().header("Authorization", accessToken)
                .get();

        assertEquals(Status.BAD_REQUEST, Status.fromStatusCode(uploadResponse.getStatus()));
        assertEquals(Status.NOT_FOUND, Status.fromStatusCode(getUploadedFileResponse.getStatus()));
        System.out.println("Success");
    }

    @Test
    @Order(order = 10)
    public void shouldReturnErrorStatusCodeForDuplicateFileUpload() {
        System.out.println("Duplicate file fails to upload with expectation failed status ...");
        byte[] data = randomBytes(4096, 8192);
        ByteArrayInputStream in = new ByteArrayInputStream(data);
        String containerOne = randomWebSafeString(16);
        String containerTwo = randomWebSafeString(16);
        String specifiedId = randomWebSafeString(16);
        String md5 = BaseEncoding.base64().encode(Hashing.md5().hashBytes(data).asBytes());
        Response firstUploadResponse = client.target(url).path("api").path("file").path(account)
                .path(containerOne).path(specifiedId).request().header("Content-MD5", md5)
                .header("Authorization", accessToken)
                .header("Content-Length", data.length)
                .header("Expect", "100-continue")
                .put(Entity.entity(in, MediaType.APPLICATION_OCTET_STREAM));

        in = new ByteArrayInputStream(data);
        Response duplicateUploadResponse = client.target(url).path("api").path("file").path(account)
                .path(containerTwo).path(specifiedId).request().header("Content-MD5", md5)
                .header("Authorization", accessToken)
                .header("Content-Length", data.length)
                .put(Entity.entity(in, MediaType.APPLICATION_OCTET_STREAM));
        Response getFirstUploadFileResponse = client.target(url).path("api").path("file").path(account)
                .path(containerOne).path(specifiedId).request().header("Authorization", accessToken)
                .get();
        Response getDuplicateUploadFileResponse = client.target(url).path("api").path("file").path(account)
                .path(containerTwo).path(specifiedId).request().header("Authorization", accessToken)
                .get();

        assertEquals(Status.OK, Status.fromStatusCode(firstUploadResponse.getStatus()));
        assertEquals(Status.OK, Status.fromStatusCode(getFirstUploadFileResponse.getStatus()));
        assertEquals(Status.EXPECTATION_FAILED, Status.fromStatusCode(duplicateUploadResponse.getStatus()));
        assertEquals(Status.OK, Status.fromStatusCode(getDuplicateUploadFileResponse.getStatus()));
        System.out.println("Success");
    }

    @Test
    @Order(order = 11)
    public void shouldDeleteOlderFileOnConflictingFileUpload() {
        System.out.println("Should overwrite previous file in case of conflict upload ...");
        byte[] data = randomBytes(4096, 8192);
        ByteArrayInputStream in = new ByteArrayInputStream(data);
        String container = randomWebSafeString(16);
        String specifiedId = randomWebSafeString(16);
        String md5 = BaseEncoding.base64().encode(Hashing.md5().hashBytes(data).asBytes());

        Response firstUploadResponse = client.target(url).path("api").path("file").path(account)
                .path(container).path(specifiedId).request().header("Content-MD5", md5)
                .header("Authorization", accessToken)
                .header("Content-Length", data.length)
                .header("Expect", "100-continue")
                .put(Entity.entity(in, MediaType.APPLICATION_OCTET_STREAM));

        data = randomBytes(4096, 8192);
        in = new ByteArrayInputStream(data);
        md5 = BaseEncoding.base64().encode(Hashing.md5().hashBytes(data).asBytes());

        Response conflictingUploadResponse = client.target(url).path("api").path("file").path(account)
                .path(container).path(specifiedId).request().header("Content-MD5", md5)
                .header("Authorization", accessToken)
                .header("Content-Length", data.length)
                .header("Expect", "100-continue")
                .put(Entity.entity(in, MediaType.APPLICATION_OCTET_STREAM));

        Response fetchFileResponse = client.target(url).path("api").path("file").path(account)
                .path(container).path(specifiedId).request().header("Authorization", accessToken)
                .get();
        byte[] fetchedData = fetchFileResponse.readEntity(byte[].class);

        assertEquals(Status.OK, Status.fromStatusCode(firstUploadResponse.getStatus()));
        assertEquals(Status.OK, Status.fromStatusCode(conflictingUploadResponse.getStatus()));
        assertEquals(md5, BaseEncoding.base64().encode(Hashing.md5().hashBytes(fetchedData).asBytes()));
        System.out.println("Success");
    }

    @Test
    @Order(order = 12)
    public void shouldReturnErrorStatusCodeWhenDeletingNonExistentFile() {
        System.out.println("Non existent file deletion should fail ...");
        String container = randomWebSafeString(16);
        String specifiedId = randomWebSafeString(16);

        Response deleteRequestResponse = client.target(url).path("api").path("file").path(account)
                .path(container).path(specifiedId).request()
                .header("Authorization", accessToken).delete();

        assertEquals(Status.NOT_FOUND, Status.fromStatusCode(deleteRequestResponse.getStatus()));
        System.out.println("Success");
    }

    @Test
    @Order(order = 13)
    public void shouldShareContainer() {
        System.out.println("Should return shared auth token when sharing a container ...");
        String container = randomWebSafeString(50);
        String specifiedId = randomWebSafeString(50);
        byte[] data = randomBytes(4096, 8192);
        ByteArrayInputStream in = new ByteArrayInputStream(data);
        String md5 = BaseEncoding.base64().encode(Hashing.md5().hashBytes(data).asBytes());
        Response response = client.target(url).path("api").path("file").path(account)
                .path(container).path(specifiedId).request().header("Content-MD5", md5)
                .header("Authorization", accessToken)
                .header("Content-Length", data.length)
                .put(Entity.entity(in, MediaType.APPLICATION_OCTET_STREAM));
        assertEquals(Status.OK, Status.fromStatusCode(response.getStatus()));

        Response shareContainerResponse = client.target(url).path("api").path("container").path(account)
                .path(container).path("share").request().header("Authorization", accessToken).get();
        ShareContainerResult shareContainerResult = shareContainerResponse.readEntity(ShareContainerResult.class);
        String sharedAuthToken = shareContainerResult.getSharedAuthToken();

        assertEquals(Response.Status.OK, Response.Status.fromStatusCode(shareContainerResponse.getStatus()));
        assertEquals(true, sharedAuthToken != null);
        System.out.println("Success");
    }

    @Test
    @Order(order = 14)
    public void shouldDeleteContainers() {
        System.out.println("Should delete all existing containers ...");
        Response response = client.target(url).path("api").path("container").path(account).path("containers").request()
                .header("Authorization", accessToken).get();
        ContainerResult containerResult = response.readEntity(ContainerResult.class);
        assertEquals(Response.Status.OK, Response.Status.fromStatusCode(response.getStatus()));
        ArrayList<String> resultContainers = (ArrayList<String>) containerResult.getContainers();
        byte[] continuationKey = containerResult.getContinuationKey();
        while (continuationKey != null) {
            response = client.target(url).path("api").path("container").path(account).path("containers").queryParam("continuationKey", continuationKey).request()
                    .header("Authorization", accessToken).get();
            containerResult = response.readEntity(ContainerResult.class);
            assertEquals(Response.Status.OK, Response.Status.fromStatusCode(response.getStatus()));
            resultContainers.addAll(containerResult.getContainers());
            continuationKey = containerResult.getContinuationKey();
        }

        for (String resultContainer : resultContainers) {
            Response deleteContainerResponse = client.target(url).path("api").path("container").path(account)
                    .path(resultContainer).request().header("Authorization", accessToken).delete();
            assertEquals(Response.Status.OK, Response.Status.fromStatusCode(deleteContainerResponse.getStatus()));
        }

        Response listContainerResponse = client.target(url).path("api").path("container").path(account)
                .path("containers").request().header("Authorization", accessToken).get();
        ContainerResult containersResult = listContainerResponse.readEntity(ContainerResult.class);
        resultContainers = (ArrayList<String>) containersResult.getContainers();

        assertEquals(0, resultContainers.size());
        assertEquals(Response.Status.OK, Response.Status.fromStatusCode(listContainerResponse.getStatus()));
        System.out.println("Success");
    }

    @Test
    @Order(order = 15)
    public void shouldListContainers() {
        System.out.println("Should list all existing containers ...");
        int numberOfContainers = 10;
        ArrayList<String> containers = new ArrayList<>();
        for (int i = 1; i <= numberOfContainers; i++) {
            containers.add(randomWebSafeString(16));
        }
        for (int i = 0; i < numberOfContainers; i++) {
            String specifiedId = randomWebSafeString((i + 1) * 5);
            byte[] data = randomBytes(4096, 8192);
            ByteArrayInputStream in = new ByteArrayInputStream(data);
            String md5 = BaseEncoding.base64().encode(Hashing.md5().hashBytes(data).asBytes());

            Response response = client.target(url).path("api").path("file").path(account)
                    .path(containers.get(i)).path(specifiedId).request().header("Content-MD5", md5)
                    .header("Authorization", accessToken)
                    .header("Content-Length", data.length)
                    .put(Entity.entity(in, MediaType.APPLICATION_OCTET_STREAM));
            assertEquals(Status.OK, Status.fromStatusCode(response.getStatus()));
        }
        Response response = client.target(url).path("api").path("container").path(account).path("containers").request()
                .header("Authorization", accessToken).get();
        ContainerResult containerResult = response.readEntity(ContainerResult.class);

        assertEquals(Response.Status.OK, Response.Status.fromStatusCode(response.getStatus()));
        ArrayList<String> resultContainers = (ArrayList<String>) containerResult.getContainers();
        byte[] continuationKey = containerResult.getContinuationKey();
        while (continuationKey != null) {
            response = client.target(url).path("api").path("container").path(account).path("containers").queryParam("continuationKey", continuationKey).request()
                    .header("Authorization", accessToken).get();
            containerResult = response.readEntity(ContainerResult.class);
            assertEquals(Response.Status.OK, Response.Status.fromStatusCode(response.getStatus()));
            resultContainers.addAll(containerResult.getContainers());
            continuationKey = containerResult.getContinuationKey();
        }

        assertEquals(numberOfContainers, resultContainers.size());
        for (int i = 0; i < numberOfContainers; i++) {
            assertEquals(containers.contains(resultContainers.get(i)), true);
        }
        System.out.println("Success");
    }

    private String fetchAccessToken(String code) {
        System.out.println("Fetching accesstoken ...");
        MultivaluedMap<String, String> formTokenData = new MultivaluedHashMap<>();
        formTokenData.add("client_id", CLIENT_ID);
        formTokenData.add("client_secret", CLIENT_SECRET);
        formTokenData.add("redirect_uri", redirectUri);
        formTokenData.add("grant_type", "authorization_code");
        formTokenData.add("code", code);

        Response tokenResponse = client.target(url).path("api").path("auth").path("code")
                .request().post(Entity.entity(formTokenData, MediaType.APPLICATION_FORM_URLENCODED));

        assertEquals(Status.OK, Status.fromStatusCode(tokenResponse.getStatus()));
        AccessTokenResponse accessTokenResponse = tokenResponse.readEntity(AccessTokenResponse.class);

        assertEquals(account, accessTokenResponse.getAccount());
        System.out.println("Success");
        return accessTokenResponse.accessToken();
    }

    private String oAuthFetchCode() {
        System.out.println("Fetching code ...");
        redirectUri = "urn:ietf:wg:oauth:2.0:oob:auto";
        MultivaluedMap<String, String> formData = new MultivaluedHashMap<>();
        formData.add("account", account);
        formData.add("password", password);
        formData.add("redirect_uri", redirectUri);

        Response userResponse = client.target(url).path("user")
                .request().post(Entity.entity(formData, MediaType.APPLICATION_FORM_URLENCODED));

        assertEquals(Status.OK, Status.fromStatusCode(userResponse.getStatus()));
        String responseEntity = userResponse.readEntity(String.class);
        assertTrue(responseEntity.startsWith("<html><head><title>code="));
        assertTrue(responseEntity.endsWith("</title></head></html>"));
        System.out.println("Success");

        return responseEntity.substring(responseEntity.indexOf("code=") + 5, responseEntity.indexOf("</title>"));
    }
}
