package omstore.http;

import core.RandomData;
import omstore.Authentication;
import omstore.contract.ShareContainerResult;
import omstore.database.ContainerDb;
import omstore.database.FilesDb;
import omstore.service.AuthenticationService;
import omstore.service.FileContentService;

import javax.inject.Inject;
import javax.inject.Singleton;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;

import static javax.ws.rs.core.Response.ok;
import static omstore.Authentication.AuthenticationResponse;
import static omstore.database.ContainerDb.ContainerResult;
import static omstore.database.FilesDb.FileInfo;
import static omstore.database.FilesDb.FilesResult;

@Singleton
@Path("/api/container")
public class ContainerContent {
    @Inject
    ContainerDb containerDb;
    @Inject
    FilesDb filesDb;
    @Inject
    FileContentService fileContentService;
    @Inject
    AuthenticationService authService;

    public ContainerContent() {
    }

    @Path("/{account}/containers")
    @GET
    @Produces("application/json")
    public Response listContainers(@Context Authentication auth, @PathParam("account") String account,
                                   @QueryParam("continuationKey") String continuationKey) {
        AuthenticationResponse authenticationResponse = authService.validateRequest(auth, account);
        if (authenticationResponse.equals(AuthenticationResponse.Forbidden)) {
            throw new ForbiddenException();
        } else if (authenticationResponse.equals(AuthenticationResponse.Invalid)) {
            throw new BadRequestException();
        }

        byte[] continuationBytes;
        if (continuationKey != null) {
            continuationBytes = continuationKey.getBytes();
        } else {
            continuationBytes = null;
        }
        ContainerResult containers = containerDb.listContainersForAccount(account, continuationBytes);
        return ok(containers).build();
    }

    @Path("/{account}/{container}")
    @DELETE
    public Response deleteContainer(@Context Authentication auth, @PathParam("account") String account,
                                    @PathParam("container") String container) {
        AuthenticationResponse authenticationResponse = authService.validateRequest(auth, account);
        if (authenticationResponse.equals(AuthenticationResponse.Forbidden)) {
            throw new ForbiddenException();
        } else if (authenticationResponse.equals(AuthenticationResponse.Invalid)) {
            throw new BadRequestException();
        }

        if (containerDb.getContainerForAccount(account, container) == null) {
            throw new NotFoundException();
        }

        FilesResult filesResult;
        byte[] continuationKey = null;
        do {
            filesResult = filesDb.getFilesForContainer(account, container, null, null, continuationKey);
            for (FileInfo file : filesResult.files) {
                filesDb.removeFile(account, container, file.specifiedId);
                if (filesDb.getStoragePath(file.md5) == null) {
                    fileContentService.deleteContent(file.relativeStorageUrl);
                }
            }
            continuationKey = filesResult.continuationKey;
        } while (continuationKey != null);

        containerDb.deleteContainer(account, container);
        return ok().build();
    }

    @Path("/{account}/{container}/share")
    @GET
    @Produces("application/json")
    public Response share(@Context Authentication auth, @PathParam("account") String account,
                                    @PathParam("container") String container) {
        AuthenticationResponse authenticationResponse = authService.validateRequest(auth, account);
        if (authenticationResponse.equals(AuthenticationResponse.Forbidden)) {
            throw new ForbiddenException();
        } else if (authenticationResponse.equals(AuthenticationResponse.Invalid)) {
            throw new BadRequestException();
        }

        if (containerDb.getContainerForAccount(account, container) == null) {
            throw new NotFoundException();
        }

        String sharedAuthToken = containerDb.getAuthTokenForSharedContainer(account, container);
        if (sharedAuthToken == null) {
            sharedAuthToken = RandomData.randomWebSafeString(20);
            containerDb.addSharedContainer(account, container, sharedAuthToken);
        }

        return ok(new ShareContainerResult(sharedAuthToken)).build();
    }
}
