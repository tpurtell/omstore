package omstore.http.oauth2;

import com.fasterxml.jackson.annotation.JsonProperty;

public class AccessTokenResponse {
    @JsonProperty
    public String account;
    @JsonProperty
    public String access_token;

    public AccessTokenResponse() {
    }

    public AccessTokenResponse(String account, String access_token) {
        this.account = account;
        this.access_token = access_token;
    }

    public String getAccount() {
        return account;
    }

    public String accessToken() {
        return access_token;
    }
}
