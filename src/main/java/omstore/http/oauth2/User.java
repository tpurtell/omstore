package omstore.http.oauth2;

import com.google.common.hash.Hashing;
import com.google.common.io.BaseEncoding;
import core.LocatorIds;
import core.RandomData;
import core.jxp.JxpProcessor;
import core.jxp.JxpTemplateProvider;
import omstore.database.TokenDb;
import omstore.database.UserDb;
import omstore.util.ProdUtils;
import omstore.util.URIUtils;

import javax.inject.Inject;
import javax.inject.Named;
import javax.ws.rs.*;
import javax.ws.rs.core.*;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.concurrent.ConcurrentHashMap;

import static javax.ws.rs.core.Response.ok;

@Path("/user")
public class User {
    public static final String OOB_AUTO_REDIRECT_URL = "urn:ietf:wg:oauth:2.0:oob:auto";
    @Inject
    TokenDb tokendb;

    @Inject
    UserDb userDb;

    private ConcurrentHashMap<String, String> cookieMap;
    private JxpTemplateProvider jxp;

    @Inject
    public User(@Named("cookieMap") ConcurrentHashMap cookieMap, JxpTemplateProvider jxp) {
        this.cookieMap = cookieMap;
        this.jxp = jxp;
    }

    public static class LoginFormModel {
        public String redirectURI;
        public boolean isFailure;

        public LoginFormModel(String redirectURI, boolean isFailure) {
            this.redirectURI = redirectURI;
            this.isFailure = isFailure;
        }
    }

    public static class RegistrationFormModel {
        public String message;
        public boolean isSuccessful;

        public RegistrationFormModel(String message, boolean isSuccessful) {
            this.message = message;
            this.isSuccessful = isSuccessful;
        }
    }

    @GET
    @Produces(MediaType.TEXT_HTML)
    public Response login(@QueryParam("redirect_uri") String redirectUri, @Context HttpHeaders header)
            throws IOException, RuntimeException {
        LoginFormModel loginFormModel = new LoginFormModel(redirectUri, false);
        JxpProcessor signin = jxp.instantiate("/templates/login.jxp", loginFormModel);
        return ok(signin.evaluate()).build();
    }

    @POST
    @Produces(MediaType.TEXT_HTML)
    public Response login(@FormParam("account") String account, @FormParam("password") String password,
                          @FormParam("redirect_uri") String redirectUri, @Context HttpHeaders headers,
                          @Context UriInfo uriInfo) throws IOException, RuntimeException, URISyntaxException {
        String hashAccount = hashAccount(account);
        String hashPassword = hashAccount(password);

        if (!userDb.checkUser(hashAccount) || !userDb.checkPassword(hashAccount, hashPassword)) {
            LoginFormModel loginFormModel = new LoginFormModel(redirectUri, true);
            JxpProcessor signin = jxp.instantiate("/templates/login.jxp", loginFormModel);
            return ok(signin.evaluate()).build();
        }

        String code = tokendb.getAccessCodeForAccount(account);

        if (code == null) {
            code = RandomData.randomWebSafeString(20);
            Long expires = System.currentTimeMillis() + 24 * 60 * 60 * 1000l;
            tokendb.addAccessCode(account, code, expires);
        }

        if (redirectUri.equals(OOB_AUTO_REDIRECT_URL)) {
            return Response.ok("<html><head>" +
                    "<title>code=" + code + "</title>" +
                    "</head>" +
                    "</html>").build();

        } else {
            String token = tokendb.getTokenForAccount(account);
            if (token == null) {
                token = RandomData.randomString(10, 20);
                tokendb.setTokenForAccount(account, token);
            }
            cookieMap.put(account, token);
            Response.ResponseBuilder responseBuilder = Response.seeOther(URIUtils.createURI(redirectUri, headers));
            return responseBuilder.status(Response.Status.SEE_OTHER).cookie(new NewCookie("authToken", token),
                    new NewCookie("account", account)).build();
        }
    }

    @POST
    @Path("/logout")
    @Produces(MediaType.TEXT_HTML)
    public Response logout(@FormParam("account") String account, @Context HttpHeaders header, @Context UriInfo uriInfo)
            throws IOException, RuntimeException {
        tokendb.deleteToken(account);

        return Response.ok().build();
    }

    @GET
    @Path("/register")
    @Produces(MediaType.TEXT_HTML)
    public Response getRegistrationPage() {
        RegistrationFormModel formModel = new RegistrationFormModel("Welcome to Omlet cloud! User registration", true);
        JxpProcessor registration = jxp.instantiate("/templates/register.jxp", formModel);
        return ok(registration.evaluate()).build();
    }

    @POST
    @Path("/register")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces(MediaType.TEXT_HTML)
    public Response register(@FormParam("account") String account, @FormParam("password") String password,
                             @FormParam("adminKey") String adminKey)
            throws IOException, RuntimeException {
        if (!ProdUtils.getAdminKey().equals(adminKey)) {
            RegistrationFormModel formModel = new RegistrationFormModel("Invalid credentials. Please try again.", false);
            JxpProcessor registration = jxp.instantiate("/templates/register.jxp", formModel);
            return ok(registration.evaluate()).build();
        }

        String hashAccount = hashAccount(account);
        String hashPassword = hashAccount(password);

        userDb.registerUser(hashAccount, hashPassword);
        RegistrationFormModel formModel = new RegistrationFormModel("User " + account + " successfully registered!", true);
        JxpProcessor registration = jxp.instantiate("/templates/register.jxp", formModel);
        return ok(registration.evaluate()).build();
    }

    private String hashAccount(String account) {
        return BaseEncoding.base32().encode(Hashing.md5().newHasher().putBytes(account.getBytes()).hash().asBytes());
    }
}
