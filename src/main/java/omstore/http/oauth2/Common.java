package omstore.http.oauth2;

public final class Common {
    public static final String CLIENT_ID = "omstore_client_id";
    public static final String CLIENT_SECRET = "omstore_secret";
}
