package omstore.http.oauth2;

import core.RandomData;
import omstore.database.TokenDb;
import omstore.util.URIUtils;

import javax.inject.Inject;
import javax.inject.Singleton;
import javax.ws.rs.*;
import javax.ws.rs.core.*;
import java.net.URI;
import java.net.URISyntaxException;

@Singleton
@Path("/api/auth")
public class Auth {
    @Inject
    TokenDb tokenDb;

    public Auth() {
    }

    @Path("/signin")
    @GET
    public Response authorize(@QueryParam("client_id") String client_id,
                              @QueryParam("redirect_uri") String redirectUri, @QueryParam("scope") String scope,
                              @QueryParam("response_type") String responseType, @Context HttpHeaders headers)
            throws URISyntaxException {
        if (client_id != null && client_id.equals(Common.CLIENT_ID) && responseType != null && responseType.equals("code") && redirectUri != null) {
            Response.ResponseBuilder builder = Response.seeOther(URIUtils
                    .createURI("/user?redirect_uri=" + redirectUri, headers));
            return builder.status(Response.Status.SEE_OTHER).build();
        } else {
            throw new WebApplicationException(Response.Status.BAD_REQUEST);
        }
    }

    @Path("/code")
    @POST
    @Consumes("application/x-www-form-urlencoded")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getAccessToken(MultivaluedMap<String, String> multiMap) {
        String grantType = multiMap.getFirst("grant_type");
        String scope = multiMap.getFirst("scope");
        String redirectUri = multiMap.getFirst("redirect_uri");
        String client_id = multiMap.getFirst("client_id");
        String client_secret = multiMap.getFirst("client_secret");
        String code = multiMap.getFirst("code");
        String account = null;
        Long expires = 0l;
        if (code != null) {
            account = tokenDb.getAccountForAccessCode(code);
            expires = Long.parseLong(tokenDb.checkAccountForAccessCode(account, code));
        }

        Long current = System.currentTimeMillis();
        if (client_id != null && client_id.equals(Common.CLIENT_ID) && grantType != null && grantType.equals("authorization_code")
                && redirectUri != null
                && client_secret != null && client_secret.equals(Common.CLIENT_SECRET)
                && code != null && account != null
                && (expires >= current)) {

            String token = tokenDb.getTokenForAccount(account);
            if (token == null) {
                token = RandomData.randomString(10, 20);
                tokenDb.setTokenForAccount(account, token);
            }

            AccessTokenResponse tokenResponse = new AccessTokenResponse(account, token);

            return Response.ok(tokenResponse).build();
        } else {
            throw new WebApplicationException(Response.Status.BAD_REQUEST);
        }
    }
}
