package omstore.http;

import com.google.common.hash.HashFunction;
import com.google.common.hash.Hashing;
import com.google.common.hash.HashingInputStream;
import com.google.common.io.BaseEncoding;
import core.RandomData;
import omstore.Authentication;
import omstore.Authentication.AuthenticationResponse;
import omstore.database.ContainerDb;
import omstore.database.FilesDb;
import omstore.database.FilesDb.FileInfo;
import omstore.exceptions.FileContentException;
import omstore.service.AuthenticationService;
import omstore.service.FileContentService;
import org.apache.commons.io.FilenameUtils;
import org.bouncycastle.util.encoders.UrlBase64;
import org.glassfish.jersey.grizzly2.httpserver.GrizzlyHttpContainer.ExpectAcknowledger;

import javax.activation.MimetypesFileTypeMap;
import javax.inject.Inject;
import javax.inject.Singleton;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

import static java.lang.System.currentTimeMillis;
import static javax.ws.rs.core.Response.ok;

@Singleton
@Path("/api/file")
public class FileContent {
    private final MimetypesFileTypeMap mimetypesFileTypeMap;

    @Inject
    FilesDb filesDb;
    @Inject
    FileContentService fileContentService;
    @Inject
    ContainerDb containerDb;
    @Inject
    AuthenticationService authService;

    public FileContent() {
        mimetypesFileTypeMap = new MimetypesFileTypeMap();
        mimetypesFileTypeMap.addMimeTypes("application/json json");
        mimetypesFileTypeMap.addMimeTypes("image/png png");
        mimetypesFileTypeMap.addMimeTypes("image/jpeg jpg jpeg");
        mimetypesFileTypeMap.addMimeTypes("image/gif gif");
        mimetypesFileTypeMap.addMimeTypes("video/mp4 mp4");
    }

    @Path("/{account}/{container}/{specifiedId}")
    @PUT
    public Response writeFile(InputStream data, @HeaderParam("Content-MD5") String md5,
                              @Context Authentication auth, @PathParam("account") String account,
                              @PathParam("container") String container, @PathParam("specifiedId") String specifiedId,
                              @HeaderParam("Content-Length") long contentLength, @HeaderParam("Content-Type") String contentType,
                              @HeaderParam("Content-Tag") String contentTag, @Context ExpectAcknowledger acknowledger) throws IOException {
        AuthenticationResponse authenticationResponse = authService.validateRequest(auth, account);
        if (authenticationResponse.equals(AuthenticationResponse.Forbidden)) {
            throw new ForbiddenException();
        } else if (authenticationResponse.equals(AuthenticationResponse.Invalid)) {
            throw new BadRequestException();
        }

        FileInfo existingFile = filesDb.getFileFromSpecifiedId(account, container, specifiedId);
        if (existingFile != null) {
            filesDb.removeFile(account, container, specifiedId);
            if (filesDb.getStoragePath(existingFile.md5) == null) {
                fileContentService.deleteContent(existingFile.relativeStorageUrl);
            }
        }

        if (containerDb.getContainerForAccount(account, container) == null) {
            containerDb.addContainer(account, container, currentTimeMillis());
        }

        String storagePath = filesDb.getStoragePath(md5);
        if (storagePath != null) {
            filesDb.addFile(account, container, specifiedId, storagePath, contentTag, contentType,
                    contentLength, md5, currentTimeMillis());
            throw new WebApplicationException(Status.EXPECTATION_FAILED);
        } else {
            storagePath = makeStoragePath(account, container, specifiedId);
            if (acknowledger != null) {
                acknowledger.sendAcknowledgment();
            }
        }

        HashFunction hasher = Hashing.md5();
        HashingInputStream wrapped = new HashingInputStream(hasher, data);

        String temporaryPath = RandomData.randomWebSafeString(50);
        fileContentService.writeContent(temporaryPath, wrapped);

        String calculatedMD5 = BaseEncoding.base64().encode(wrapped.hash().asBytes());

        if (!calculatedMD5.equals(md5)) {
            fileContentService.deleteContent(temporaryPath);
            throw new BadRequestException();
        }

        fileContentService.moveContent(temporaryPath, storagePath);

        filesDb.addFile(account, container, specifiedId, storagePath, contentTag, contentType,
                contentLength, md5, currentTimeMillis());

        return ok().build();
    }

    @Path("/{account}/{container}/{specifiedId}")
    @GET
    public Response readFile(@PathParam("account") String account,
                             @PathParam("container") String container, @PathParam("specifiedId") String specifiedId) {
        FileInfo fileInfo = filesDb.getFileFromSpecifiedId(account, container, specifiedId);

        if (fileInfo == null)
            throw new FileContentException.FileDoesntExist(null);

        return ok(fileContentService.openContent(fileInfo.relativeStorageUrl), fileInfo.mimeType).build();
    }

    @Path("/{md5}")
    @GET
    public Response fetchFileByMD5(@PathParam("md5") String md5) {
        String contentType;
        String decodedMd5 = new String(UrlBase64.decode(md5));
        String fileStoragePath = filesDb.getStoragePath(decodedMd5);

        if (fileStoragePath == null)
            throw new FileContentException.FileDoesntExist(null);

        contentType = mimetypesFileTypeMap.getContentType(fileStoragePath);
        return ok(fileContentService.openContent(fileStoragePath), contentType).build();
    }

    @Path("/{account}/{container}/files")
    @GET
    @Produces("application/json")
    public Response listFilesForContainers(@Context Authentication auth, @PathParam("account") String account,
                                           @PathParam("container") String container, @QueryParam("tag") String tag,
                                           @QueryParam("mimeType") String mimeType) {
        AuthenticationResponse authenticationResponse;
        String authTokenForSharedContainer = containerDb.getAuthTokenForSharedContainer(account, container);
        if (authTokenForSharedContainer != null && authTokenForSharedContainer.equals(auth.getAccessToken())) {
            authenticationResponse = AuthenticationResponse.None;
        } else {
            authenticationResponse = authService.validateRequest(auth, account);
        }
        if (authenticationResponse.equals(AuthenticationResponse.Forbidden)) {
            throw new ForbiddenException();
        } else if (authenticationResponse.equals(AuthenticationResponse.Invalid)) {
            throw new BadRequestException();
        }

        byte[] continuationKey = null;
        FilesDb.FilesResult filesResults = new FilesDb.FilesResult();
        filesResults.files = new ArrayList<>();

        //TODO: continuation key should be implemented at http level
        FilesDb.FilesResult filesResult;
        do {
            filesResult = filesDb.getFilesForContainer(account, container, mimeType, tag, continuationKey);
            filesResults.files.addAll(filesResult.files);
            continuationKey = filesResult.continuationKey;
        } while (continuationKey != null);

        if (filesResults.files.isEmpty())
            throw new FileContentException.FileDoesntExist(null);

        return ok(filesResults).build();
    }

    @Path("/{account}/{container}/{specifiedId}")
    @DELETE
    public Response deleteFileInContainer(@Context Authentication auth, @PathParam("account") String account,
                                          @PathParam("container") String container,
                                          @PathParam("specifiedId") String specifiedId) {
        AuthenticationResponse authenticationResponse = authService.validateRequest(auth, account);
        if (authenticationResponse.equals(AuthenticationResponse.Forbidden)) {
            throw new ForbiddenException();
        } else if (authenticationResponse.equals(AuthenticationResponse.Invalid)) {
            throw new BadRequestException();
        }

        FileInfo file = filesDb.getFileFromSpecifiedId(account, container, specifiedId);
        if (file == null) {
            throw new NotFoundException();
        }
        filesDb.removeFile(account, container, specifiedId);
        if (filesDb.getStoragePath(file.md5) == null) {
            fileContentService.deleteContent(file.relativeStorageUrl);
        }

        return ok().build();
    }

    private String makeStoragePath(String account, String container, String specifiedId) {
        return BaseEncoding.base32().encode(
                Hashing.md5().newHasher().putBytes(account.getBytes())
                        .putBytes(container.getBytes()).putBytes(specifiedId.getBytes()).hash()
                        .asBytes()) + FilenameUtils.getExtension(specifiedId);
    }
}
