package omstore.exceptions;

import org.apache.commons.io.FileUtils;

import javax.ws.rs.NotFoundException;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;
import java.io.File;
import java.io.IOException;

@Provider
public class NotFoundExceptionMapper implements ExceptionMapper<NotFoundException> {
    public Response toResponse(NotFoundException exception) {
        try {
            return Response.status(Response.Status.NOT_FOUND)
                    .entity(FileUtils.readFileToString(new File("webroot/404.html")))
                    .build();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return Response.status(Response.Status.NOT_FOUND)
                .entity("Not Found")
                .build();
    }
}