package omstore.exceptions;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;

public class PojoParsingException extends WebApplicationException {
    private static final long serialVersionUID = 9130169560457123579L;

    public PojoParsingException(Throwable t) {
        super(t, Response.status(Response.Status.BAD_REQUEST).entity(t.getMessage()).build());
    }
}
