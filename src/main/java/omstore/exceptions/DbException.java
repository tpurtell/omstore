package omstore.exceptions;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response.Status;

public class DbException extends WebApplicationException {

    private static final long serialVersionUID = 8064335695684842039L;

    public DbException(String message, Throwable t) {
        super(message, t, Status.SERVICE_UNAVAILABLE);
    }

    public DbException(String message) {
        super(message, Status.SERVICE_UNAVAILABLE);
    }

    public DbException(Throwable t) {
        super(t, Status.SERVICE_UNAVAILABLE);
    }

    // you could define specific errors here if subclassing is meaningful
}
