package omstore.exceptions;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response.Status;

public class FileContentException extends WebApplicationException {

    private static final long serialVersionUID = 8064335695684842039L;

    public FileContentException(String message, Throwable t) {
        super(message, t);
    }

    public FileContentException(String message) {
        super(message);
    }

    public FileContentException(Throwable t) {
        super(t);
    }

    public FileContentException(String message, Throwable t, Status status) {
        super(message, t, status);
    }

    public FileContentException(String message, Status status) {
        super(message, status);
    }

    public FileContentException(Throwable t, Status status) {
        super(t, status);
    }

    // you could define specific errors here if subclassing is meaningful
    public static class FileDoesntExist extends FileContentException {
        private static final long serialVersionUID = 1346765745737424963L;

        public FileDoesntExist(Throwable t) {
            super(t, Status.NOT_FOUND);
        }
    }
}
