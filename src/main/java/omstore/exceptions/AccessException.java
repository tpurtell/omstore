package omstore.exceptions;

import javax.ws.rs.core.Response.Status;

public class AccessException extends ApiException {
	private static final long serialVersionUID = -4507300214485167135L;

	public AccessException() {
		super(Status.FORBIDDEN, ApiCode.AccessDenied);
	}

	public AccessException(Throwable t) {
		super(Status.FORBIDDEN, ApiCode.AccessDenied, t);
	}

	public AccessException(ApiCode code) {
		super(Status.FORBIDDEN, code);
	}
}
