package omstore.exceptions;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

public class ApiException extends WebApplicationException {
    public enum ApiCode {
        AccessDenied
    }

    private static final long serialVersionUID = 4774162329177763668L;
    public final ApiCode code;
    public final String detail;

    public ApiException(ApiCode code) {
        super(Response.status(Response.Status.BAD_REQUEST).type(MediaType.TEXT_PLAIN)
                .entity(code.toString()).build());
        this.code = code;
        this.detail = null;
    }

    public ApiException(ApiCode code, String message) {
        super(Response.status(Response.Status.BAD_REQUEST).type(MediaType.TEXT_PLAIN)
                .entity(String.format("%s: %s", code.toString(), message)).build());
        this.code = code;
        this.detail = message;
    }

    public ApiException(Response.Status status, ApiCode code) {
        super(Response.status(status).type(MediaType.TEXT_PLAIN).entity(code.toString()).build());
        this.code = code;
        this.detail = null;
    }

    public ApiException(ApiCode code, Throwable t) {
        super(t, Response.status(Response.Status.BAD_REQUEST).type(MediaType.TEXT_PLAIN)
                .entity(code.toString()).build());
        this.code = code;
        this.detail = null;
    }

    public ApiException(Response.Status status, ApiCode code, Throwable t) {
        super(t, Response.status(status).type(MediaType.TEXT_PLAIN).entity(code.toString()).build());
        this.code = code;
        this.detail = null;
    }

    @Override
    public String toString() {
        return super.toString() + ": " + code;
    }

}
