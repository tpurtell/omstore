package omstore.contract;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ShareContainerResult {
    @JsonProperty
    private String sharedAuthToken;

    public ShareContainerResult() {
    }

    public ShareContainerResult(String sharedAuthToken) {
        this.sharedAuthToken = sharedAuthToken;
    }

    public String getSharedAuthToken() {
        return sharedAuthToken;
    }
}
