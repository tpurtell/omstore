package omstore;

import org.apache.commons.lang3.builder.ToStringBuilder;

public class Authentication {
    public enum AuthenticationResponse {
        Forbidden, Invalid, None;

        @Override
        public String toString() {
            return ToStringBuilder.reflectionToString(this);
        }
    }

    private final String accessToken;

    public Authentication() {
        accessToken = null;
    }

    public Authentication(String accessToken) {
        this.accessToken = accessToken;
    }

    public String getAccessToken() {
        return accessToken;
    }
}
