package omstore.util;

import org.apache.http.client.utils.URIBuilder;

import javax.ws.rs.core.HttpHeaders;
import java.net.URI;
import java.net.URISyntaxException;

public class URIUtils {
    public static URI createURI(String url, HttpHeaders headers) throws URISyntaxException {
        URIBuilder uriBuilder = new URIBuilder(URI.create(url));
        if (headers.getRequestHeaders().get("X-Forwarded-Proto") != null) {
            uriBuilder.setScheme("https");
        }

        return uriBuilder.build();
    }
}
