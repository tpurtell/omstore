package omstore.util;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ProdConfiguration {
    @JsonProperty("adminKey")
    public String adminKey;
}
