package omstore.util;

import core.ConfigurationFile;
import core.RandomData;

public class ProdUtils {
    public static void init() {
        ProdConfiguration prodConfiguration = ConfigurationFile.load(ProdConfiguration.class);

        if (prodConfiguration.adminKey == null) {
            prodConfiguration.adminKey = RandomData.randomWebSafeString(10);
            ConfigurationFile.writeConfigForProd(prodConfiguration);
        }
    }

    public static String getAdminKey() {
        ProdConfiguration prodConfiguration = ConfigurationFile.load(ProdConfiguration.class);

        return prodConfiguration.adminKey;
    }
}
