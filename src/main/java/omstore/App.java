package omstore;

import core.LocatorIds;
import core.ServiceLauncher;
import omstore.database.impl.DynamoOmStoreDatabaseProvider;
import omstore.http.OmStoreHttpApi;
import omstore.service.impl.DiskOmStoreProvider;
import omstore.util.ProdUtils;
import org.apache.commons.cli.*;
import org.apache.commons.io.IOUtils;
import org.glassfish.hk2.utilities.ServiceLocatorUtilities;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

public class App {
    public static void main(String[] args) throws IOException, InterruptedException {
        Options options = new Options();

        options.addOption("help", false, "Show this message");
        options.addOption("base", true, "Base url for the service");

        CommandLineParser parser = new BasicParser();
        CommandLine cmd;
        try {
            cmd = parser.parse(options, args);
        } catch (ParseException e) {
            System.err.println("Failed to parse options: " + e);
            System.exit(1);
            return;
        }

        IOUtils.toString(new ByteArrayInputStream(new byte[0]));

        if (cmd.hasOption("help")) {
            HelpFormatter formatter = new HelpFormatter();
            formatter.printHelp("ant", options);
            return;
        }

        URI base_uri;
        try {
            base_uri = new URI(cmd.getOptionValue("base", "http://127.0.0.1:2653"));
        } catch (URISyntaxException e) {
            System.err.println("Bad base URI: " + e);
            System.exit(1);
            return;
        }
        ProdUtils.init();

        ServiceLauncher launcher = new ServiceLauncher(base_uri, new OmStoreHttpApi());

        ServiceLocatorUtilities.addOneConstant(launcher.locator, base_uri, LocatorIds.PUBLIC_URI,
                URI.class);

        ServiceLocatorUtilities.addOneConstant(launcher.locator, "PR_", LocatorIds.TABLE_BASE,
                String.class);
        new DynamoOmStoreDatabaseProvider().bind(launcher.locator);
        new DiskOmStoreProvider().bind(launcher.locator);

        launcher.start();

        Thread.currentThread().join();
    }
}
