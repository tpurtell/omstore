package core;

import java.nio.ByteBuffer;
import java.security.SecureRandom;

import org.junit.Assert;
import org.junit.Test;

import com.google.common.primitives.UnsignedBytes;

import core.KeyEncoding.AccountAndContainer;

public class KeyEncodingTest {
    SecureRandom random = new SecureRandom();

    @Test
    public void longBigEndianRoundTrip() {
        Assert.assertEquals(Long.MIN_VALUE,
                KeyEncoding.decodeLongBigEndian((KeyEncoding.encodeLongBigEndian(Long.MIN_VALUE))));
        Assert.assertEquals(1000,
                KeyEncoding.decodeLongBigEndian((KeyEncoding.encodeLongBigEndian(1000))));
        Assert.assertEquals(Long.MAX_VALUE,
                KeyEncoding.decodeLongBigEndian((KeyEncoding.encodeLongBigEndian(Long.MAX_VALUE))));
    }

    @Test
    public void longBigEndianByteOder() {
        long s2_7 = 1 << 7;
        long s2_8 = 1 << 8;
        long s2_62 = 1 << 62;
        ByteBuffer b2_7 = KeyEncoding.encodeLongBigEndian(s2_7);
        ByteBuffer b2_8 = KeyEncoding.encodeLongBigEndian(s2_8);
        ByteBuffer b2_62 = KeyEncoding.encodeLongBigEndian(s2_62);
        Assert.assertTrue(UnsignedBytes.lexicographicalComparator().compare(b2_7.array(),
                b2_8.array()) < 0);
        Assert.assertTrue(UnsignedBytes.lexicographicalComparator().compare(b2_8.array(),
                b2_62.array()) < 0);
    }

    @Test
    public void boolRoundTrip() {
        Assert.assertEquals(true, KeyEncoding.decodeBool((KeyEncoding.encodeBool(true))));
        Assert.assertEquals(false, KeyEncoding.decodeBool((KeyEncoding.encodeBool(false))));
    }

    @Test
    public void accountAndContainerRoundTrip() {
        String account = RandomData.randomString(10, 20);
        String container = RandomData.randomString(20, 30);

        ByteBuffer buf = KeyEncoding.encodeAccountAndContainer(account, container);
        Assert.assertNotNull(buf);

        AccountAndContainer ac = KeyEncoding.decodeAccountAndContainer(buf);
        Assert.assertNotNull(ac);
        Assert.assertEquals(account, ac.account);
        Assert.assertEquals(container, ac.container);
    }

}
