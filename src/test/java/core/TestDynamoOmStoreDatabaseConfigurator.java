package core;

import omstore.database.impl.DynamoOmStoreDatabaseProvider;

import org.glassfish.hk2.api.ServiceLocator;
import org.glassfish.hk2.utilities.ServiceLocatorUtilities;

public class TestDynamoOmStoreDatabaseConfigurator extends DynamoOmStoreDatabaseProvider {
    @Override
    public void bind(ServiceLocator locator) {
        super.bind(locator);
        ServiceLocatorUtilities.addOneConstant(locator, DynamoUtils.TEST_TABLE_BASE,
                LocatorIds.TABLE_BASE, String.class);
    }
}
