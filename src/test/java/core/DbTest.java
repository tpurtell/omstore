package core;

import org.glassfish.hk2.api.ServiceLocator;
import org.glassfish.hk2.api.ServiceLocatorFactory;
import org.junit.AfterClass;
import org.junit.Before;

public abstract class DbTest {
    public static ServiceLocator locator = ServiceLocatorFactory.getInstance().create(null);
    public static boolean initializedDatabase = false;

    // register the db provider for whatever impl you want to test
    public abstract ServiceConfigurationProvider getDatabaseConfigurator();

    @Before
    public void initializeDatabase() {
        if (!initializedDatabase) {
            getDatabaseConfigurator().bind(locator);
            initializedDatabase = true;
        }
    }

    @AfterClass
    public static void resetDatabase() {
        initializedDatabase = false;
    }
}
