package core;

import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.log4j.Logger;
import org.glassfish.hk2.api.ServiceLocator;
import org.glassfish.hk2.api.ServiceLocatorFactory;
import org.glassfish.hk2.utilities.ServiceLocatorUtilities;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClient;
import com.amazonaws.services.dynamodbv2.model.AttributeDefinition;
import com.amazonaws.services.dynamodbv2.model.AttributeValue;
import com.amazonaws.services.dynamodbv2.model.CreateTableRequest;
import com.amazonaws.services.dynamodbv2.model.KeySchemaElement;
import com.amazonaws.services.dynamodbv2.model.KeyType;
import com.amazonaws.services.dynamodbv2.model.ProvisionedThroughput;
import com.amazonaws.services.dynamodbv2.model.ScalarAttributeType;
import com.amazonaws.services.dynamodbv2.util.Tables;

public class LocalDynamoTest {
    public final ServiceLocator locator = ServiceLocatorFactory.getInstance().create(null);
    static Logger logger = Logger.getLogger(LocalDynamoTest.class);

    @Before
    public void setupDynamo() {
        ServiceLocatorUtilities.addOneConstant(locator, DynamoUtils.getDynamoClient());
    }

    @Test
    public void createDeleteTable() {
        final String table = "TestTableForDelete";
        final String hash_key = "HK";
        final String range_key = "RK";
        AmazonDynamoDBClient db = locator.getService(AmazonDynamoDBClient.class);

        if (Tables.doesTableExist(db, table))
            db.deleteTable(table);

        DynamoUtils.waitForTableToBeDeleted(db, table);

        ArrayList<KeySchemaElement> ks = new ArrayList<KeySchemaElement>();
        ArrayList<AttributeDefinition> attributeDefinitions = new ArrayList<AttributeDefinition>();

        ks.add(new KeySchemaElement().withAttributeName(hash_key).withKeyType(KeyType.HASH));
        attributeDefinitions.add(new AttributeDefinition().withAttributeName(hash_key)
                .withAttributeType(ScalarAttributeType.S));

        ks.add(new KeySchemaElement().withAttributeName(range_key).withKeyType(KeyType.RANGE));
        attributeDefinitions.add(new AttributeDefinition().withAttributeName(range_key)
                .withAttributeType(ScalarAttributeType.S));

        ProvisionedThroughput provisionedthroughput = new ProvisionedThroughput()
                .withReadCapacityUnits(1L).withWriteCapacityUnits(1L);

        CreateTableRequest request = new CreateTableRequest().withTableName(table)
                .withKeySchema(ks).withProvisionedThroughput(provisionedthroughput);

        request.setAttributeDefinitions(attributeDefinitions);

        db.createTable(request);

        logger.info("created " + table);

        Tables.waitForTableToBecomeActive(db, table);

        db.deleteTable(table);

        DynamoUtils.waitForTableToBeDeleted(db, table);
    }

    @Test
    public void temporarySerialize() {
        Map<String, AttributeValue> a = new HashMap<>();
        a.put("a", new AttributeValue().withS("b"));
        a.put("b", new AttributeValue().withB(ByteBuffer.wrap(RandomData.randomBytes(16))));
        Map<String, AttributeValue> b = DynamoUtils.temporaryDeserializeKey(DynamoUtils
                .temporarySerializeKey(a));
        Iterator<Entry<String, AttributeValue>> ia = a.entrySet().iterator();
        Iterator<Entry<String, AttributeValue>> ib = b.entrySet().iterator();

        for (;;) {
            Assert.assertFalse(ia.hasNext() ^ ib.hasNext());
            if (!ia.hasNext())
                break;
            Entry<String, AttributeValue> aa = ia.next();
            Entry<String, AttributeValue> bb = ib.next();
            Assert.assertEquals(aa.getKey(), bb.getKey());
            Assert.assertEquals(aa.getValue().getS(), bb.getValue().getS());
        }
    }
}
