package core;

import omstore.service.impl.DiskFileContentService;
import omstore.service.impl.OmStoreProvider;
import org.glassfish.hk2.api.ServiceLocator;
import org.glassfish.hk2.utilities.ServiceLocatorUtilities;

public class TestDiskOmStoreProvider extends OmStoreProvider {
    public void bind(ServiceLocator locator) {
        super.bind(locator);
        ServiceLocatorUtilities.addOneConstant(locator, "storage_test", LocatorIds.LOCAL_STORAGE_PATH,
                String.class);
        ServiceLocatorUtilities.addClasses(locator, DiskFileContentService.class);
    }
}
