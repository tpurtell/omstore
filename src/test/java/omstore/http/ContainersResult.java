package omstore.http;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Collection;

public class ContainersResult {
    @JsonProperty
    private Collection<String> containers;
    @JsonProperty
    private byte[] continuationKey;

    public Collection<String> getContainers() {
        return containers;
    }
}