package omstore.http;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Collection;

public class FilesResult {
    @JsonProperty
    public Collection<FileInfo> files;
    @JsonProperty
    public byte[] continuationKey;
}