package omstore.http.dynamo.disk;

import core.ServiceConfigurationProvider;
import core.TestDynamoOmStoreDatabaseConfigurator;

import omstore.http.NetworkOAuthContentTest;
import omstore.service.impl.DiskOmStoreProvider;


public class DynamoDiskNetworkOAuthContentTest extends NetworkOAuthContentTest {
    @Override
    public ServiceConfigurationProvider getDatabaseConfigurator() {
        return new TestDynamoOmStoreDatabaseConfigurator();
    }

    @Override
    public ServiceConfigurationProvider getServiceConfigurator() {
        return new DiskOmStoreProvider();
    }
}
