package omstore.http.dynamo.disk;

import core.ServiceConfigurationProvider;
import core.TestDiskOmStoreProvider;
import core.TestDynamoOmStoreDatabaseConfigurator;
import omstore.http.NetworkContainerContentTest;
import org.apache.commons.io.FileUtils;
import org.junit.After;

import java.io.File;
import java.io.IOException;

public class DynamoDiskNetworkContainerContentTest extends NetworkContainerContentTest {
    @Override
    public ServiceConfigurationProvider getDatabaseConfigurator() {
        return new TestDynamoOmStoreDatabaseConfigurator();
    }

    @Override
    public ServiceConfigurationProvider getServiceConfigurator() {
        return new TestDiskOmStoreProvider();
    }

    @After
    public void tearDown() {
        try {
            FileUtils.deleteDirectory(new File("storage_test"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
