package omstore.http.dynamo.disk;


import core.ServiceConfigurationProvider;
import core.TestDynamoOmStoreDatabaseConfigurator;
import omstore.http.NetworkOAuthContentTest;
import omstore.http.NetworkUserContentTest;
import omstore.service.impl.DiskOmStoreProvider;

public class DynamoDiskNetworkUserContentTest extends NetworkUserContentTest {
    @Override
    public ServiceConfigurationProvider getDatabaseConfigurator() {
        return new TestDynamoOmStoreDatabaseConfigurator();
    }

    @Override
    public ServiceConfigurationProvider getServiceConfigurator() {
        return new DiskOmStoreProvider();
    }
}
