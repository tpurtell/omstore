package omstore.http;

import com.google.common.hash.Hashing;
import com.google.common.io.BaseEncoding;
import core.LocatorIds;
import core.WebClient;
import omstore.ServiceTest;
import omstore.database.ContainerDb;
import omstore.database.TokenDb;
import org.bouncycastle.util.encoders.UrlBase64;
import org.junit.Before;
import org.junit.Test;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import java.io.ByteArrayInputStream;
import java.net.URI;

import static core.RandomData.randomBytes;
import static core.RandomData.randomWebSafeString;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public abstract class NetworkFileContentTest extends ServiceTest {
    Client client;
    URI baseUri;
    TokenDb tokenDb;
    String account;
    String accessToken;

    @Before
    public void setup() {
        tokenDb = launcher.locator.getService(TokenDb.class);
        ContainerDb containerDb = launcher.locator.getService(ContainerDb.class);
        containerDb.deleteTables();
        containerDb.createTables();
        tokenDb.createTables();
        client = WebClient.getWebClient();
        baseUri = launcher.locator.getService(URI.class, LocatorIds.PUBLIC_URI);
        account = randomWebSafeString(16);
        accessToken = randomWebSafeString(16);
        tokenDb.setTokenForAccount(account, accessToken);
    }

    @Test
    public void putBadAuth() {
        byte[] data = randomBytes(4096, 8192);
        ByteArrayInputStream in = new ByteArrayInputStream(data);
        String account = randomWebSafeString(16);
        String accessToken = randomWebSafeString(16);
        String container = randomWebSafeString(16);
        String specifiedId = randomWebSafeString(16);
        Response response = client.target(baseUri).path("api").path("file").path(account)
                .path(container).path(specifiedId).request()
                .header("Authorization", accessToken)
                .put(Entity.entity(in, MediaType.APPLICATION_OCTET_STREAM));
        assertEquals(Status.FORBIDDEN, Status.fromStatusCode(response.getStatus()));
    }

    @Test
    public void getMissing() {
        byte[] data = randomBytes(4096, 8192);
        ByteArrayInputStream in = new ByteArrayInputStream(data);
        String container = randomWebSafeString(16);
        String specifiedId = randomWebSafeString(16);
        Response response = client.target(baseUri).path("api").path("file").path(account)
                .path(container).path(specifiedId).request()
                .header("Authorization", accessToken).get();
        assertEquals(Status.NOT_FOUND, Status.fromStatusCode(response.getStatus()));
    }

    @Test
    public void shouldGetFile() {
        byte[] data = randomBytes(4096, 8192);
        ByteArrayInputStream in = new ByteArrayInputStream(data);
        String container = randomWebSafeString(16);
        String specifiedId = randomWebSafeString(16);
        String md5 = BaseEncoding.base64().encode(Hashing.md5().hashBytes(data).asBytes());
        client.target(baseUri).path("api").path("file").path(account)
                .path(container).path(specifiedId).request().header("Content-MD5", md5)
                .header("Authorization", accessToken)
                .header("Content-Length", data.length)
                .put(Entity.entity(in, MediaType.APPLICATION_OCTET_STREAM));

        Response response = client.target(baseUri).path("api").path("file").path(account)
                .path(container).path(specifiedId).request().header("Authorization", accessToken)
                .get();

        assertEquals(Status.OK, Status.fromStatusCode(response.getStatus()));
    }

    @Test
    public void shouldDeleteFile() {
        byte[] data = randomBytes(4096, 8192);
        ByteArrayInputStream in = new ByteArrayInputStream(data);
        String container = randomWebSafeString(16);
        String specifiedId = randomWebSafeString(16);
        String md5 = BaseEncoding.base64().encode(Hashing.md5().hashBytes(data).asBytes());
        client.target(baseUri).path("api").path("file").path(account)
                .path(container).path(specifiedId).request().header("Content-MD5", md5)
                .header("Authorization", accessToken)
                .header("Content-Length", data.length)
                .put(Entity.entity(in, MediaType.APPLICATION_OCTET_STREAM));

        Response deleteRequestResponse = client.target(baseUri).path("api").path("file").path(account)
                .path(container).path(specifiedId).request()
                .header("Authorization", accessToken).delete();

        assertEquals(Status.OK, Status.fromStatusCode(deleteRequestResponse.getStatus()));
        Response getRequestResponse = client.target(baseUri).path("api").path("file").path(account)
                .path(container).path(specifiedId).request()
                .header("Authorization", accessToken).get();
        assertEquals(Status.NOT_FOUND, Status.fromStatusCode(getRequestResponse.getStatus()));
    }

    @Test
    public void shouldListFilesForGivenContainer() {
        String container = randomWebSafeString(16);
        int numberOfFiles = 100;
        for (int i = 1; i <= numberOfFiles; i++) {
            String specifiedId = randomWebSafeString(i);
            byte[] data = randomBytes(4096, 8192);
            ByteArrayInputStream in = new ByteArrayInputStream(data);
            String md5 = BaseEncoding.base64().encode(Hashing.md5().hashBytes(data).asBytes());

            Response response = client.target(baseUri).path("api").path("file").path(account)
                    .path(container).path(specifiedId).request().header("Content-MD5", md5)
                    .header("Authorization", accessToken)
                    .header("Content-Length", data.length)
                    .put(Entity.entity(in, MediaType.APPLICATION_OCTET_STREAM));
            assertEquals(Status.OK, Status.fromStatusCode(response.getStatus()));
        }

        Response response = client.target(baseUri).path("api").path("file").path(account)
                .path(container).path("files").queryParam("tag", "").queryParam("mimeType", "")
                .request().header("Authorization", accessToken).get();

        FilesResult fileResult = response.readEntity(FilesResult.class);
        assertEquals(numberOfFiles, fileResult.files.size());
    }

    @Test
    public void getTokensTest() {
        String account = randomWebSafeString(16);
        String password = randomWebSafeString(16);
        Response response = client.target(baseUri).path("api").path("authorize").queryParam("account", account)
                .queryParam("password", password).request().get();

        String token = response.readEntity(String.class);
        assertNotNull(token);
    }

    @Test
    public void shouldContinueUploadingFile() {
        byte[] data = randomBytes(4096, 8192);
        ByteArrayInputStream in = new ByteArrayInputStream(data);
        String container = randomWebSafeString(16);
        String specifiedId = randomWebSafeString(16);
        String md5 = BaseEncoding.base64().encode(Hashing.md5().hashBytes(data).asBytes());
        Response uploadResponse = client.target(baseUri).path("api").path("file").path(account)
                .path(container).path(specifiedId).request().header("Content-MD5", md5)
                .header("Authorization", accessToken)
                .header("Content-Length", data.length)
                .header("Expect", "100-continue")
                .put(Entity.entity(in, MediaType.APPLICATION_OCTET_STREAM));

        Response getUploadedFileResponse = client.target(baseUri).path("api").path("file").path(account)
                .path(container).path(specifiedId).request().header("Authorization", accessToken)
                .get();

        assertEquals(Status.OK, Status.fromStatusCode(uploadResponse.getStatus()));
        assertEquals(Status.OK, Status.fromStatusCode(getUploadedFileResponse.getStatus()));
    }

    @Test
    public void shouldNotUploadFileOnChecksumMismatch() {
        byte[] data = randomBytes(4096, 8192);
        ByteArrayInputStream in = new ByteArrayInputStream(data);
        String container = randomWebSafeString(16);
        String specifiedId = randomWebSafeString(16);
        String md5 = BaseEncoding.base64().encode(randomBytes(16));
        Response uploadResponse = client.target(baseUri).path("api").path("file").path(account)
                .path(container).path(specifiedId).request().header("Content-MD5", md5)
                .header("Authorization", accessToken)
                .header("Content-Length", data.length)
                .header("Expect", "100-continue")
                .put(Entity.entity(in, MediaType.APPLICATION_OCTET_STREAM));

        Response getUploadedFileResponse = client.target(baseUri).path("api").path("file").path(account)
                .path(container).path(specifiedId).request().header("Authorization", accessToken)
                .get();

        assertEquals(Status.BAD_REQUEST, Status.fromStatusCode(uploadResponse.getStatus()));
        assertEquals(Status.NOT_FOUND, Status.fromStatusCode(getUploadedFileResponse.getStatus()));
    }

    @Test
    public void shouldNotContinueUploadingDuplicateFile() {
        byte[] data = randomBytes(4096, 8192);
        ByteArrayInputStream in = new ByteArrayInputStream(data);
        String containerOne = randomWebSafeString(16);
        String containerTwo = randomWebSafeString(16);
        String specifiedId = randomWebSafeString(16);
        String md5 = BaseEncoding.base64().encode(Hashing.md5().hashBytes(data).asBytes());
        Response firstUploadResponse = client.target(baseUri).path("api").path("file").path(account)
                .path(containerOne).path(specifiedId).request().header("Content-MD5", md5)
                .header("Authorization", accessToken)
                .header("Content-Length", data.length)
                .header("Expect", "100-continue")
                .put(Entity.entity(in, MediaType.APPLICATION_OCTET_STREAM));

        in = new ByteArrayInputStream(data);
        Response duplicateUploadResponse = client.target(baseUri).path("api").path("file").path(account)
                .path(containerTwo).path(specifiedId).request().header("Content-MD5", md5)
                .header("Authorization", accessToken)
                .header("Content-Length", data.length)
                .header("Expect", "100-continue")
                .put(Entity.entity(in, MediaType.APPLICATION_OCTET_STREAM));
        Response getFirstUploadFileResponse = client.target(baseUri).path("api").path("file").path(account)
                .path(containerOne).path(specifiedId).request().header("Authorization", accessToken)
                .get();
        Response getDuplicateUploadFileResponse = client.target(baseUri).path("api").path("file").path(account)
                .path(containerTwo).path(specifiedId).request().header("Authorization", accessToken)
                .get();

        assertEquals(Status.OK, Status.fromStatusCode(firstUploadResponse.getStatus()));
        assertEquals(Status.OK, Status.fromStatusCode(getFirstUploadFileResponse.getStatus()));
        assertEquals(Status.EXPECTATION_FAILED, Status.fromStatusCode(duplicateUploadResponse.getStatus()));
        assertEquals(Status.OK, Status.fromStatusCode(getDuplicateUploadFileResponse.getStatus()));
    }

    @Test
    public void shouldDeleteOlderFileOnConflictingFileUpload() {
        byte[] data = randomBytes(4096, 8192);
        ByteArrayInputStream in = new ByteArrayInputStream(data);
        String container = randomWebSafeString(16);
        String specifiedId = randomWebSafeString(16);
        String md5 = BaseEncoding.base64().encode(Hashing.md5().hashBytes(data).asBytes());

        Response firstUploadResponse = client.target(baseUri).path("api").path("file").path(account)
                .path(container).path(specifiedId).request().header("Content-MD5", md5)
                .header("Authorization", accessToken)
                .header("Content-Length", data.length)
                .header("Expect", "100-continue")
                .put(Entity.entity(in, MediaType.APPLICATION_OCTET_STREAM));

        data = randomBytes(4096, 8192);
        in = new ByteArrayInputStream(data);
        md5 = BaseEncoding.base64().encode(Hashing.md5().hashBytes(data).asBytes());

        Response conflictingUploadResponse = client.target(baseUri).path("api").path("file").path(account)
                .path(container).path(specifiedId).request().header("Content-MD5", md5)
                .header("Authorization", accessToken)
                .header("Content-Length", data.length)
                .header("Expect", "100-continue")
                .put(Entity.entity(in, MediaType.APPLICATION_OCTET_STREAM));

        Response fetchFileResponse = client.target(baseUri).path("api").path("file").path(account)
                .path(container).path(specifiedId).request().header("Authorization", accessToken)
                .get();
        byte[] fetchedData = fetchFileResponse.readEntity(byte[].class);

        assertEquals(Status.OK, Status.fromStatusCode(firstUploadResponse.getStatus()));
        assertEquals(Status.OK, Status.fromStatusCode(conflictingUploadResponse.getStatus()));
        assertEquals(md5, BaseEncoding.base64().encode(Hashing.md5().hashBytes(fetchedData).asBytes()));
    }

    @Test
    public void shouldReturnErrorStatusCodeWhenDeletingNonExistentFile() {
        String container = randomWebSafeString(16);
        String specifiedId = randomWebSafeString(16);

        Response deleteRequestResponse = client.target(baseUri).path("api").path("file").path(account)
                .path(container).path(specifiedId).request()
                .header("Authorization", accessToken).delete();

        assertEquals(Status.NOT_FOUND, Status.fromStatusCode(deleteRequestResponse.getStatus()));
    }

    @Test
    public void shouldDeleteFileFromStorageOnlyForLastReference() {
        byte[] data = randomBytes(4096, 8192);
        ByteArrayInputStream in = new ByteArrayInputStream(data);
        String containerOne = randomWebSafeString(16);
        String containerTwo = randomWebSafeString(16);
        String specifiedId = randomWebSafeString(16);
        String md5 = BaseEncoding.base64().encode(Hashing.md5().hashBytes(data).asBytes());
        client.target(baseUri).path("api").path("file").path(account)
                .path(containerOne).path(specifiedId).request().header("Content-MD5", md5)
                .header("Authorization", accessToken)
                .header("Content-Length", data.length)
                .header("Expect", "100-continue")
                .put(Entity.entity(in, MediaType.APPLICATION_OCTET_STREAM));
        //need a new instance, the other one was consumed
        in = new ByteArrayInputStream(data);
        client.target(baseUri).path("api").path("file").path(account)
                .path(containerTwo).path(specifiedId).request().header("Content-MD5", md5)
                .header("Authorization", accessToken)
                .header("Content-Length", data.length)
                .header("Expect", "100-continue")
                .put(Entity.entity(in, MediaType.APPLICATION_OCTET_STREAM));

        Response deleteFirstUploadResponse = client.target(baseUri).path("api").path("file").path(account)
                .path(containerOne).path(specifiedId).request()
                .header("Authorization", accessToken).delete();
        Response getDuplicateUploadFileResponseOne = client.target(baseUri).path("api").path("file").path(account)
                .path(containerTwo).path(specifiedId).request().header("Authorization", accessToken)
                .get();
        Response deleteDuplicateUploadResponse = client.target(baseUri).path("api").path("file").path(account)
                .path(containerTwo).path(specifiedId).request()
                .header("Authorization", accessToken).delete();
        Response getDuplicateUploadFileResponseTwo = client.target(baseUri).path("api").path("file").path(account)
                .path(containerTwo).path(specifiedId).request().header("Authorization", accessToken)
                .get();

        assertEquals(Status.OK, Status.fromStatusCode(deleteFirstUploadResponse.getStatus()));
        assertEquals(Status.OK, Status.fromStatusCode(getDuplicateUploadFileResponseOne.getStatus()));
        assertEquals(Status.OK, Status.fromStatusCode(deleteDuplicateUploadResponse.getStatus()));
        assertEquals(Status.NOT_FOUND, Status.fromStatusCode(getDuplicateUploadFileResponseTwo.getStatus()));
    }

    @Test
    public void shouldInsertAndGetFileByTagAndType() {
        String container = randomWebSafeString(16);
        String tagOne = randomWebSafeString(16);
        String tagTwo = randomWebSafeString(16);
        String typeOne = MediaType.APPLICATION_OCTET_STREAM;
        String typeTwo = MediaType.APPLICATION_FORM_URLENCODED;

        for (int i = 0; i < 5; i++) {
            byte[] data = randomBytes(4096, 8192);
            ByteArrayInputStream in = new ByteArrayInputStream(data);
            String specifiedId = randomWebSafeString(16);
            String md5 = BaseEncoding.base64().encode(Hashing.md5().hashBytes(data).asBytes());
            client.target(baseUri).path("api").path("file").path(account)
                    .path(container).path(specifiedId).request().header("Content-MD5", md5)
                    .header("Authorization", accessToken)
                    .header("Content-Length", data.length)
                    .header("Content-Tag", tagOne)
                    .put(Entity.entity(in, typeOne));
        }

        for (int i = 0; i < 10; i++) {
            byte[] data = randomBytes(4096, 8192);
            ByteArrayInputStream in = new ByteArrayInputStream(data);
            String specifiedId = randomWebSafeString(16);
            String md5 = BaseEncoding.base64().encode(Hashing.md5().hashBytes(data).asBytes());
            client.target(baseUri).path("api").path("file").path(account)
                    .path(container).path(specifiedId).request().header("Content-MD5", md5)
                    .header("Authorization", accessToken)
                    .header("Content-Length", data.length)
                    .header("Content-Tag", tagTwo)
                    .put(Entity.entity(in, typeTwo));
        }

        Response correctResponseOne = client.target(baseUri).path("api").path("file").path(account)
                .path(container).path("files").queryParam("tag", tagOne).queryParam("mimeType", typeOne)
                .request().header("Authorization", accessToken).get();
        FilesResult fileResultOne = correctResponseOne.readEntity(FilesResult.class);
        Response correctResponseTwo = client.target(baseUri).path("api").path("file").path(account)
                .path(container).path("files").queryParam("tag", tagTwo).queryParam("mimeType", typeTwo)
                .request().header("Authorization", accessToken).get();
        FilesResult fileResultTwo = correctResponseTwo.readEntity(FilesResult.class);
        Response wrongResponseOne = client.target(baseUri).path("api").path("file").path(account)
                .path(container).path("files").queryParam("tag", tagOne).queryParam("mimeType", typeTwo)
                .request().header("Authorization", accessToken).get();
        Response wrongResponseTwo = client.target(baseUri).path("api").path("file").path(account)
                .path(container).path("files").queryParam("tag", tagTwo).queryParam("mimeType", typeOne)
                .request().header("Authorization", accessToken).get();

        assertEquals(Status.OK, Status.fromStatusCode(correctResponseOne.getStatus()));
        assertEquals(Status.OK, Status.fromStatusCode(correctResponseTwo.getStatus()));
        assertEquals(Status.NOT_FOUND, Status.fromStatusCode(wrongResponseOne.getStatus()));
        assertEquals(Status.NOT_FOUND, Status.fromStatusCode(wrongResponseTwo.getStatus()));
        assertEquals(5, fileResultOne.files.size());
        assertEquals(10, fileResultTwo.files.size());
        for (FileInfo file : fileResultOne.files) {
            assertEquals(tagOne, file.tag);
            assertEquals(typeOne, file.mimeType);
        }
        for (FileInfo file : fileResultTwo.files) {
            assertEquals(tagTwo, file.tag);
            assertEquals(typeTwo, file.mimeType);
        }
    }

    @Test
    public void shouldHitResletHandlerForApiUrlPath() {
        String container = randomWebSafeString(16);
        String[] fileTypes = new String[]{".html", ".jpg", ".css", ".png", ".js", ".map", ".gif"};

        for (String fileType : fileTypes) {
            byte[] data = randomBytes(4096, 8192);
            ByteArrayInputStream in = new ByteArrayInputStream(data);
            String md5 = BaseEncoding.base64().encode(Hashing.md5().hashBytes(data).asBytes());
            String specifiedId = randomWebSafeString(16) + fileType;
            client.target(baseUri).path("api").path("file").path(account)
                    .path(container).path(specifiedId).request().header("Content-MD5", md5)
                    .header("Authorization", accessToken)
                    .header("Content-Length", data.length)
                    .put(Entity.entity(in, MediaType.APPLICATION_OCTET_STREAM));

            Response response = client.target(baseUri).path("api").path("file").path(account)
                    .path(container).path(specifiedId).request().header("Authorization", accessToken)
                    .get();

            assertEquals(Status.OK, Status.fromStatusCode(response.getStatus()));
        }
    }

    @Test
    public void shouldGetFileUsingMD5() {
        byte[] data = randomBytes(4096, 8192);
        ByteArrayInputStream in = new ByteArrayInputStream(data);
        String container = randomWebSafeString(16);
        String specifiedId = randomWebSafeString(16);
        String md5 = BaseEncoding.base64().encode(Hashing.md5().hashBytes(data).asBytes());
        client.target(baseUri).path("api").path("file").path(account)
                .path(container).path(specifiedId).request().header("Content-MD5", md5)
                .header("Authorization", accessToken)
                .header("Content-Length", data.length)
                .put(Entity.entity(in, MediaType.APPLICATION_OCTET_STREAM));

        Response response = client.target(baseUri).path("api").path("file")
                .path(new String(UrlBase64.encode(md5.getBytes())))
                .request().get();

        byte[] fetchedData = response.readEntity(byte[].class);
        assertEquals(Status.OK, Status.fromStatusCode(response.getStatus()));
        assertEquals(md5, BaseEncoding.base64().encode(Hashing.md5().hashBytes(fetchedData).asBytes()));
    }
}
