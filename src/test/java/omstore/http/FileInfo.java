package omstore.http;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class FileInfo {
    @JsonProperty
    public String account;
    @JsonProperty
    public String container;
    @JsonProperty
    public String specifiedId;
    @JsonProperty
    public String relativeStorageUrl;
    @JsonProperty
    public String md5;
    @JsonProperty
    public String tag;
    @JsonProperty
    public String mimeType;
    @JsonProperty
    public long size;
    @JsonProperty
    public long uploadDate;
}
