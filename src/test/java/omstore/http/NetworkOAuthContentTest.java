package omstore.http;

import com.google.common.hash.Hashing;
import com.google.common.io.BaseEncoding;
import core.LocatorIds;
import core.RandomData;
import core.WebClient;
import omstore.ServiceTest;
import omstore.database.TokenDb;
import omstore.database.UserDb;
import omstore.http.oauth2.AccessTokenResponse;
import org.junit.Before;
import org.junit.Test;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.*;
import java.net.URI;

import static omstore.http.oauth2.Common.CLIENT_ID;
import static omstore.http.oauth2.Common.CLIENT_SECRET;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public abstract class NetworkOAuthContentTest extends ServiceTest {
    Client client;
    URI baseUri;
    TokenDb tokenDb;
    UserDb userDb;

    @Before
    public void setup() {
        tokenDb = launcher.locator.getService(TokenDb.class);
        userDb = launcher.locator.getService(UserDb.class);
        tokenDb.deleteTables();
        tokenDb.createTables();
        userDb.createTables();
        userDb.registerUser(generateHash("test"), generateHash("test"));
        client = WebClient.getWebClient();
        baseUri = launcher.locator.getService(URI.class, LocatorIds.PUBLIC_URI);
    }

    private String generateHash(String str) {
        return BaseEncoding.base32().encode(
                Hashing.md5().newHasher().putBytes(str.getBytes())
                        .hash()
                        .asBytes());
    }

    @Test
    public void shouldOpenSignInPage() {
        Response response = client.target(baseUri).path("api").path("auth").path("signin").queryParam("client_id", CLIENT_ID)
                .queryParam("response_type", "code").queryParam("redirect_uri", "urn:ietf:wg:oauth:2.0:oob:auto")
                .request().get();
        assertEquals(Response.Status.OK, Response.Status.fromStatusCode(response.getStatus()));
    }

    @Test
    public void shouldNotOpenSignInPageForInvalidRequests() {
        Response codeResponse = client.target(baseUri).path("api").path("auth").path("signin")
                .queryParam("response_type", "code").queryParam("redirect_uri", "urn:ietf:wg:oauth:2.0:oob:auto")
                .request().get();
        assertEquals(Response.Status.BAD_REQUEST, Response.Status.fromStatusCode(codeResponse.getStatus()));
        codeResponse = client.target(baseUri).path("api").path("auth").path("signin").queryParam("client_id", CLIENT_ID)
                .queryParam("redirect_uri", "urn:ietf:wg:oauth:2.0:oob:auto")
                .request().get();
        assertEquals(Response.Status.BAD_REQUEST, Response.Status.fromStatusCode(codeResponse.getStatus()));
        codeResponse = client.target(baseUri).path("api").path("auth").path("signin").queryParam("client_id", CLIENT_ID)
                .queryParam("response_type", "code")
                .request().get();
        assertEquals(Response.Status.BAD_REQUEST, Response.Status.fromStatusCode(codeResponse.getStatus()));
    }

    @Test
    public void shouldGetAccessCodeWithOOBRedirectUrl() {
        String redirectUri = "urn:ietf:wg:oauth:2.0:oob:auto";
        Response signInResponse = client.target(baseUri).path("api").path("auth").path("signin").queryParam("client_id", CLIENT_ID)
                .queryParam("response_type", "code").queryParam("redirect_uri", redirectUri)
                .request().get();
        assertEquals(Response.Status.OK, Response.Status.fromStatusCode(signInResponse.getStatus()));
        MultivaluedMap<String, String> formData = new MultivaluedHashMap<>();
        formData.add("account", "test");
        formData.add("password", "test");
        formData.add("redirect_uri", redirectUri);

        Response userResponse = client.target(baseUri).path("user")
                .request().post(Entity.entity(formData, MediaType.APPLICATION_FORM_URLENCODED));

        assertEquals(Response.Status.OK, Response.Status.fromStatusCode(userResponse.getStatus()));
        String responseEntity = userResponse.readEntity(String.class);
        assertTrue(responseEntity.startsWith("<html><head><title>code="));
        assertTrue(responseEntity.endsWith("</title></head></html>"));
    }

    @Test
    public void shouldNotValidateInvalidUser() {
        Response codeResponse = client.target(baseUri).path("api").path("auth").path("signin").queryParam("client_id", CLIENT_ID)
                .queryParam("response_type", "code").queryParam("redirect_uri", "urn:ietf:wg:oauth:2.0:oob:auto")
                .request().get();
        assertEquals(Response.Status.OK, Response.Status.fromStatusCode(codeResponse.getStatus()));
        MultivaluedMap<String, String> formData = new MultivaluedHashMap<>();
        String redirectUri = "urn:ietf:wg:oauth:2.0:oob:auto";
        formData.add("account", RandomData.randomString(10));
        formData.add("password", RandomData.randomString(10));
        formData.add("redirect_uri", redirectUri);

        Response response = client.target(baseUri).path("user")
                .request().post(Entity.entity(formData, MediaType.APPLICATION_FORM_URLENCODED));

        assertEquals(Response.Status.FORBIDDEN, Response.Status.fromStatusCode(response.getStatus()));
    }


    @Test
    public void shouldGetAccessToken() {
        String redirectUri = "urn:ietf:wg:oauth:2.0:oob:auto";
        Response signInResponse = client.target(baseUri).path("api").path("auth").path("signin").queryParam("client_id", CLIENT_ID)
                .queryParam("response_type", "code").queryParam("redirect_uri", redirectUri)
                .request().get();
        assertEquals(Response.Status.OK, Response.Status.fromStatusCode(signInResponse.getStatus()));
        MultivaluedMap<String, String> formData = new MultivaluedHashMap<>();
        formData.add("account", "test");
        formData.add("password", "test");
        formData.add("redirect_uri", redirectUri);
        Response userResponse = client.target(baseUri).path("user")
                .request().post(Entity.entity(formData, MediaType.APPLICATION_FORM_URLENCODED));

        assertEquals(Response.Status.OK, Response.Status.fromStatusCode(userResponse.getStatus()));
        String responseEntity = userResponse.readEntity(String.class);
        assertTrue(responseEntity.startsWith("<html><head><title>code="));
        assertTrue(responseEntity.endsWith("</title></head></html>"));
        String code = responseEntity.substring(responseEntity.indexOf("code=") + 5, responseEntity.indexOf("</"));
        MultivaluedMap<String, String> formTokenData = new MultivaluedHashMap<>();
        String account = tokenDb.getAccountForAccessCode(code);
        String expires = tokenDb.checkAccountForAccessCode(account, code);
        formTokenData.add("client_id", CLIENT_ID);
        formTokenData.add("client_secret", CLIENT_SECRET);
        formTokenData.add("redirect_uri", redirectUri);
        formTokenData.add("grant_type", "authorization_code");
        formTokenData.add("code", code);

        Response tokenResponse = client.target(baseUri).path("api").path("auth").path("code")
                .request().post(Entity.entity(formTokenData, MediaType.APPLICATION_FORM_URLENCODED));

        if (Long.parseLong(expires) >= System.currentTimeMillis()) {
            assertEquals(Response.Status.OK, Response.Status.fromStatusCode(tokenResponse.getStatus()));
            AccessTokenResponse accessTokenResponse = tokenResponse.readEntity(AccessTokenResponse.class);

            assertEquals(account, accessTokenResponse.getAccount());
            assertEquals(account, tokenDb.getAccountForToken(accessTokenResponse.accessToken()));
            assertEquals(tokenDb.getTokenForAccount(account), accessTokenResponse.accessToken());
        } else {
            assertEquals(Response.Status.BAD_REQUEST, Response.Status.fromStatusCode(tokenResponse.getStatus()));
        }
    }

    @Test
    public void shouldNotGetAccessTokenForInvalidRequest() {
        String redirectUri = "urn:ietf:wg:oauth:2.0:oob:auto";
        Response signInResponse = client.target(baseUri).path("api").path("auth").path("signin").queryParam("client_id", CLIENT_ID)
                .queryParam("response_type", "code").queryParam("redirect_uri", redirectUri)
                .request().get();
        assertEquals(Response.Status.OK, Response.Status.fromStatusCode(signInResponse.getStatus()));
        MultivaluedMap<String, String> formData = new MultivaluedHashMap<>();
        formData.add("account", "test");
        formData.add("password", "test");
        formData.add("redirect_uri", redirectUri);
        Response userResponse = client.target(baseUri).path("user")
                .request().post(Entity.entity(formData, MediaType.APPLICATION_FORM_URLENCODED));

        assertEquals(Response.Status.OK, Response.Status.fromStatusCode(userResponse.getStatus()));
        String responseEntity = userResponse.readEntity(String.class);
        assertTrue(responseEntity.startsWith("<html><head><title>code="));
        assertTrue(responseEntity.endsWith("</title></head></html>"));
        MultivaluedMap<String, String> formTokenData = new MultivaluedHashMap<>();
        formTokenData.add("client_id", CLIENT_ID);
        formTokenData.add("client_secret", CLIENT_SECRET);
        formTokenData.add("redirect_uri", redirectUri);
        formTokenData.add("grant_type", "authorization_code");

        Response tokenResponse = client.target(baseUri).path("api").path("auth").path("code")
                .request().post(Entity.entity(formTokenData, MediaType.APPLICATION_FORM_URLENCODED));

        assertEquals(Response.Status.BAD_REQUEST, Response.Status.fromStatusCode(tokenResponse.getStatus()));
    }
}
