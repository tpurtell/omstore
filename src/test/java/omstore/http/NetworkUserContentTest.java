package omstore.http;


import core.ConfigurationFile;
import core.LocatorIds;
import core.WebClient;
import omstore.ServiceTest;
import omstore.database.UserDb;
import omstore.util.ProdConfiguration;
import org.junit.Before;
import org.junit.Test;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.*;
import java.net.URI;

import static org.junit.Assert.assertEquals;

public abstract class NetworkUserContentTest extends ServiceTest {
    Client client;
    URI baseUri;
    UserDb userDb;

    @Before
    public void setup() {
        userDb = launcher.locator.getService(UserDb.class);
        // tokenDb.deleteTables();
        userDb.createTables();
        client = WebClient.getWebClient();
        baseUri = launcher.locator.getService(URI.class, LocatorIds.PUBLIC_URI);
    }

    @Test
    public void userRegisterRequest() {
        MultivaluedMap<String,String> formData = new MultivaluedHashMap<String,String>();
        String account = "test";
        String password = "test";
        String adminKey = "test";
        ProdConfiguration prodConfiguration = new ProdConfiguration();
        prodConfiguration.adminKey = adminKey;
        ConfigurationFile.writeConfigForProd(prodConfiguration);
        formData.add("account", account);
        formData.add("password", password);
        formData.add("adminKey", adminKey);

        Response response = client.target(baseUri).path("user").path("register")
                .request().post(Entity.entity(formData, MediaType.APPLICATION_FORM_URLENCODED));

        assertEquals(Response.Status.OK, Response.Status.fromStatusCode(response.getStatus()));
    }


}
