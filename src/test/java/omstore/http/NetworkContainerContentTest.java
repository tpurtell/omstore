package omstore.http;

import core.LocatorIds;
import core.RandomData;
import core.WebClient;
import omstore.ServiceTest;
import omstore.contract.ShareContainerResult;
import omstore.database.ContainerDb;
import omstore.database.TokenDb;
import omstore.dto.SharedContainer;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import javax.ws.rs.client.Client;
import javax.ws.rs.core.Response;
import java.net.URI;
import java.util.ArrayList;

import static java.lang.System.currentTimeMillis;

public abstract class NetworkContainerContentTest extends ServiceTest {
    Client client;
    URI baseUri;
    TokenDb tokenDb;
    ContainerDb containerDb;
    String account;
    String accessToken;

    @Before
    public void setup() {
        tokenDb = launcher.locator.getService(TokenDb.class);
        containerDb = launcher.locator.getService(ContainerDb.class);
        tokenDb.createTables();
        containerDb.createTables();
        client = WebClient.getWebClient();
        baseUri = launcher.locator.getService(URI.class, LocatorIds.PUBLIC_URI);
        account = RandomData.randomWebSafeString(16);
        accessToken = RandomData.randomWebSafeString(16);
        tokenDb.setTokenForAccount(account, accessToken);
    }

    @Test
    public void shouldListContainers() {
        ArrayList<String> containers = new ArrayList<>();
        for (int i = 0; i < 6; i++) {
            containers.add(RandomData.randomString(10, 20));
            containerDb.addContainer(account, containers.get(i), currentTimeMillis());
        }

        Response response = client.target(baseUri).path("api").path("container").path(account).path("containers").request()
                .header("Authorization", accessToken).get();
        ContainersResult containersResult = response.readEntity(ContainersResult.class);

        ArrayList<String> resultContainers = (ArrayList<String>) containersResult.getContainers();
        Assert.assertEquals(6, resultContainers.size());
        for (int i = 0; i < 6; i++) {
            Assert.assertEquals(containers.get(6 - i - 1), resultContainers.get(i));
        }
        Assert.assertEquals(Response.Status.OK, Response.Status.fromStatusCode(response.getStatus()));
    }

    @Test
    public void shouldDeleteContainer() {
        String container = RandomData.randomWebSafeString(50);
        containerDb.addContainer(account, container, currentTimeMillis());

        Response deleteContainerResponse = client.target(baseUri).path("api").path("container").path(account)
                .path(container).request().header("Authorization", accessToken).delete();
        Response listContainerResponse = client.target(baseUri).path("api").path("container").path(account)
                .path("containers").request().header("Authorization", accessToken).get();
        ContainersResult containersResult = listContainerResponse.readEntity(ContainersResult.class);
        ArrayList<String> resultContainers = (ArrayList<String>) containersResult.getContainers();

        Assert.assertEquals(Response.Status.OK, Response.Status.fromStatusCode(deleteContainerResponse.getStatus()));
        Assert.assertEquals(0, resultContainers.size());
        Assert.assertEquals(Response.Status.OK, Response.Status.fromStatusCode(listContainerResponse.getStatus()));
    }

    @Test
    public void shouldShareContainer() {
        String container = RandomData.randomWebSafeString(50);
        containerDb.addContainer(account, container, currentTimeMillis());

        Response shareContainerResponse = client.target(baseUri).path("api").path("container").path(account)
                .path(container).path("share").request().header("Authorization", accessToken).get();
        ShareContainerResult shareContainerResult = shareContainerResponse.readEntity(ShareContainerResult.class);
        String sharedAuthToken = shareContainerResult.getSharedAuthToken();

        Assert.assertEquals(Response.Status.OK, Response.Status.fromStatusCode(shareContainerResponse.getStatus()));
        Assert.assertEquals(containerDb.getAuthTokenForSharedContainer(account, container), sharedAuthToken);
    }
}
