package omstore.database;

import core.DbTest;
import core.RandomData;
import omstore.dto.SharedContainer;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static core.RandomData.randomString;
import static core.RandomData.randomWebSafeString;
import static java.lang.System.currentTimeMillis;
import static org.junit.Assert.*;

public abstract class ContainersDbTest extends DbTest {
    protected ContainerDb containersDb;

    @Before
    public void setup() {
        containersDb = locator.getService(ContainerDb.class);
        containersDb.deleteTables();
        containersDb.createTables();
    }

    @Test
    public void deleteTables() {
        containersDb.deleteTables();
    }

    @Test
    public void shouldAddAndListContainer() {
        String account = randomString(10, 20);
        String container = randomString(10, 20);
        containersDb.addContainer(account, container, currentTimeMillis());

        List<String> containers = (List<String>) containersDb.listContainersForAccount(account, null).getContainers();

        assertEquals(1, containers.size());
        assertEquals(container, containers.get(0));
    }

    @Test
    public void shouldListContainersForAccount() {
        String account1 = randomString(10, 20);
        String account2 = randomString(10, 20);
        String container1 = randomString(10, 20);
        String container2 = randomString(10, 20);
        String container3 = randomString(10, 20);
        String container4 = randomString(10, 20);
        String container5 = randomString(10, 20);
        String container6 = randomString(10, 20);
        containersDb.addContainer(account1, container1, currentTimeMillis());
        containersDb.addContainer(account1, container2, currentTimeMillis());
        containersDb.addContainer(account1, container3, currentTimeMillis());
        containersDb.addContainer(account1, container4, currentTimeMillis());
        containersDb.addContainer(account2, container5, currentTimeMillis());
        containersDb.addContainer(account2, container6, currentTimeMillis());

        List<String> containersForAccount1 = (List<String>) containersDb.listContainersForAccount(account1, null).getContainers();
        List<String> containersForAccount2 = (List<String>) containersDb.listContainersForAccount(account2, null).getContainers();

        assertEquals(4, containersForAccount1.size());
        assertEquals(2, containersForAccount2.size());
        assertEquals(container4, containersForAccount1.get(0));
        assertEquals(container3, containersForAccount1.get(1));
        assertEquals(container2, containersForAccount1.get(2));
        assertEquals(container1, containersForAccount1.get(3));
        assertEquals(container6, containersForAccount2.get(0));
        assertEquals(container5, containersForAccount2.get(1));
    }

    @Test
    public void shouldDeleteContainer() {
        String account = randomString(10, 20);
        String container = randomString(10, 20);
        containersDb.addContainer(account, container, currentTimeMillis());

        containersDb.deleteContainer(account, container);

        List<String> containersForAccount = (List<String>) containersDb.listContainersForAccount(account, null).getContainers();
        assertEquals(0, containersForAccount.size());
    }

    @Test
    public void shouldGetLargeListOfContainers() {
        String account = randomString(32);
        List<String> expectedContainers = new ArrayList<>();
        List<String> actualContainers = new ArrayList<>();
        byte[] continuationKey = null;
        int numberOfEntries = 0;
        while (numberOfEntries < 1024 * 1024) {
            String container = randomString(1024);
            containersDb.addContainer(account, container, RandomData.randomPositiveLong());
            expectedContainers.add(container);
            numberOfEntries += container.length();
        }


        ContainerDb.ContainerResult containerResult = containersDb.listContainersForAccount(account, continuationKey);
        continuationKey = containerResult.getContinuationKey();
        assertNotNull(continuationKey);
        actualContainers.addAll(containerResult.getContainers());
        containerResult = containersDb.listContainersForAccount(account, continuationKey);
        continuationKey = containerResult.getContinuationKey();
        assertNull(continuationKey);
        actualContainers.addAll(containerResult.getContainers());

        assertEquals(expectedContainers.size(), actualContainers.size());
    }

    @Test
    public void shouldGetContainerForAccount() {
        String account = randomString(10, 20);
        String container = randomString(10, 20);
        containersDb.addContainer(account, container, currentTimeMillis());

        ContainerDb.ContainerInfo containerInfo = containersDb.getContainerForAccount(account, container);

        assertEquals(account, containerInfo.getAccount());
        assertEquals(container, containerInfo.getContainer());
    }

    @Test
    public void shouldGetNullForNonExistentContainerForAccount() {
        String nonExistentAccount = randomString(10, 20);
        String nonExistentContainer = randomString(10, 20);

        assertNull(containersDb.getContainerForAccount(nonExistentAccount, nonExistentContainer));
    }

    @Test
    public void shouldAddAndGetSharedContainer() {
        String account = randomString(10, 20);
        String container = randomString(10, 20);
        String authToken = randomWebSafeString(20);
        containersDb.addSharedContainer(account, container, authToken);

        String fetchedAuthToken = containersDb.getAuthTokenForSharedContainer(account, container);

        assertEquals(authToken, fetchedAuthToken);
    }

    @Test
    public void shouldAddSameContainerForDifferentAccountsAndGetSharedContainer() {
        String account1 = randomString(10, 20);
        String account2 = randomString(10, 20);
        String container = randomString(10, 20);
        String authToken1 = randomWebSafeString(20);
        String authToken2 = randomWebSafeString(20);
        containersDb.addSharedContainer(account1, container, authToken1);
        containersDb.addSharedContainer(account2, container, authToken2);

        SharedContainer sharedContainer1 = containersDb.getSharedContainerForAuthToken(authToken1);
        SharedContainer sharedContainer2 = containersDb.getSharedContainerForAuthToken(authToken2);

        assertEquals(account1, sharedContainer1.getOwnerAccount());
        assertEquals(container, sharedContainer1.getContainer());
        assertEquals(account2, sharedContainer2.getOwnerAccount());
        assertEquals(container, sharedContainer2.getContainer());
    }

    @Test
    public void shouldGetAuthTokenForAccountByContainer() {
        String account = randomString(10, 20);
        String container1 = randomString(10, 20);
        String authToken1 = randomWebSafeString(20);
        String authToken2 = randomWebSafeString(20);
        String container2 = randomString(10, 20);
        containersDb.addSharedContainer(account, container1, authToken1);
        containersDb.addSharedContainer(account, container2, authToken2);

        String fetchedAuthToken1 = containersDb.getAuthTokenForSharedContainer(account, container1);
        String fetchedAuthToken2 = containersDb.getAuthTokenForSharedContainer(account, container2);

        assertEquals(authToken1, fetchedAuthToken1);
        assertEquals(authToken2, fetchedAuthToken2);
    }
}
