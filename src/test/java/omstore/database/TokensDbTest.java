package omstore.database;

import core.DbTest;
import core.RandomData;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

public abstract class TokensDbTest extends DbTest {
    protected TokenDb tokenDb;

    @Before
    public void setup() {
        tokenDb = locator.getService(TokenDb.class);
        tokenDb.createTables();
    }

    @After
    public void tearDown() {
        tokenDb.deleteTables();
    }

    @Test
    public void deleteTables() {
        tokenDb.deleteTables();
    }

    @Test
    public void shouldAddToken() {
        String account = RandomData.randomString(10, 20);
        String token = RandomData.randomString(10, 20);
        tokenDb.setTokenForAccount(account, token);

        String fetchedToken = tokenDb.getTokenForAccount(account);

        assertEquals(token, fetchedToken);
    }

    @Test
    public void shouldGetAccountByToken() {
        String account = RandomData.randomString(10, 20);
        String token = RandomData.randomString(10, 20);
        tokenDb.setTokenForAccount(account, token);

        String fetchedAccount = tokenDb.getAccountForToken(token);

        assertEquals(account, fetchedAccount);
    }

    @Test
    public void shouldReturnNullAccountIfInvalidToken() {
        String account = RandomData.randomString(10, 20);
        String token = RandomData.randomString(10, 20);
        String invalidToken = RandomData.randomString(10, 20);
        tokenDb.setTokenForAccount(account, token);

        String fetchedToken = tokenDb.getAccountForToken(invalidToken);

        assertNull(fetchedToken);
    }

    @Test
    public void shouldReturnNullTokenIfInvalidAccount() {
        String account = RandomData.randomString(10, 20);
        String token = RandomData.randomString(10, 20);
        String invalidAccount = RandomData.randomString(10, 20);
        tokenDb.setTokenForAccount(account, token);

        String fetchedToken = tokenDb.getTokenForAccount(invalidAccount);

        assertNull(fetchedToken);
    }

    @Test
    public void shouldGetAccountByAccessCode() {
        String account = RandomData.randomString(10, 20);
        String accessCode = RandomData.randomString(10, 20);
        tokenDb.addAccessCode(account, accessCode, 0);

        String clientAccount = tokenDb.getAccountForAccessCode(accessCode);

        assertEquals(account, clientAccount);
    }

    @Test
    public void shouldGetAndVerifyAccountForAccessCode() {
        String account = RandomData.randomString(10, 20);
        String accessCode = RandomData.randomString(10, 20);
        Long expires = 3600l;
        String valid = expires.toString();
        tokenDb.addAccessCode(account, accessCode, 3600l);

        String clientAccount = tokenDb.getAccountForAccessCode(accessCode);

        assertEquals(account, clientAccount);

        assertEquals(valid, tokenDb.checkAccountForAccessCode(account, accessCode));
    }

    @Test
    public void shouldDeleteTokens() {
        String account = RandomData.randomString(10, 20);
        String token = RandomData.randomString(10, 20);

        tokenDb.setTokenForAccount(account, token);

        tokenDb.deleteToken(account);

        assertNull(tokenDb.getTokenForAccount(account));
    }
}
