package omstore.database.dynamo;

import core.ServiceConfigurationProvider;
import core.TestDynamoOmStoreDatabaseConfigurator;
import omstore.database.TokensDbTest;

public class DynamoTokensDbTest extends TokensDbTest {
    @Override
    public ServiceConfigurationProvider getDatabaseConfigurator() {
        return new TestDynamoOmStoreDatabaseConfigurator();
    }
}
