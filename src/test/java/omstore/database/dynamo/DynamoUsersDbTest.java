package omstore.database.dynamo;

import core.ServiceConfigurationProvider;
import core.TestDynamoOmStoreDatabaseConfigurator;
import omstore.database.TokensDbTest;
import omstore.database.UsersDbTest;

public class DynamoUsersDbTest extends UsersDbTest {
    @Override
    public ServiceConfigurationProvider getDatabaseConfigurator() {
        return new TestDynamoOmStoreDatabaseConfigurator();
    }
}
