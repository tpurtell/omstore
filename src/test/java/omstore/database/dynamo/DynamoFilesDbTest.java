package omstore.database.dynamo;

import omstore.database.FilesDbTest;
import core.ServiceConfigurationProvider;
import core.TestDynamoOmStoreDatabaseConfigurator;

public class DynamoFilesDbTest extends FilesDbTest {
    @Override
    public ServiceConfigurationProvider getDatabaseConfigurator() {
        return new TestDynamoOmStoreDatabaseConfigurator();
    }
}
