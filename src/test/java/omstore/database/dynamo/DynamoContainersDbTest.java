package omstore.database.dynamo;

import core.ServiceConfigurationProvider;
import core.TestDynamoOmStoreDatabaseConfigurator;
import omstore.database.ContainerDb;
import omstore.database.ContainersDbTest;
import omstore.database.FilesDbTest;

public class DynamoContainersDbTest extends ContainersDbTest {
    @Override
    public ServiceConfigurationProvider getDatabaseConfigurator() {
        return new TestDynamoOmStoreDatabaseConfigurator();
    }
}
