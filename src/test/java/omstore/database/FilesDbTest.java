package omstore.database;

import core.DbTest;
import core.RandomData;
import omstore.database.FilesDb.FileInfo;
import omstore.database.FilesDb.FilesResult;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;


public abstract class FilesDbTest extends DbTest {
    protected FilesDb filesDb;

    @Before
    public void setup() {
        filesDb = locator.getService(FilesDb.class);
        filesDb.createTables();
    }

    @After
    public void tearDown() {
        filesDb.deleteTables();
    }

    @Test
    public void deleteTables() {
        filesDb.deleteTables();
    }

    @Test
    public void testAddGet() {
        String account = RandomData.randomString(10, 20);
        String container = RandomData.randomString(10, 20);
        String specifiedId = RandomData.randomString(10, 20);
        String relativeStorageUrl = RandomData.randomString(10, 20);
        String md5 = RandomData.randomString(10, 20);
        String tag = RandomData.randomString(10, 20);
        String mimeType = RandomData.randomString(10, 20);
        long size = RandomData.randomPositiveLong();
        long uploadDate = RandomData.randomPositiveLong();
        filesDb.addFile(account, container, specifiedId, relativeStorageUrl, tag, mimeType, size,
                md5, uploadDate);

        FileInfo fi = filesDb.getFileFromSpecifiedId(account, container, specifiedId);
        Assert.assertEquals(account, fi.account);
        Assert.assertEquals(container, fi.container);
        Assert.assertEquals(specifiedId, fi.specifiedId);
        Assert.assertEquals(relativeStorageUrl, fi.relativeStorageUrl);

        Assert.assertEquals(tag, fi.tag);
        Assert.assertEquals(mimeType, fi.mimeType);
        Assert.assertEquals(size, fi.size);
        Assert.assertEquals(uploadDate, fi.uploadDate);
    }

    @Test
    public void shouldGetStoragePathFromHashOfFile() {
        String account = RandomData.randomString(10, 20);
        String container = RandomData.randomString(10, 20);
        String specifiedId = RandomData.randomString(10, 20);
        String expectedStoragePath = RandomData.randomString(10, 20);
        String md5 = RandomData.randomString(10, 20);
        long size = RandomData.randomPositiveLong();
        long uploadDate = RandomData.randomPositiveLong();
        filesDb.addFile(account, container, specifiedId, expectedStoragePath, null, null, size,
                md5, uploadDate);

        String actualStoragePath = filesDb.getStoragePath(md5);

        Assert.assertEquals(expectedStoragePath, actualStoragePath);
    }

    @Test
    public void testDelete() {
        String account = RandomData.randomString(10, 20);
        String container = RandomData.randomString(10, 20);
        String specifiedId = RandomData.randomString(10, 20);
        String md5 = RandomData.randomString(10, 20);
        String tag = RandomData.randomString(10, 20);
        String relativeStorageUrl = RandomData.randomString(10, 20);
        String mimeType = RandomData.randomString(10, 20);
        long size = RandomData.randomPositiveLong();
        long uploadDate = RandomData.randomPositiveLong();
        filesDb.addFile(account, container, specifiedId, relativeStorageUrl, tag, mimeType, size,
                md5, uploadDate);

        filesDb.removeFile(account, container, specifiedId);

        FileInfo fi = filesDb.getFileFromSpecifiedId(account, container, specifiedId);
        Assert.assertNull(fi);
    }

    @Test
    public void testAddList() {
        String account = RandomData.randomString(10, 20);
        String container = RandomData.randomString(10, 20);
        String md5 = RandomData.randomString(10, 20);
        FileInfo[] fi = new FileInfo[3];
        long base_time = RandomData.randomPositiveLong();
        base_time = Math.max(base_time, base_time + 3);
        for (int i = 0; i < fi.length; ++i) {
            fi[i] = new FileInfo();
            fi[i].account = account;
            fi[i].container = container;
            fi[i].specifiedId = "a" + i; // so they are in order
            fi[i].relativeStorageUrl = RandomData.randomString(10, 20);
            fi[i].tag = RandomData.randomString(10, 20);
            fi[i].mimeType = RandomData.randomString(10, 20);
            fi[i].size = RandomData.randomPositiveLong();
            fi[i].uploadDate = base_time - i; // forces the order to match what
            // is supposed to come back
            // (newest to oldest)
            filesDb.addFile(fi[i].account, fi[i].container, fi[i].specifiedId,
                    fi[i].relativeStorageUrl, fi[i].tag, fi[i].mimeType, fi[i].size,
                    md5, fi[i].uploadDate);
        }

        FilesResult result = filesDb.getFilesForContainer(account, container, null, null, null);
        Assert.assertEquals(fi.length, result.files.size());
        int i = 0;
        for (FileInfo ifi : result.files)
            assertEquals(fi[i++], ifi);

        for (int j = 0; j < fi.length; ++j) {
            result = filesDb.getFilesForContainer(account, container, fi[j].mimeType, null, null);
            Assert.assertEquals(1, result.files.size());
            assertEquals(fi[j], result.files.iterator().next());

            result = filesDb
                    .getFilesForContainer(account, container, null, fi[j].tag, null);
            Assert.assertEquals(1, result.files.size());
            assertEquals(fi[j], result.files.iterator().next());
        }
    }

    void assertEquals(FileInfo a, FileInfo b) {
        Assert.assertEquals(a.account, b.account);
        Assert.assertEquals(a.container, b.container);
        Assert.assertEquals(a.mimeType, b.mimeType);
        Assert.assertEquals(a.relativeStorageUrl, b.relativeStorageUrl);
        Assert.assertEquals(a.size, b.size);
        Assert.assertEquals(a.specifiedId, b.specifiedId);
        Assert.assertEquals(a.uploadDate, b.uploadDate);
        Assert.assertEquals(a.tag, b.tag);
    }

    @Test
    public void testGetFilesLarge() {
        String account = RandomData.randomString(10, 20);
        String container = RandomData.randomString(10, 20);
        HashMap<String, String> entries = new HashMap<>();
        int byte_size = 0;
        while (byte_size < 1024 * 1024) {
            String specifiedId = RandomData.randomString(32);
            String md5 = RandomData.randomString(10);
            String relativeStorageUrl = RandomData.randomString(1024);
            String tag = RandomData.randomString(10, 20);
            String mimeType = RandomData.randomString(10, 20);
            long size = RandomData.randomPositiveLong();
            long uploadDate = RandomData.randomPositiveLong();
            filesDb.addFile(account, container, specifiedId, relativeStorageUrl, tag, mimeType,
                    size, md5, uploadDate);
            byte_size += 2 * relativeStorageUrl.length() / 3; // approx
            entries.put(specifiedId, relativeStorageUrl);
        }
        FilesResult result1 = filesDb.getFilesForContainer(account, container, null, null, null);
        Assert.assertNotNull(result1.continuationKey);
        FilesResult result2 = filesDb.getFilesForContainer(account, container, null, null,
                result1.continuationKey);
        Assert.assertNull(result2.continuationKey);
        Assert.assertEquals(entries.size(), result1.files.size() + result2.files.size());

        for (FileInfo i : result1.files) {
            String url = entries.remove(i.specifiedId);
            Assert.assertNotNull(url);
            Assert.assertEquals(url, i.relativeStorageUrl);
        }
        for (FileInfo i : result2.files) {
            String url = entries.remove(i.specifiedId);
            Assert.assertNotNull(url);
            Assert.assertEquals(url, i.relativeStorageUrl);
        }
    }
}
