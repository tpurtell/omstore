package omstore.database;


import core.DbTest;
import core.RandomData;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public abstract class UsersDbTest extends DbTest {

    protected UserDb userDb;
    @Before
    public void setup() {
        userDb = locator.getService(UserDb.class);
        userDb.createTables();
    }

    @After
    public void tearDown() {
        userDb.deleteTables();
    }


    @Test
    public void shouldRegisterUser() {
        String account = RandomData.randomString(10, 20);
        String password = RandomData.randomString(10, 20);
        userDb.registerUser(account, password);
        assertTrue(userDb.checkUser(account));
        assertTrue(userDb.checkPassword(account, password));
    }

    @Test
    public void shouldCheckNonexistingUser() {
        String account = RandomData.randomString(10, 20);
        String password = RandomData.randomString(10, 20);

        assertFalse(userDb.checkUser(account));
        userDb.registerUser(account,password);
        String passwordNew = RandomData.randomString(10, 20);;
        assertFalse(userDb.checkPassword(account,passwordNew));
    }
}
