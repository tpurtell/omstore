package omstore;

import core.LocatorIds;
import core.ServiceConfigurationProvider;
import core.ServiceLauncher;
import core.TestConstants;
import omstore.http.OmStoreHttpApi;
import org.glassfish.hk2.utilities.ServiceLocatorUtilities;
import org.junit.AfterClass;
import org.junit.Before;

import java.net.URI;

public abstract class ServiceTest {
    public static ServiceLauncher launcher;
    private static boolean initializedService = false;

    public abstract ServiceConfigurationProvider getDatabaseConfigurator();

    public abstract ServiceConfigurationProvider getServiceConfigurator();

    // set up one instances of the service
    public ServiceLauncher createLauncher() throws Exception {
        ServiceLauncher launcher = new ServiceLauncher(TestConstants.SERVER_URI,
                new OmStoreHttpApi());
        ServiceLocatorUtilities.addOneConstant(launcher.locator, TestConstants.SERVER_URI,
                LocatorIds.PUBLIC_URI, URI.class);

        getDatabaseConfigurator().bind(launcher.locator);

        getServiceConfigurator().bind(launcher.locator);

        launcher.start();

        return launcher;
    }

    @Before
    public void initializeService() throws Exception {
        if (!initializedService) {
            launcher = createLauncher();
            long start = System.currentTimeMillis();
            initializedService = true;
        }
    }

    @AfterClass
    public static void resetService() throws Exception {
        initializedService = false;
        if (launcher == null)
            return;
        launcher.stop();
        // TODO: Test if server has actually stopped instead of sleeping for random amount of time
        Thread.sleep(10000);
    }
}
