package omstore.service;

import java.io.ByteArrayInputStream;
import java.io.IOException;

import omstore.ServiceTest;
import omstore.exceptions.FileContentException;

import org.apache.log4j.Logger;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.amazonaws.util.IOUtils;
import com.google.common.io.BaseEncoding;

import core.RandomData;

public abstract class FileContentServiceTest extends ServiceTest {
    Logger logger = Logger.getLogger(FileContentServiceTest.class);

    protected FileContentService fileContent;

    @Before
    public void setup() {
        fileContent = launcher.locator.getService(FileContentService.class);
    }

    String randomPath() {
        return String.format("%s/%s/%s", BaseEncoding.base16().encode(RandomData.randomBytes(8)),
                BaseEncoding.base16().encode(RandomData.randomBytes(8)), BaseEncoding.base16()
                        .encode(RandomData.randomBytes(8)));
    }

    @Test
    public void openNonExistentFile() {
        String path = randomPath();

        try {
            fileContent.openContent(path);
            Assert.fail("shouldn't be able to open this files");
        } catch (FileContentException.FileDoesntExist e) {
            // good
        }
    }

    @Test
    public void deleteNonExistentFile() {
        String path = randomPath();

        // should just proceed silently, if its already gone
        fileContent.deleteContent(path);
    }

    @Test
    public void createCompareDelete() throws IOException {
        String path = randomPath();

        byte[] orig = RandomData.randomBytes(31337);
        ByteArrayInputStream input = new ByteArrayInputStream(orig);

        fileContent.writeContent(path, input);

        byte[] ret = IOUtils.toByteArray(fileContent.openContent(path));

        Assert.assertArrayEquals(orig, ret);

        fileContent.deleteContent(path);
    }
}
