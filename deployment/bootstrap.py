#!/usr/bin/env python

import os

def main():
  os.system("pkill noip2")
  os.system("noip2 -C")
  os.system("noip2")
  host_name = raw_input("Please enter the domain name that you selected in no-ip: ")
  print "Hostname entered: " + host_name
  print "Configuring apache2"

  conf_file = ''
  with open("/etc/apache2/sites-available/000-default.conf", "r") as conf_fd_in:
    conf_file = conf_fd_in.read()

  conf_file = conf_file.replace("<server-name>", host_name)
  conf_fd_out = open("/etc/apache2/sites-available/000-default.conf", 'w')
  conf_fd_out.write(conf_file)
  conf_fd_out.close()

  os.system("service apache2 restart")
  print "Apache2 configuration complete"
  print "Configuring Let's encrypt"
  os.chdir("/home/ombox/lets-encrypt-preview/")
  os.system("./venv/bin/letsencrypt -t -e -d " + host_name)

  conf_le_file = ''
  with open("/etc/apache2/sites-available/000-default-le-ssl.conf", "r") as conf_le_fd_in:
    conf_le_file = conf_le_fd_in.readlines()

  conf_le_fd_out = open("/etc/apache2/sites-available/000-default-le-ssl.conf", 'w')
  for line in conf_le_file:
      conf_le_fd_out.write(line)
      if 'Include /etc/letsencrypt/options-ssl.conf' in line:
          conf_le_fd_out.write('Header add X-Forwarded-Proto "https"\nRequestHeader set X-Forwarded-Proto "https"\n')
  conf_le_fd_out.close()

  os.system("service apache2 restart")

  print "Configuration complete"
  os.system("/bin/bash /home/ombox/startup.sh")

if __name__ == '__main__':
  main()
