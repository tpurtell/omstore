#!/bin/bash

(cd /home/ombox/dynamodb/ && ./run.sh) &
cd /home/ombox/build/
java -jar omlet-store-0.0.1-SNAPSHOT-jar-with-dependencies.jar &> logs &
cd ..
sleep 10
echo "Production Configuration"
cat build/override/ProdConfiguration.json
echo "Logs"
tail -f build/logs
