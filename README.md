# Omlet open cloud storage #

### What is this repository for? ###

* [Omlet](http://www.omlet.me/) is an open messaging and application platform that allows users to own their social networking data.  Users choose a cloud repository to store their history and the Omlet system uses the users’ storage provider to store history and share with friends. This project implements that cloud storage services can provide to enable Omlet and Omlet powered applications to use any user’s preferred cloud storage. It also allows an individual or organization to create and configure their own managed cloud storage.

### How do I get set up? ###

* To setup the development environment you need to have Java 8 and maven. The project also requires DynamoDB. By default it uses the local instance of DynamoDB, therefore to run it locally cd into dynamo directory and execute ./run.sh. This will start local instance of DynamoDB.
* The database configuration can be changed by changing the conf/DynamoConfiguration.json file.
* All the dependencies are resolved using Maven. Make sure you run maven after changing the pom file.
* To get the deployable build of the project run ./build.sh to obtain the tarball of the application. The script will generate ombox-build.tar.gz at the end.
* The Omlet cloud can be easily deployed using [Docker](https://docker.com/). A Docker image of the production environment is available on docker public registry. The Project uses DynamoDB as the DB and local file storage disk for storing files. Whole design of the system is quite modular and can be easily hooked on to other DBs and file storage services. Pull requests with some other DB or file storage service (like S3) integration are most welcome :)
* Please [install Docker](https://docs.docker.com/installation/) on your system and run it before following these instructions:
    1. Pull the docker image of Omlet cloud by executing: docker pull harshitagg/ombbox.
    2. Run: ```docker run -p 2653:2653 harshitagg/ombox ./home/ombox/startup.sh```
    3. Look for ProdConfiguration in the logs that are printed on the console. Make a note of Admin key.
* For HTTPS OmStore uses [Let's Encrypt](https://letsencrypt.org/) and [no-ip](http://www.noip.com/) for the DNS mapping. The supported docker image can be installed using this command ```
docker run -i -t -p 80:80 -p 443:443 -p 2653:2653 harshitagg/ombox-https /home/ombox/bootstrap.py
```. The setup will ask user about the no-ip and lets-encrypt settings. Once completed successfully, omstore will be available over https. Note that the lets-encrypt is not released yet, so it uses test CA which will throw warning when opened in the browser. 


### Who do I talk to? ###

* TJ, tj@mobisocial.us
* Harshit, hagarwal@andrew.cmu.edu
* Sohil, snhabib@andrew.cmu.edu